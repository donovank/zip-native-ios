//
//  ScaledSlider.swift
//  ZSP
//
//  Created by Donovan King on 7/14/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ScaledSlider: UIView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var tickView: UIView!
    
    @IBOutlet weak var firstTick: UILabel!
    
    var delegate: ScaledSliderDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func updateConstraints() {
        super.updateConstraints()
        
        if let thumbWidth = slider.currentThumbImage?.size.width {
           let tickWidth = NSLayoutConstraint(item: firstTick, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: thumbWidth)
            firstTick.addConstraint(tickWidth)
        } else {
            print("no width")
        }
    }
    
    func commonInit() {
        NSBundle.mainBundle().loadNibNamed("ScaledSlider", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = bounds
        backgroundColor = UIColor.clearColor()
        tickView.backgroundColor = UIColor.clearColor()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("sliderTapped:"))
        self.slider.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        //  print("A")
        
        let pointTapped: CGPoint = gestureRecognizer.locationInView(self.slider)
        
        let positionOfSlider: CGPoint = slider.frame.origin
        let widthOfSlider: CGFloat = slider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(slider.maximumValue) / widthOfSlider)
        
        slider.setValue(Float(lroundf(Float(newValue))), animated: true)
        delegate?.sliderChangedValue(Int(slider.value))
    }

    @IBAction func stickySlider(sender: UISlider) {
        
        sender.setValue(Float(lroundf(sender.value)), animated: true)
        delegate?.sliderChangedValue(Int(sender.value))
    }
    
}