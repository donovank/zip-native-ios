//
//  ZSPReminder.swift
//  ZSP
//
//  Created by Donovan King on 7/30/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPReminder: ZSPModelObject, ZSPDateCompareable {
    
    var id: String?
    let userID: String
    var title: String
    var activity: ZSPActivityItem?
    var medication: ZSPMedicationItem?
    var frequency: ZSPReminderFrequency
    var date: NSDate
    var type: ZSPReminderType
    
    var active = true
    var extendedView = false
    
    var delegate: ZSPSectionManager?
    
    var day: Int {
        get {
            return NSCalendar.currentCalendar().component(.Day, fromDate: self.date)
        }
    }
    
    var month: Int {
        get {
            return NSCalendar.currentCalendar().component(.Month, fromDate: self.date)
        }
    }
    
    var year: Int {
        get {
            return NSCalendar.currentCalendar().component(.Year, fromDate: self.date)
        }
    }
    
    
//    enum ReminderFrequency: String {
//        case OneTime = "one time"
//        case Weekly = "weekly"
//        case OnDays = "on these days"
//        
//        static func fromString(freq: String) -> ReminderFrequency {
//            switch freq {
//            case "one time":
//                return .OneTime
//            case "weekly":
//                return .Weekly
//            case "on these days":
//                return .OnDays
//            default:
//                return .OneTime
//            }
//        }
//    }
    
    init(userID: String, title: String, frequency: ZSPReminderFrequency, date: NSDate){
        self.userID = userID
        self.title = title
        self.date = date
        self.frequency = frequency
        self.type = .Other
    }
    
    init(fromActivity activity: ZSPActivityItem, withFrequency frequency: ZSPReminderFrequency, onDate: NSDate){
        self.userID = ""
        self.activity = activity
        self.frequency = frequency
        self.date = onDate
        self.title = activity.title
        self.type = .Activity
    }
    
    init(fromMedication med: ZSPMedicationItem, withFrequency frequency: ZSPReminderFrequency, onDate: NSDate) {
        self.userID = ""
        self.medication = med
        self.title = med.title
        self.date = onDate
        self.frequency = frequency
        self.type = .Medication
    }
    
    init(fromOtherType other: ZSPOtherReminderItem, withFrequency frequency: ZSPReminderFrequency, onDate: NSDate) {
        self.userID = ""
        self.title = other.title
        self.date = onDate
        self.frequency = frequency
        self.type = .Other 
    }
    
    func updateObjectAtSectionIndex(sectionIndex: Int) {
        self.delegate?.objectUpdated(self, inSectionIndex: sectionIndex)
    }
    
}

