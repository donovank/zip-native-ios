//
//  ZSPActivityModel.swift
//  ZSP
//
//  Created by Donovan King on 7/30/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit


class ZSPActivityItem: ZSPSafetyPlanItem {

    let mood: Int
    var reminderOn: Bool
    
    
    init(userID: String, title: String, mood: Int, reminder: Bool){
        self.mood = mood
        self.reminderOn = reminder
        
        super.init(userID: userID, title: title)
    }
    
}