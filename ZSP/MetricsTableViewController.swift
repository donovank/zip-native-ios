//
//  MetricsTableViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/12/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import SWRevealViewController
import DZNEmptyDataSet

class MetricsTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SWRevealViewControllerDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var timeAxisSelector: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var metricEmptyDataView: MetricEmptyDataView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set up table view
        tableView.delegate = self
        tableView.dataSource = self // TODO: Lets move this to it's own DS class
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetSource = self
        tableView.tableFooterView = UIView()
        
        metricEmptyDataView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 60, 0)
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "MetricNavBackground"), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        }
        
        if self.revealViewController() != nil {
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            tableView.userInteractionEnabled = true
            timeAxisSelector.userInteractionEnabled = true
        } else {
            tableView.userInteractionEnabled = false
            timeAxisSelector.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            tableView.userInteractionEnabled = true
            timeAxisSelector.userInteractionEnabled = true
        } else {
            tableView.userInteractionEnabled = false
            timeAxisSelector.userInteractionEnabled = false
        }
    }
    
//    func customViewForEmptyDataSet(scrollView: UIScrollView!) -> UIView! {
//        
//        return MetricEmptyDataView(frame: CGRectMake(0, 0, 600, 700))
//        
//    }
//    
//    func verticalOffsetForEmptyDataSet(scrollView: UIScrollView!) -> CGFloat {
//        return -350
//    }
    
    @IBAction func timeAxisSwitched(sender: UISegmentedControl) {
        print(timeAxisSelector.selectedSegmentIndex)
    }

    // MARK: - Table view data source

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("MoodCell", forIndexPath: indexPath) as! MoodMetricChartCell
        
        cell.setUpCell()
                
        return cell
    }

}
