//
//  FAQViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/25/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class FAQViewController: UIViewController, LeanTaaSUIMixin {
    
    @IBOutlet weak var scrollViewBackground: UIView!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if menuButton != nil && self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        applyBackground()
        setupViews()
        
    }
    
    
    func applyBackground() {
        if let img = UIImage(named: "SettingsBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    func setupViews() {
        
        scrollViewBackground.layer.cornerRadius = 10
    }
    
}