//
//  ZSPUser.swift
//  ZSP
//
//  Created by Donovan King on 9/20/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import PhoneNumberKit

var CURRENT_USER_NAME: String {
    get {
        if let nameString = ZSPUserProfile.Default.userName {
            return nameString
        } else {
            print("NO USERNAME RIGHT NOW")
            fatalError()
        }
    }
}

final class ZSPUserProfile: ZSPManager {
    
    static var Default = ZSPUserProfile()
    
    private init () {
        
    }
    
    func clearForSignOff() {
        
    }
    
    var id: String?
    var userName: String?
    var firstName: String?
    var lastName: String?
    var mobileNumber: PhoneNumber?
    var gender: String?
    var dateOfBirth: NSDate?

}