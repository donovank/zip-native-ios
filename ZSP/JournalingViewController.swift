//
//  JournalingViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/14/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import MZFormSheetPresentationController
import SWRevealViewController

class JournalingViewController: UIViewController,
                                GradientHelper,
                                DZNEmptyDataSetSource,
                                DZNEmptyDataSetDelegate,
                                UITableViewDelegate,
                                UITableViewDataSource,
                                UIPopoverPresentationControllerDelegate,
                                ZSPModelManagerDelegate,
                                SWRevealViewControllerDelegate,
                                ZSPMoodPickerDelegate,
                                LeanTaaSThreadingMixin {
    
    let presets = SpecGradientPresets()
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var entryCreationButtonsContainer: UIView!
    @IBOutlet weak var fromActivityButton: UIButton!
    @IBOutlet weak var filterToolBar: UIToolbar!
    @IBOutlet weak var searchIcon: UIBarButtonItem!
    @IBOutlet weak var filterMoodIcon: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var toggleDraftButton: UIBarButtonItem!
    
    lazy var searchBar = UISearchBar(frame: CGRectMake(0, 0, 0, 20))
    
    let MODEL = ZSPModelManager.DefaultModelManager.JournalManager
    
    var editingEntryIndexPath: NSIndexPath?
    
    // here only for protocol conformation
    var moodHasBeenSet = false
    var showDrafts = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator.hidden = true
        activityIndicator.activityIndicatorViewStyle = .WhiteLarge
        
        var items = navigationItem.rightBarButtonItems
        let searchBarItem = UIBarButtonItem(customView: searchBar)
        items?.append(searchBarItem)
        navigationItem.setRightBarButtonItems(items, animated: false)

        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font]
        }
        
        MODEL.delegate = self
        setupTableView()
        setupButtonDivider()
        setupButtonShadows()
        self.automaticallyAdjustsScrollViewInsets = false
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        
        if self.revealViewController() != nil {
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)
        navigationItem.leftBarButtonItem?.tintColor = UIColor.darkGrayColor()
        
        
        self.tableView.infiniteScrollTriggerOffset = 500
        self.tableView.addInfiniteScrollWithHandler { [weak self] (tableView) -> Void in
            
            
            ZSPNetworkManager.Network.API.Journaling.getNextPage({ (Void) in
                dispatch_async(dispatch_get_main_queue(), {
                    self?.tableView.finishInfiniteScroll()
                })
            })
        }
        
        self.tableView.setShouldShowInfiniteScrollHandler { [weak self] (tableView) -> Bool in
            return !ZSPNetworkManager.Network.API.Journaling.onLastPage
        }
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let img = UIImage(named: "JournalBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("MEMORY WARNING")
        // Dispose of any resources that can be recreated.
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if isSearchOut {
            clickedSearch(self)
        }
        if(position == .Left) {
            tableView.userInteractionEnabled = true
            entryCreationButtonsContainer.userInteractionEnabled = true
        } else {
            
            tableView.userInteractionEnabled = false
            entryCreationButtonsContainer.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        if isSearchOut {
            clickedSearch(self)
        }
        if(position == .Left) {
            tableView.userInteractionEnabled = true
            entryCreationButtonsContainer.userInteractionEnabled = true
        } else {
            view.endEditing(true)
            tableView.userInteractionEnabled = false
            entryCreationButtonsContainer.userInteractionEnabled = false
        }
    }
    
    func refresh() {
        tableView.reloadData()
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if isSearchOut {
            clickedSearch(self)
        }
        guard let id = segue.identifier else {
            return
        }
        
        switch id {
        case "MoodFilterPopoverSegue":
            if let moodPickerView = segue.destinationViewController as? MoodSelectionPopoverController {
                moodPickerView.delegate = self
            }
            setupMoodFilterPopover(segue)
        case "FromActivityPopoverSegue":
            setupFromActivityPopover(segue)
        default:
            break
        }
        
        if id == "JournalEntrySegue" {
            if let activity = sender as? ZSPActivityItem {
                if let nc = segue.destinationViewController as? UINavigationController {
                    if let vc = nc.viewControllers[0] as? JournalEntryViewController {
                        vc.activity = activity
                    }
                }
            } else if let editEntry = sender as? ZSPJournalEntry {
                if let nc = segue.destinationViewController as? UINavigationController {
                    if let vc = nc.viewControllers[0] as? JournalEntryViewController {
                        vc.entry = editEntry
                        vc.editingEntryIndexPath = self.editingEntryIndexPath
                        self.editingEntryIndexPath = nil
                    }
                }
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if showDrafts {
            return 1
        }
        return MODEL.sectionCount
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if showDrafts {
            let num = MODEL.draftEntries.count * 2
            if num == 0 {
                toggleDraftView(self)
                return num
            } else {
                return num
            }
        }
        return MODEL.countForSection(section) * 2
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("DateHeader") as! JournalEntryHeaderView
        if showDrafts {
            header.titleLabel.text = "Drafts"
        } else {
            header.titleLabel.text = MODEL.headerStringForSection(section)
        }
        header.background.clipsToBounds = true
        header.background.layer.cornerRadius = 4
        header.background.backgroundColor = UIColor(red:0.09, green:0.45, blue:0.68, alpha:0.5)
        return header
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            return tableView.dequeueReusableCellWithIdentifier("SpacerCell", forIndexPath: indexPath)
        } else {
            
            let modifiedIndexPath = NSIndexPath(forRow: indexPath.row/2,
                                                inSection: indexPath.section)
            
            let cellType: ZSPJournalEntryCellType
            if showDrafts {
                cellType = MODEL.draftEntries[modifiedIndexPath.row].getCellType()
            } else {
                cellType = MODEL.getEntryForIndexPath(modifiedIndexPath).getCellType()
            }
            
            switch cellType {
            case .Basic:
                return tableView.dequeueReusableCellWithIdentifier("JournalEntryCellBasic", forIndexPath: indexPath)
            case .ActivityEntry:
                return tableView.dequeueReusableCellWithIdentifier("JournalEntryCellActivity", forIndexPath: indexPath)
            case .NoTitle:
                return tableView.dequeueReusableCellWithIdentifier("JournalEntryCellNoTitle", forIndexPath: indexPath)
            }
        }
        
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        guard indexPath.row % 2 != 1 else {
            return
        }
        
        cell.clipsToBounds = true
        cell.layer.cornerRadius = 10
        cell.selected = false
        
        let modifiedIndexPath = NSIndexPath(forRow: indexPath.row/2,
                                            inSection: indexPath.section)
        var data: ZSPJournalEntry
        
        if showDrafts {
            data = MODEL.draftEntries[modifiedIndexPath.row]
        } else {
            data = MODEL.getEntryForIndexPath(modifiedIndexPath)
        }
        
        if let basicCell = cell as? JournalEntryCellBasic {
            basicCell.dateLabel.text = data.getDateString()
            basicCell.titleLabel.text = data.title
            basicCell.previewLabel.text = data.text
            basicCell.mood = data.mood
            if data.photo == nil {
                basicCell.showPhotoIndicator = false
            } else {
                basicCell.showPhotoIndicator = true
            }
        }
        
        if let noTitleCell = cell as? JournalEntryCellNoTitle {
            noTitleCell.dateLabel.text = data.getDateString()
            noTitleCell.previewLabel.text = data.text
            noTitleCell.mood = data.mood
            if data.photo == nil {
                noTitleCell.showPhotoIndicator = false
            } else {
                noTitleCell.showPhotoIndicator = true
            }
        }
        
        if let activityCell = cell as? JournalEntryCellActivity {
            activityCell.dateLabel.text = data.getDateString()
            activityCell.titleLabel.text = data.title
            activityCell.previewLabel.text = data.text
            activityCell.mood = data.mood
            if data.photo == nil {
                activityCell.showPhotoIndicator = false
            } else {
                activityCell.showPhotoIndicator = true
            }
        }
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            return 10
        } else {
            let modifiedIndexPath = NSIndexPath(forRow: indexPath.row/2,
                                                inSection: indexPath.section)
            let cellType: ZSPJournalEntryCellType
            if showDrafts {
                cellType = MODEL.draftEntries[modifiedIndexPath.row].getCellType()
            } else {
                cellType = MODEL.getEntryForIndexPath(modifiedIndexPath).getCellType()
            }
            
            // Change cell size a little bit more dynamically here. 
            
            switch cellType {
            case .Basic:
                return 120
            case .ActivityEntry:
                return 120
            case .NoTitle:
                return 100
            }
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let modifiedIndexPath = NSIndexPath(forRow: indexPath.row/2,
                                            inSection: indexPath.section)
        let editEntry: ZSPJournalEntry
        if showDrafts{
            editEntry = MODEL.draftEntries[modifiedIndexPath.row]
        } else {
            editEntry = MODEL.getEntryForIndexPath(modifiedIndexPath)
        }
        editingEntryIndexPath = modifiedIndexPath
        performSegueWithIdentifier("JournalEntrySegue", sender: editEntry)
        tableView.cellForRowAtIndexPath(indexPath)?.selected = false
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Your journal entries will be displayed here."
        if let font = UIFont(name: "Lato-Regular", size: 16) {
            let attrs = [NSFontAttributeName: font,
                         NSForegroundColorAttributeName: UIColor.darkGrayColor()]
            return NSAttributedString(string: str, attributes: attrs)
        } else {
            let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
                         NSForegroundColorAttributeName: UIColor.darkGrayColor()]
            return NSAttributedString(string: str, attributes: attrs)
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
    
    
    @IBAction func toggleDraftView(sender: AnyObject) {
        showDrafts = !showDrafts
        guard MODEL.draftEntries.count != 0 else {
            let alert = UIAlertController(title: "No Drafts", message: nil, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default) { (alert) in
                    self.showDrafts = false
                    self.toggleDraftButton.title = "Show Drafts"
                    self.refresh()
                })
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        if showDrafts {
            toggleDraftButton.title = "Hide Drafts"
        } else {
            toggleDraftButton.title = "Show Drafts"
        }
        tableView.reloadData()
    }
    
    func setupButtonDivider() {
        entryCreationButtonsContainer.layer.cornerRadius = 10
        
        let divider = CAShapeLayer()
        divider.strokeColor = UIColor(red: 4/255, green: 177/255, blue: 229/255, alpha: 1.0).CGColor
        divider.fillColor = nil
        divider.lineDashPattern = [2,3]
        let divRect = CGRect(x: 0.0, y: 0.0, width: 0, height: Double(fromActivityButton.frame.size.height))
        divider.path = UIBezierPath(rect: divRect).CGPath
        divider.frame = fromActivityButton.bounds
        fromActivityButton.layer.addSublayer(divider)
    }
    
    func setupButtonShadows() {
        entryCreationButtonsContainer.layer.shadowColor = UIColor.blackColor().CGColor
        entryCreationButtonsContainer.layer.shadowOpacity = 0.2
        entryCreationButtonsContainer.layer.shadowOffset = CGSizeMake(0, 3 )
        entryCreationButtonsContainer.layer.shadowRadius = 1
    }
    
    func setupTableView() {
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let header = UINib(nibName: "JournalEntryHeaderView", bundle: nil)
        tableView.registerNib(header, forHeaderFooterViewReuseIdentifier: "DateHeader")
        
        let cellOneNib = UINib(nibName: "JournalEntryCellBasic", bundle: nil)
        tableView.registerNib(cellOneNib, forCellReuseIdentifier: "JournalEntryCellBasic")
        
        let cellTwoNib = UINib(nibName: "JournalEntryCellNoTitle", bundle: nil)
        tableView.registerNib(cellTwoNib, forCellReuseIdentifier: "JournalEntryCellNoTitle")
        
        let cellThreeNib = UINib(nibName: "JournalEntryCellActivity", bundle: nil)
        tableView.registerNib(cellThreeNib, forCellReuseIdentifier: "JournalEntryCellActivity")

    }
    
    func setupMoodFilterPopover(segue: UIStoryboardSegue) {
        let presentationSegue = segue as! MZFormSheetPresentationViewControllerSegue
        let container = presentationSegue.formSheetPresentationController.view
        let screenBounds = UIScreen.mainScreen().bounds
        let statusBarHeight = UIApplication.sharedApplication().statusBarFrame.size.height
        
        if let prezController = presentationSegue.formSheetPresentationController.presentationController {
            prezController.contentViewSize = UILayoutFittingCompressedSize // Set size first!
            
            let y = screenBounds.height - container.frame.height - statusBarHeight - filterToolBar.layer.bounds.height - 5
            
            prezController.portraitTopInset = y
            prezController.shouldCenterHorizontally = false
            prezController.shouldCenterVertically = false
            prezController.shouldApplyBackgroundBlurEffect = false
            prezController.shouldDismissOnBackgroundViewTap = true
            
        }
        
        let x = screenBounds.width - container.frame.width - 5
        presentationSegue.formSheetPresentationController.contentViewControllerTransitionStyle = .SlideFromRight
        presentationSegue.formSheetPresentationController.contentViewCornerRadius = 10
        container.frame = CGRect(x: x, y: 0, width: 0, height: 0)
    }
    
    func setupFromActivityPopover(segue: UIStoryboardSegue) {
        let activityPopover = segue.destinationViewController as! FromActivityPopoverTableViewController
        activityPopover.modalPresentationStyle = UIModalPresentationStyle.Popover
        activityPopover.popoverPresentationController?.delegate = self
        activityPopover.popoverPresentationController?.sourceRect = fromActivityButton.bounds
        activityPopover.parent = self
    }
    
    var isSearchOut = false
    
    @IBAction func clickedSearch(sender: AnyObject) {
        if isSearchOut {
            isSearchOut = false
            UIView.animateWithDuration(0.25,
            animations: {
                self.searchBar.frame = CGRectMake(0, 0, 0, 20)
            },
            completion: { (completed) in
                self.navigationItem.title = "Journaling"
            })
            self.searchBar.resignFirstResponder()
            
        } else {
            isSearchOut = true
            navigationItem.title = nil
            let width = UIScreen.mainScreen().bounds.width * 0.7
            self.searchBar.becomeFirstResponder()
            UIView.animateWithDuration(0.25,
            animations: {
                self.searchBar.frame.size.width = width
                self.searchBar.frame.origin.x = 0
            },
            completion: { (completed) in
            })
        }
        
    }
    
    var filterByMood: ZSPMoodType?
    
    func moodSelected(mood: ZSPMoodType) {
        switch mood {
        case .VeryHappy:
            filterByMood = .VeryHappy
            filterMoodIcon.image = UIImage(named: "VeryHappyFilterIcon")
            filterMoodIcon.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        case .Happy:
            filterByMood = .Happy
            filterMoodIcon.image = UIImage(named: "HappyFilterIcon")
            filterMoodIcon.tintColor = UIColor(red:0.46, green:0.76, blue:0.85, alpha:1.0)
        case .Neutral:
            filterByMood = .Neutral
            filterMoodIcon.image = UIImage(named: "NeutralFilterIcon")
            filterMoodIcon.tintColor = UIColor(red:0.65, green:0.79, blue:0.84, alpha:1.0)
        case .Sad:
            filterByMood = .Sad
            filterMoodIcon.image = UIImage(named: "SadFilterIcon")
            filterMoodIcon.tintColor = UIColor(red:0.67, green:0.71, blue:0.73, alpha:1.0)
        case .VerySad:
            filterByMood = .VerySad
            filterMoodIcon.image = UIImage(named: "VerySadFilterIcon")
            filterMoodIcon.tintColor = UIColor(red:0.52, green:0.52, blue:0.52, alpha:1.0)
        case .Other:
            break
        }
    }

}
