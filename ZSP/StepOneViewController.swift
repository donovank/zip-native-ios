//
//  StepOneViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

/*
 *  CLASS TODO:
 *  - Refactor mood selection bar into own class. Handle
 *    events with delegation.
 *  - Make filter value Enum instead of Int
 */

import UIKit
import MZFormSheetPresentationController

class StepOneViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ZSPModelManagerDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var sadButton: UIButton!
    @IBOutlet weak var sadImage: UIImageView!
    @IBOutlet weak var sadLabel: UILabel!
    @IBOutlet weak var lonelyButton: UIButton!
    @IBOutlet weak var lonelyImage: UIImageView!
    @IBOutlet weak var lonelyLabel: UILabel!
    @IBOutlet weak var worriedButton: UIButton!
    @IBOutlet weak var worriedImage: UIImageView!
    @IBOutlet weak var worriedLabel: UILabel!
    @IBOutlet weak var angryButton: UIButton!
    @IBOutlet weak var angryImage: UIImageView!
    @IBOutlet weak var angryLabel: UILabel!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var otherImage: UIImageView!
    @IBOutlet weak var otherLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var emotionsContainerView: UIView!
    
    
    @IBOutlet weak var topContainerView: UIView!
    
    var buttons = [(button: UIButton, val: Int, img: UIImageView, label: UILabel)]()
    
    let MODEL = ZSPModelManager.DefaultModelManager.SafetyPlan
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        MODEL.stepOneDelegate = self
        buttons = [(button: sadButton, val: 1, img: sadImage, label: sadLabel),
                   (button: lonelyButton, val: 2, img: lonelyImage, label: lonelyLabel),
                   (button: worriedButton, val: 3, img: worriedImage, label: worriedLabel),
                   (button: angryButton, val: 4, img: angryImage, label: angryLabel),
                   (button: otherButton, val: 5, img: otherImage, label: otherLabel)]
        
        if let img = UIImage(named: "SafetyPlanBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
    }
    
    override func viewDidLayoutSubviews() {
        viewSetup()
    }
    
    func refresh() {
        tableView.reloadData()
        for tup in buttons {
            if MODEL.currentFilter == tup.val {
                tup.img.tintColor = selectedBlue()
                tup.label.textColor = selectedBlue()
            } else {
                tup.img.tintColor = UIColor.lightGrayColor()
                tup.label.textColor = UIColor.lightGrayColor()
            }
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = (MODEL.stepOneItems.count * 2)
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row % 2 == 1 {
            return tableView.dequeueReusableCellWithIdentifier("SpacerCell", forIndexPath: indexPath)
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("SimpleActivityCell", forIndexPath: indexPath) as! ActivityCell
            
            cell.layer.cornerRadius = 10
            cell.fromActivity(MODEL.stepOneItems[indexPath.row/2])
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            return 10
        }
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        
        view.backgroundColor = UIColor.clearColor()
        
        return view
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        if indexPath.row % 2 == 1 {
            return nil
        }
        
        let deleteHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            if let cell = self.tableView.cellForRowAtIndexPath(indexPath) as? ActivityCell {
                if let item = cell.activity {
                    ZSPModelManager.DefaultModelManager.SafetyPlan.removeActivity(item)
                }
            }
        }
        
        let delete = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteHandler)
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.layer.cornerRadius = 0
                
        return [delete]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.row % 2 == 1 {
            return false
        }
        return true
    }
    
    func tableView(tableView: UITableView, didEndEditingRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.layer.cornerRadius = 10
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "CreateActivity" {
            let presentationSegue = segue as! MZFormSheetPresentationViewControllerSegue
            
            if let prezController = presentationSegue.formSheetPresentationController.presentationController {
                
                var viewSize = CGSize()
                viewSize.width = 300
                viewSize.height = UIScreen.mainScreen().bounds.height * 0.7
                print(viewSize)
                prezController.contentViewSize = viewSize
                prezController.shouldCenterHorizontally = true
                prezController.shouldApplyBackgroundBlurEffect = false
            }
        }
    }

    
    @IBAction func setMoodFilter(sender: UIButton) {
        for tup in buttons {
            if sender == tup.button {
                tup.img.tintColor = selectedBlue()
                tup.label.textColor = selectedBlue()
            } else {
                tup.img.tintColor = UIColor.lightGrayColor()
                tup.label.textColor = UIColor.lightGrayColor()
            }
        }
        
        switch sender {
        case sadButton:
            MODEL.setActivityMood(1)
        case lonelyButton:
            MODEL.setActivityMood(2)
        case worriedButton:
            MODEL.setActivityMood(3)
        case angryButton:
            MODEL.setActivityMood(4)
        case otherButton:
            MODEL.setActivityMood(5)
        default:
            break
        }
    }
    
        
    func viewSetup() {
        for tup in buttons {
            if MODEL.currentFilter == tup.val {
                tup.img.tintColor = selectedBlue()
                tup.label.textColor = selectedBlue()
            } else {
                tup.img.tintColor = UIColor.lightGrayColor()
                tup.label.textColor = UIColor.lightGrayColor()
            }
        }
        
        let topCorners = UIRectCorner.TopRight.union(UIRectCorner.TopLeft)
        let topPath = UIBezierPath(roundedRect: topContainerView.layer.bounds, byRoundingCorners: topCorners, cornerRadii: CGSizeMake(10, 10))
        let topMask = CAShapeLayer()
        topMask.frame = topContainerView.bounds
        topMask.path = topPath.CGPath
        topContainerView.layer.mask = topMask
        
        let corners = UIRectCorner.BottomRight.union(UIRectCorner.BottomLeft)
        let path = UIBezierPath(roundedRect: emotionsContainerView.layer.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(10, 10))
        let mask = CAShapeLayer()
        mask.frame = emotionsContainerView.bounds
        mask.path = path.CGPath
        emotionsContainerView.layer.mask = mask
    }
    
}
