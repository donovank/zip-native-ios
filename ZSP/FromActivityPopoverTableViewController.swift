//
//  FromActivityPopoverTableViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class FromActivityPopoverTableViewController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, LeanTaaSUIMixin {
    
    weak var parent: JournalingViewController?
    
    let activityList = ZSPModelManager.DefaultModelManager.SafetyPlan.getAllActivities()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let activity = activityList[indexPath.row]
        let tmpParent = parent
        self.dismissViewControllerAnimated(true) { 
            tmpParent?.performSegueWithIdentifier("JournalEntrySegue", sender: activity)
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activityList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FromActivityCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = activityList[indexPath.row].title
        
        return cell
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "You can create activities in Step One of your Safety Plan."
        if let font = UIFont(name: "Lato-Regular", size: 16) {
            let attrs = [NSFontAttributeName: font,
                         NSForegroundColorAttributeName: UIColor.darkGrayColor()]
            return NSAttributedString(string: str, attributes: attrs)
        } else {
            let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
                         NSForegroundColorAttributeName: UIColor.darkGrayColor()]
            return NSAttributedString(string: str, attributes: attrs)
        }
    }
}
