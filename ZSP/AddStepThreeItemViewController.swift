//
//  AddStepThreeItemViewController.swift
//  ZSP
//
//  Created by Donovan King on 9/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import PhoneNumberKit

class AddStepThreeItemViewController: AddSafetyPlanItemViewController {
    
    @IBOutlet weak var phoneNumberTextField: PhoneNumberTextField!

    var phoneNumber: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEnabled(false)
        textField.delegate = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupPopup()
    }
    
    @IBAction override func save(sender: AnyObject) {
        view.endEditing(true)
        saveThis = textField.text
        if phoneNumberTextField.isValidNumber {
            phoneNumber = phoneNumberTextField.text
            dismissViewControllerAnimated(true, completion: nil)
        } else if let invalid = phoneNumberTextField.text where !invalid.isEmpty {
            let badNumberAlert = UIAlertController(title: "Invalid Number", message: "\(invalid) is not a valid phone number. Please Try again", preferredStyle: .Alert)
            badNumberAlert.addAction(UIAlertAction(title: "Okay", style: .Cancel, handler: { (action) in
                self.phoneNumberTextField.becomeFirstResponder()
            }))
            self.presentViewController(badNumberAlert, animated: true, completion: nil)
        } else {
            dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    override func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === self.textField {
            textField.resignFirstResponder()
            phoneNumberTextField.becomeFirstResponder()
            
        } else if textField === phoneNumberTextField {
            phoneNumberTextField.resignFirstResponder()
            if saveButton.enabled {
                
            }
        }
        
        return true
    }
    
}
