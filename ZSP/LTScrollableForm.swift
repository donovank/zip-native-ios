//
//  LTScrollableForm.swift
//  ZSP
//
//  Created by Donovan King on 8/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

@objc protocol LeanTaaSScrollableForm: AnyObject {
    var activeField: UITextField? { get set }
    var scrollView: UIScrollView! { get set }
    var view: UIView! { get set }
    
    @objc func keyboardWasShown()
    @objc func keyboardWillBeHidden()
}

extension LeanTaaSScrollableForm {
    
    func registerForKeyboardNotifications(caller: LeanTaaSScrollableForm) {
        //Adding notifies on keyboard appearing
        
        
        NSNotificationCenter.defaultCenter().addObserver(caller, selector: #selector(caller.keyboardWasShown), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(caller, selector: #selector(caller.keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func deregisterFromKeyboardNotifications(caller: LeanTaaSScrollableForm) {
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(caller, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(caller, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        guard let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size else { return }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
        
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = view.frame
        aRect.size.height -= keyboardSize.height
        if let activeFieldPresent = activeField{
            if (!CGRectContainsPoint(aRect, activeFieldPresent.frame.origin)){
                scrollView.scrollRectToVisible(activeFieldPresent.frame, animated: true)
            }
        }
    }
    
    
    func keyboardWillBeHidden(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo!
        guard let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size else { return }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, -keyboardSize.height, 0.0)
        scrollView.contentInset = contentInsets
        scrollView.scrollIndicatorInsets = contentInsets
        view.endEditing(true)
        scrollView.scrollEnabled = false
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        activeField = nil
    }
    
}