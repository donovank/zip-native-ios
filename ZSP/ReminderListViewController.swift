//
//  ReminderListViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/19/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import SWRevealViewController

class ReminderListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ZSPModelManagerDelegate, LeanTaaSUIMixin, SWRevealViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let MODEL = ZSPModelManager.DefaultModelManager.ReminderManager
    
    var editingEntryIndexPath: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        tableView.delegate = self
        tableView.dataSource = self
        
        MODEL.delegate = self
        
        let headerNib = UINib(nibName: "ReminderHeaderView", bundle: nil)
        tableView.registerNib(headerNib, forHeaderFooterViewReuseIdentifier: "ReminderHeader")
        
        let cellNib = UINib(nibName: "ReminderCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "ReminderCell")
        
        if self.revealViewController() != nil {
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)

        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let img = UIImage(named: "RemindersBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            tableView.userInteractionEnabled = true
        } else {
            tableView.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            tableView.userInteractionEnabled = true
        } else {
            tableView.userInteractionEnabled = false
        }
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return MODEL.getSectionCount()
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return MODEL.countForSection(section) * 2
        
    }

    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            return tableView.dequeueReusableCellWithIdentifier("SpacerCell", forIndexPath: indexPath)
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ReminderCell", forIndexPath: indexPath) as! ReminderCell
            
            cell.cellSetup(withReminder: MODEL.getReminderInSection(indexPath.section, atRow: indexPath.row/2))
            curveView(cell.alarmButton, byRoundingCorners: [.BottomLeft, .TopLeft], withRadius: 4)
            if let font = UIFont(name: "Lato-Regular", size: 14) {
                cell.repeatOnDaysView.repeatLabel.font = font
            }
            if let font = UIFont(name: "Lato-Regular", size: 12) {
                cell.repeatOnDaysView.textRepeatLabel.font = font
            }
            cell.repeatOnDaysView.setDaysFontSize(12)
            
            cell.contentView.addSubview(cell.buttonContainerView)
            cell.contentView.layoutIfNeeded()
            cell.contentView.addConstraint(NSLayoutConstraint(item: cell.buttonContainerView,
                                                              attribute: .Width,
                                                              relatedBy: .Equal,
                                                              toItem: cell.contentView,
                                                              attribute: .Width,
                                                              multiplier: 0.2,
                                                              constant: 0))
            cell.contentView.addConstraint(NSLayoutConstraint(item: cell.buttonContainerView,
                                                              attribute: .Leading,
                                                              relatedBy: .Equal,
                                                              toItem: cell.contentView,
                                                              attribute: .Leading,
                                                              multiplier: 1,
                                                              constant: 20))
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier("ReminderHeader") as! ReminderHeaderView

        header.headerSetup(MODEL.headerStringForSection(section))
        
        return header
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            return 10
        }
        
        let reminder = MODEL.getReminderInSection(indexPath.section, atRow: indexPath.row/2)
        if reminder.extendedView {
            return 100
        } else {
            return 50
        }
        
        
    }
 

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.row % 2 == 1 {
          return false
        }
        return true
    }
 

    
    // Override to support editing the table view.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
 
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            self.MODEL.deleteReminderInSection(indexPath.section, atRow: (indexPath.row/2))
        }
        
        let editHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            self.editingEntryIndexPath = NSIndexPath(forRow: indexPath.row/2,
                                                     inSection: indexPath.section)
            self.performSegueWithIdentifier("EditReminderSegue", sender: self.MODEL.getReminderInSection(indexPath.section, atRow: (indexPath.row/2)))
        }
        
        let delete = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteHandler)
        let edit = UITableViewRowAction(style: .Normal, title: "Edit", handler: editHandler)
        
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) as? ReminderCell else {
            return [delete, edit]
        }
        
//        curveView(cell.contentContainer, byRoundingCorners: [.TopRight, .BottomLeft], withRadius: 0)
        cell.contentContainer.layer.cornerRadius = 0
        
        return [delete, edit]
    }
    
    func tableView(tableView: UITableView, didEndEditingRowAtIndexPath indexPath: NSIndexPath) {
        
        guard let cell = tableView.cellForRowAtIndexPath(indexPath) as? ReminderCell else {
            return
        }
        cell.contentContainer.layer.cornerRadius = 4
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row % 2 != 1 {
            let reminder = MODEL.getReminderInSection(indexPath.section, atRow: indexPath.row/2)
            reminder.extendedView = !reminder.extendedView
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        print("hello you")
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "EditReminderSegue" {
            if let nc = segue.destinationViewController as? UINavigationController {
                if let vc = nc.childViewControllers[0] as? EditReminderViewController {
                    if let reminder = sender as? ZSPReminder {
                        vc.reminder = reminder
                        vc.editingEntryIndexPath = editingEntryIndexPath
                    }
                }
                
            }
            editingEntryIndexPath = nil
        }
    }
    
    func refresh() {
        self.tableView.reloadData()
    }

}
