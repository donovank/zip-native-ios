//
//  RecordMoodViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit

class RecordMoodViewController: UIViewController, ScaledSliderDelegate ,GradientHelper {
    
    let presets = SpecGradientPresets()
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var hopelessSlideContainerView: UIView!
    @IBOutlet weak var sadSlideContainerView: UIView!
    @IBOutlet weak var sleepSlideContainerView: UIView!
    @IBOutlet weak var wakeSlideContainerView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var sadSlider: ScaledSlider!
    @IBOutlet weak var hopelessSlider: ScaledSlider!
    @IBOutlet weak var noSleepSlider: ScaledSlider!
    @IBOutlet weak var wokeUpSlider: ScaledSlider!
    
    
    
    override func viewDidLoad() {
        setUpView()
        
        sadSlider.delegate = self
        hopelessSlider.delegate = self
        noSleepSlider.delegate = self
        wokeUpSlider.delegate = self
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(named: "MetricNavBackground"), forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor()]
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font, NSForegroundColorAttributeName: UIColor.whiteColor()], forState: .Normal)
        }
    }
    
    @IBAction func save(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func resetSliders(sender: AnyObject) {
        sadSlider.slider.setValue(0, animated: true)
        hopelessSlider.slider.setValue(0, animated: true)
        noSleepSlider.slider.setValue(0, animated: true)
        wokeUpSlider.slider.setValue(0, animated: true)
        validateResetButton()
    }
    
    func sliderChangedValue(newValue: Int) {
        if newValue == 0 {
            validateResetButton()
        }
        resetButton.enabled = true
        resetButton.alpha = 1
    }
    
    func validateResetButton() {
        let sum = sadSlider.slider.value + hopelessSlider.slider.value + noSleepSlider.slider.value + wokeUpSlider.slider.value
        if sum == 0 {
            resetButton.enabled = false
            resetButton.alpha = 0.3
        }
    }
    
    func setUpView() {
        resetButton.layer.cornerRadius = resetButton.frame.size.height/2
        resetButton.enabled = false
        resetButton.alpha = 0.3
        scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        let slideContainerViews = [hopelessSlideContainerView,
                                   sadSlideContainerView,
                                   sleepSlideContainerView,
                                   wakeSlideContainerView]
        let image = UIImage(named: "MetricCellBlue")
        if let bg = image {
            for container in slideContainerViews {
                container.layer.cornerRadius = 10
                container.backgroundColor = UIColor(patternImage: bg)
            }
        } else {
            for container in slideContainerViews {
                container.layer.cornerRadius = 10
                addBlueGradientLayer(container)
            }
        }
    }
    
}
