//
//  ZSPDayType.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

enum ZSPDayType: Int, IteratableEnum {
    case Mon = 0
    case Tue = 1
    case Wed = 2
    case Thu = 3
    case Fri = 4
    case Sat = 5
    case Sun = 6
    
    static let iteratable = [Mon, Tue, Wed, Thu, Fri, Sat, Sun]
    
    static func getTrueDays(dict: [ZSPDayType : Bool]) -> [ZSPDayType] {
        var dayList = [ZSPDayType]()
        let allValues = [Mon, Tue, Wed, Thu, Fri, Sat, Sun]
        for day in allValues {
            if let val = dict[day] where val {
                dayList.append(day)
            }
        }
        return dayList
    }
    
    static func getJSONDays(days: [ZSPDayType]) -> [Int] {
        var dayInts = [Int]()
        for day in days {
            dayInts.append(day.rawValue + 1)
        }
        return dayInts
    }
    
}