//
//  ZSPNetworkManager.swift
//  ZSP
//
//  Created by Donovan King on 8/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

var TOKEN_HEADERS: [String : String] {
    get {
        return ZSPNetworkManager.Network.getTokenHeader()
    }
}

class ZSPNetworkManager {
    
    static let Network = ZSPNetworkManager()
    static let baseURL = "https://m.zsp-aws-test.leantaas.com"
    
    let API = ( SafetyPlan: ZSPSafetyPlanAPI.API,
                Reminder: ZSPReminderAPI.API,
                Auth: ZSPAuthAPI.API,
                Journaling: ZSPJournalAPI.API,
                UserDetails: ZSPUserDetailAPI.API)
    
    private init() {
        
    }
    
    private var tokenHeader = [ "X-AUTH-TOKEN" : "", "JWTTOKEN" : ""]
    
    func setToken(token: String, callback: (()->())?) {
        self.tokenHeader["X-AUTH-TOKEN"] = token
        self.tokenHeader["JWTTOKEN"] = token
        print("Token Set")
        
        callback?()
    }
    
    func getTokenHeader() -> [String : String] {
        return tokenHeader
    }
    
    
    
    func resetToken() {
        let url = "https://test-accounts.leantaas.com/v1/account/token/reissue"
        
        Alamofire.request(.GET,
                          url,
                          parameters: nil,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
        .response(completionHandler: { (req, res, data, error) in
            guard error == nil else {
                print("Error resetting token")
                print(error)
                return
            }
            if let token = res?.allHeaderFields["X-AUTH-TOKEN"] as? String {
                self.setToken(token, callback: nil)
            } else {
                print("error unwrapping token from responce")
                print(res)
            }
            
        })
        
    }
    
}