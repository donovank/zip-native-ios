//
//  ZSPJournalDraftAPI.swift
//  ZSP
//
//  Created by Donovan King on 9/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPJournalDraftAPI: ZSPAPIManager, LeanTaaSThreadingMixin {
    
    static var API = ZSPJournalDraftAPI()
    
    private init() { }
    
    private var page_number = 0
    private var _onLastPage = false
    var onLastPage: Bool {
        get {
            return _onLastPage
        }
    }
    
    func getNextPage(handler: ((Void) -> Void)?) {
        if !_onLastPage {
            page_number += 1
            loadModelWithHandle(handler)
        } else {
            handler?()
        }
    }
    
    func loadModel() {
        self.loadModelWithHandle(nil)
    }
    
    func loadModelWithHandle(handler: ((Void) -> Void)? = nil) {
        let URL = generateURLWithParams(page_number, search: nil)
        print(URL)
        Alamofire.request(.GET,
            URL,
            parameters: nil,
            encoding: .JSON,
            headers: TOKEN_HEADERS)
            .validate()
            .responseJSON { (res) in
                print("Got Drafts")
                print("Response:")
                print(res)
                switch res.result {
                case .Success:
                    if let value = res.result.value {
                        dispatch_async(self.GlobalUserInitiatedQueue, {
                            self.convertFromJSON(value)
                        })
                    } else {
                        print(res)
                        print("ERROR reading draft entry JSON")
                    }
                case .Failure(let error):
                    print("Error getting draft entries")
                    print(res)
                    print(error)
                }
        }
    }
    
    func addDraftEntry(entry: ZSPJournalEntry, wasJournalEntry: Bool = false) {
        let URL = ZSPNetworkManager.baseURL + "/zsp/journal/addUpdateDraft"
        
        let titleOrBlank = entry.title == nil ? "" : entry.title!
        
        var params: [String : AnyObject] =
            ["user" : CURRENT_USER_NAME,
             "title" : titleOrBlank,
             "desc" : entry.text,
             "feeling" : entry.mood.rawValue,
             "activityFl" : entry.isFromActivity]
        
        if let id = entry.id where !wasJournalEntry {
            params["draftId"] = id
        }
        
        if let id = entry.id where wasJournalEntry {
            params["journalId"] = id
        }
        
        Alamofire.request(.POST,
            URL,
            parameters: params,
            encoding: .JSON,
            headers: ZSPNetworkManager.Network.getTokenHeader())
            .responseJSON { (res) in
                switch res.result {
                case .Success:
                    print("created draft response")
                    print(res)
                    if let value = res.result.value {
                        if let dataFromString = value.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
                            let json = JSON(data: dataFromString)
                            if let id = json["id"].string {
                                entry.id = id
                                print(id)
                            } else {
                                print(res)
                                print("ERROR GETTING ID VALUE FROM DRAFT SAVE")

                            }
                        } else {
                            print(res)
                            print("ERROR DECODING RES JSON FROM DRAFT SAVE")
                        }
                        
                    } else {
                        print(res)
                        print("ERROR reading Draft Reply JSON")
                    }
                case .Failure(let error):
                    print("Error adding draft entry")
                    print(res)
                    print(error)
                }
        }
        
        
    }
    
    func uploadPhotoWithId(journalId: String) {
        
        
    }
    
    private func convertFromJSON(jsonString: AnyObject, handler: ((Void) -> Void)? = nil) {
        let json = JSON(jsonString)
        
        var entries = [ZSPJournalEntry]()
        for entry in json["drafts"].arrayValue {
            let entryData = entry["drafts"]
            print(entryData)
            let title: String? = entryData["title"].stringValue == "" ? nil : entryData["title"].stringValue
            let text = entryData["desc"].stringValue
            let mood = entryData["feeling"].intValue
            let isActivity = entryData["activityFl"].boolValue
            let seconds = Double(entryData["date"]["$date"].intValue)/1000
            let date = NSDate(timeIntervalSince1970: seconds)
            let id = entryData["_id"]["$oid"].stringValue
            let photoID = entryData["photoId"].string
            
            let loadedEntry = ZSPJournalEntry(isFromActivity: isActivity,
                                              isDraft: true,
                                              postMood: mood,
                                              editDate: date,
                                              body: text)
            loadedEntry.title = title
            loadedEntry.id = id
            loadedEntry.photoID = photoID
            
            entries.append(loadedEntry)
        }
        if entries.count > 0 {
            if entries.count < 20 {
                self._onLastPage = true
            }
            handler?()
            dispatch_async(GlobalUserInitiatedQueue) {
                ZSPModelManager.DefaultModelManager.JournalManager.draftEntries.appendContentsOf(entries)
            }
        }
    }
    
    func deleteDraft(entry: ZSPJournalEntry) {
        let URL = ZSPNetworkManager.baseURL + "/zsp/journal/deleteDraft"
        
        guard let theID = entry.id else {
            print("Journal Entry has no ID")
            return
        }
        
        let params = ["user" : CURRENT_USER_NAME,
                      "draftId" : theID]
        
        Alamofire.request(.POST,
            URL,
            parameters: params,
            encoding: .JSON,
            headers: ZSPNetworkManager.Network.getTokenHeader())
            .responseJSON { (res) in
                print(res)
        }
        
        
    }
    
    private func generateURLWithParams(page: Int?, search: String?) -> String {
        var URLString = ZSPNetworkManager.baseURL + "/zsp/journal/getDrafts/" + CURRENT_USER_NAME
        if page == nil && search == nil {
            return URLString
        }
        URLString += "?"
        if let pageNum = page {
            URLString += "page=\(pageNum)"
        }
        if let searchFor = search?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            if page != nil {
                URLString += "&"
            }
            URLString += "search=\(searchFor)"
        }
        return URLString
    }
    
}