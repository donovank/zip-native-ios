//
//  NoActionTextField.swift
//  ZSP
//
//  Created by Donovan King on 9/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class NoActionTextField: UITextField {
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        switch action {
        case "paste:":
            return false
        case "cut:":
            return false
        case "copy:":
            return false
        case "select:":
            return false
        case "selectAll:":
            return false
        default:
            return super.canPerformAction(action, withSender: sender)
        }
        
    }
}