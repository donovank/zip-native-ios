//
//  ZSPReminderRepeatMode.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

enum ZSPReminderRepeatMode: String {
    case OnDays = "On These Days"
    case Daily = "Daily"
    case Weekly = "Weekly"
    case Never = "Never"
}