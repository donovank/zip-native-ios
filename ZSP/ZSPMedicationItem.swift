//
//  ZSPMedicationItem.swift
//  ZSP
//
//  Created by Donovan King on 8/1/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPMedicationItem: ZSPModelObject {
    
    let userID: String
    var id: String?
    let title: String
    
    init(userID: String, title: String){
        self.userID = userID
        self.title = title
    }

}