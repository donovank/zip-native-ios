//
//  ZSPJournalEntry.swift
//  ZSP
//
//  Created by Donovan King on 7/27/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPJournalEntry: ZSPModelObject, LeanTaaSThreadingMixin, ZSPDateCompareable {

    var id: String?
    
    var title: String?
    var isFromActivity: Bool
    var isDraft: Bool
    var text: String
    var mood: ZSPMoodType
    var date: NSDate {
        didSet {
            dateStringsLocked = true
            calculateDateStrings()
        }
    }
    
    var delegate: ZSPSectionManager?
    
    // Should this end up becoming it's own class?
    var photo: ZSPJournalEntryPhoto? = ZSPJournalEntryPhoto()
    var photoID: String?
    
    
    let shortFMer = ZSPModelManager.DefaultModelManager.JournalManager.MMMMyyyyFormater
    let longFMer = ZSPModelManager.DefaultModelManager.JournalManager.MMMMddyyyyFormater
    
    private var dateStringsLocked = false
    private var MMMMddyyyy: String?
    private var MMMMyyyy: String?
    
    var day: Int {
        get {
            return NSCalendar.currentCalendar().component(.Day, fromDate: self.date)
        }
    }
    
    var month: Int {
        get {
            return NSCalendar.currentCalendar().component(.Month, fromDate: self.date)
        }
    }
    
    var year: Int {
        get {
            return NSCalendar.currentCalendar().component(.Year, fromDate: self.date)
        }
    }
    
//    required convenience init(fromJson data: String) {
//         //change upton impliment
//    }
    
    init(isFromActivity: Bool, isDraft: Bool, postMood: Int, editDate: NSDate, body: String) {
        self.isFromActivity = isFromActivity
        self.isDraft = isDraft
        self.mood = ZSPMoodType.fromNumber(postMood)
        self.date = editDate
        self.text = body
    }
    
    func getDateString() -> String {
        if self.isDraft {
            return "Draft"
        }
        if dateStringsLocked || MMMMddyyyy == nil {
            return self.longFMer.stringFromDate(self.date)
        } else {
            return MMMMddyyyy!
        }
    }
    
    func getCellType() -> ZSPJournalEntryCellType {
        guard let titleString = title else {
            return .NoTitle
        }
        
        if titleString.isEmpty {
            return .NoTitle
        }

        if isFromActivity {
            return .ActivityEntry
        }
        
        return .Basic
    }
    
    func getMYString() -> String {
        if dateStringsLocked || MMMMyyyy == nil {
            return self.shortFMer.stringFromDate(self.date)
        } else {
            return MMMMyyyy!
        }
    }
    
    func calculateDateStrings() {
        dispatch_async(GlobalUserInitiatedQueue) { 
            self.MMMMddyyyy = self.longFMer.stringFromDate(self.date)
            self.MMMMyyyy = self.shortFMer.stringFromDate(self.date)
            self.dateStringsLocked = false
        }
    }
    
    func updateObjectAtSectionIndex(sectionIndex: Int) {
        self.delegate?.objectUpdated(self, inSectionIndex: sectionIndex)
    }
    
//    func greaterYDThan(entry: ZSPJournalEntry) -> Bool {
//        if self.year > {
//
//        }
//    }
    
}