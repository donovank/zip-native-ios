//
//  JournalEntryViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import MZFormSheetPresentationController

class JournalEntryViewController: UIViewController,
                                  UITextViewDelegate,
                                  UIScrollViewDelegate,
                                  UIImagePickerControllerDelegate,
                                  UINavigationControllerDelegate,
                                  LeanTaaSUIMixin,
                                  LeanTaaSThreadingMixin,
                                  ZSPMoodPickerDelegate {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var contentSizeView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleInfoStackView: UIStackView!
    
    @IBOutlet weak var addImageButton: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleTextFieldContainer: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activityCompletedView: UIView!
    @IBOutlet weak var activityLabelContainer: UIView!
    @IBOutlet weak var activityLabel: UILabel!
    
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var toolBarBottomContsraint: NSLayoutConstraint!
    @IBOutlet weak var textEntryView: UITextView!
    @IBOutlet weak var deleteIcon: UIBarButtonItem!
    @IBOutlet weak var copyIcon: UIBarButtonItem!
    @IBOutlet weak var currentMoodButton: UIBarButtonItem!
    
    
    // USING IUO: Don't want to have to unwrap the entire time, but I know this object will be set
    weak var entry: ZSPJournalEntry?
    
    var moodHasBeenSet = false
    
    weak var activity: ZSPActivityItem? {
        didSet {
            if activity != nil {
                isActivity = true
            } else {
                isActivity = false
            }
        }
    }
    var editingEntryIndexPath: NSIndexPath?
    
    var entryMood: ZSPMoodType?
    var saveOnDismiss = false
    
    var isActivity = false
    var placeholderIsActive = true {
        didSet {
            let tmp = !placeholderIsActive
            navigationItem.rightBarButtonItem?.enabled = tmp
            if tmp {
                navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
            } else {
                navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:0.4)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem?.enabled = false
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:0.4)
        
        titleTextField.autocapitalizationType = .Sentences
        let navbarImage = imageFromColor(UIColor.whiteColor())
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        
        let formatter = NSDateFormatter()
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSFontAttributeName : font]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
        }
        
        
        if let editEntry = entry {
            placeholderIsActive = false
            textEntryView.text = editEntry.text
            formatter.dateFormat = "MMMM dd YYYY"
            dateLabel.text = formatter.stringFromDate(NSDate())
            
            if editEntry.isFromActivity {
                titleInfoStackView.removeArrangedSubview(titleTextFieldContainer)
                titleTextFieldContainer.removeFromSuperview()
                activityLabel.text = editEntry.title
                self.isActivity = true
            } else {
                titleInfoStackView.removeArrangedSubview(activityCompletedView)
                titleInfoStackView.removeArrangedSubview(activityLabelContainer)
                activityCompletedView.removeFromSuperview()
                activityLabelContainer.removeFromSuperview()
                titleTextField.text = editEntry.title
            }
            
            if let photo = editEntry.photo?.image {
                // Will need to do delegate for updating photo
                imageView.image = photo
            }
            
            moodSelected(editEntry.mood)
            
        } else if let theActivity = activity {
            // set up to be activity
            deleteIcon.enabled = false
            deleteIcon.tintColor = UIColor.clearColor()
            copyIcon.enabled = false
            copyIcon.tintColor = UIColor.clearColor()
            titleInfoStackView.removeArrangedSubview(titleTextFieldContainer)
            titleTextFieldContainer.removeFromSuperview()
            formatter.dateFormat = "MMMM dd YYYY"
            dateLabel.text = formatter.stringFromDate(NSDate())
            activityLabel.text = theActivity.title
            
        } else {
            deleteIcon.enabled = false
            deleteIcon.tintColor = UIColor.clearColor()
            copyIcon.enabled = false
            copyIcon.tintColor = UIColor.clearColor()
            titleInfoStackView.removeArrangedSubview(activityCompletedView)
            titleInfoStackView.removeArrangedSubview(activityLabelContainer)
            activityCompletedView.removeFromSuperview()
            activityLabelContainer.removeFromSuperview()
            formatter.dateFormat = "MMMM dd YYYY"
            dateLabel.text = formatter.stringFromDate(NSDate())
        }
    
        textEntryView.delegate = self
        scrollView.delegate = self
        
//        titleTextField.becomeFirstResponder()
        
        textEntryView.textContainerInset = UIEdgeInsetsMake(10, 10, 0, 10)
        
        
        if entry == nil {
            textEntryView.text = "Tell me something..."
            textEntryView.textColor = UIColor.lightGrayColor()
        }
        
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
            placeholderIsActive = false
        }
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Tell me something..."
            textView.textColor = UIColor.lightGrayColor()
            placeholderIsActive = true
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.unregisterFromKeyboardNotifications()
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        guard let image = info[UIImagePickerControllerEditedImage] as? UIImage else {
            picker.dismissViewControllerAnimated(true, completion: { 
                let message = "There seems to have been a problem selecting that photo."
                self.displayErrorAlert(self, message: message, completion: nil)
            })
            return
        }
        
        imageView.contentMode = .ScaleAspectFit
        imageView.image = image
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func addPhoto(sender: AnyObject) {
//        view.endEditing(true)
//        titleTextField.resignFirstResponder()
        guard UIImagePickerController.isSourceTypeAvailable(.Camera) else {
            showImagePicker(UIImagePickerControllerSourceType.PhotoLibrary)
            return
        }
        let alertTitle = imageView.image == nil ? "Select Photo From:" : "Replace Photo From:"
        
        let sizedTitle = NSAttributedString(string: alertTitle, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(18)])
        
        
        let pickSource = UIAlertController(title: "", message: nil, preferredStyle: .ActionSheet)
        pickSource.setValue(sizedTitle, forKey: "attributedTitle")
        pickSource.addAction(UIAlertAction(title: "Photo Library", style: .Default, handler: { (alert) in self.showImagePicker(.PhotoLibrary) }))
        pickSource.addAction(UIAlertAction(title: "Camera", style: .Default, handler: { (alert) in self.showImagePicker(.Camera) }))
        pickSource.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))

//        presentViewControllerOverRoot(self, vc: pickSource, completion: nil)
//        rootViewController.presentViewController(pickSource, animated: true, completion: nil)
        presentOnDismiss(pickSource, completion: nil)
    }
    
    @IBAction func userDidClickCancel(sender: UIBarButtonItem) {
        
        if !placeholderIsActive {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
            
            if entry != nil {
                alert.addAction(UIAlertAction(title: "Save as Draft", style: .Default, handler: self.saveAsDraft ))
                alert.addAction(UIAlertAction(title: "Discard Edits", style: .Destructive, handler: self.revertChanges ))
                alert.addAction(UIAlertAction(title: "Return to Editing", style: .Cancel, handler: nil ))
            } else {
                alert.addAction(UIAlertAction(title: "Save Draft", style: .Default, handler: self.saveDraft ))
                alert.addAction(UIAlertAction(title: "Delete Draft", style: .Destructive, handler: self.deleteDraft ))
                alert.addAction(UIAlertAction(title: "Return to Editing", style: .Cancel, handler: nil ))
            }
            presentOnDismiss(alert, completion: nil)
        } else {
            self.view.endEditing(true)
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    override func disablesAutomaticKeyboardDismissal() -> Bool {
        return true
    }
    
    func showImagePicker(source: UIImagePickerControllerSourceType) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.modalPresentationStyle = UIModalPresentationStyle.CurrentContext
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = source
        if source == .Camera {
            imagePicker.cameraCaptureMode = .Photo
        }
        
//        presentViewControllerOverRoot(self, vc: imagePicker, completion: nil)
//        presentOnDismiss(imagePicker, completion: nil)
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    func saveDraft(action: UIAlertAction) {
        self.saveEntryAsDraft()
        view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func saveAsDraft(action: UIAlertAction) {
        self.saveEntryAsDraft()
        view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func deleteDraft(action: UIAlertAction) {
        view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func revertChanges(action: UIAlertAction) {
        view.endEditing(true)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func deleteEntry(sender: AnyObject) {
        
        let alert = UIAlertController(title: "Warning", message: "You're about to delete this journal entry.", preferredStyle: .Alert)

        alert.addAction(UIAlertAction(title: "Delete", style: .Destructive, handler: self.userDeletedEntry))
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil ))
        presentOnDismiss(alert, completion: nil)
    }
    
    func userDeletedEntry(action: UIAlertAction) {
        
        if let entry = self.entry where entry.isDraft {
            if let indexPath = editingEntryIndexPath {
                ZSPModelManager.DefaultModelManager.JournalManager.draftEntries.removeAtIndex(indexPath.row)
                dispatch_async(GlobalUserInitiatedQueue, { 
                    ZSPNetworkManager.Network.API.Journaling.DRAFT_API.deleteDraft(entry)
                })
            } else {
                print("NO IP TO DELETE DRAFT")
            }
            
        } else if let indexPath = editingEntryIndexPath {
            dispatch_async(GlobalUserInitiatedQueue, {
                ZSPModelManager.DefaultModelManager.JournalManager.deleteEntryAtIndexPath(indexPath)
            })
        } else if let entry = self.entry {
            print("ERROR WITH INDEXPATH")
            dispatch_async(GlobalUserInitiatedQueue, {
                ZSPModelManager.DefaultModelManager.JournalManager.deleteEntry(entry)
            })
        } else {
            print("ERROR WITH INDEXPATH")
            print("ERROR WITH ENTRY")
        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
   
    
    @IBAction func save(sender: AnyObject) {
        
        guard moodHasBeenSet else {
            saveOnDismiss = true
            moodHasBeenSet = true
            self.segueOnDismiss("PickCurrentMoodSegue", sender: sender)
            return
        }
        
        guard let theMood = entryMood else {
            saveOnDismiss = true
            moodHasBeenSet = true
            self.segueOnDismiss("PickCurrentMoodSegue", sender: sender)
            return
        }
        
        guard let bodyText = textEntryView.text where !bodyText.isEmpty && !placeholderIsActive else {
            let alert = UIAlertController(title: nil, message: "You can not save without body text.", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "Okay", style: .Default, handler: nil))
            self.presentOnDismiss(alert, completion: nil)
            return
        }
        
        // these lines are a bit redundant
        var theTitle: String? = nil
        if isActivity {
            if let txt = activityLabel.text {
                theTitle = txt
            }
        } else {
            if let txt = titleTextField.text {
                theTitle = txt
            }
        }
        
        if let updateEntry = entry {
            updateEntry.mood = theMood
            updateEntry.date = NSDate()
            updateEntry.text = placeholderIsActive ? "" : textEntryView.text
            if !(updateEntry.isFromActivity) {
                updateEntry.title = theTitle
            }
            // Temp
            updateEntry.photo?.image = imageView.image
            
            if let indexPath = editingEntryIndexPath {
                if updateEntry.isDraft {
                    updateEntry.isDraft = false
                    dispatch_async(GlobalUserInitiatedQueue) {
                        ZSPNetworkManager.Network.API.Journaling.addJournalEntry(updateEntry, wasDraft: true)
                    }
                    dispatch_async(GlobalUserInitiatedQueue, {
                        ZSPModelManager.DefaultModelManager.JournalManager.addNewEntry(updateEntry)
                    })
                    
                    ZSPModelManager.DefaultModelManager.JournalManager.draftEntries.removeAtIndex(indexPath.row)
                } else {
                    updateEntry.isDraft = false
                    dispatch_async(GlobalUserInitiatedQueue, {
                        updateEntry.updateObjectAtSectionIndex(indexPath.section)
                    })
                }
            } else {
                print("ERROR WITH INDEXPATH")
            }
        
        } else {
            let item = ZSPJournalEntry(isFromActivity: isActivity, isDraft: false, postMood: 1, editDate: NSDate(), body: bodyText)
            item.title = theTitle
            item.mood = theMood
            if let image = imageView.image {
                item.photo?.image = image
            }
            dispatch_async(GlobalUserInitiatedQueue) {
                ZSPNetworkManager.Network.API.Journaling.addJournalEntry(item)
            }
            dispatch_async(GlobalUserInitiatedQueue, {
                ZSPModelManager.DefaultModelManager.JournalManager.addNewEntry(item)
            })
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func saveEntryAsDraft() {
        
        var theTitle: String? = nil
        if isActivity {
            if let txt = activityLabel.text {
                theTitle = txt
            }
        } else {
            if let txt = titleTextField.text {
                theTitle = txt
            }
        }
        
        var theMood: ZSPMoodType
        if entryMood != nil {
            theMood = entryMood!
        } else {
            theMood = ZSPMoodType.Other
        }
        
        if let updateEntry = entry {
            
            updateEntry.mood = theMood
            updateEntry.date = NSDate()
            
            updateEntry.text = placeholderIsActive ? "" : textEntryView.text
            if !(updateEntry.isFromActivity) {
                updateEntry.title = theTitle
            }
            // Temp
            updateEntry.photo?.image = imageView.image
            
            if !updateEntry.isDraft {
                updateEntry.isDraft = true
                dispatch_async(GlobalUserInitiatedQueue, { 
                    ZSPNetworkManager.Network.API.Journaling.DRAFT_API.addDraftEntry(updateEntry, wasJournalEntry: true)
                    ZSPModelManager.DefaultModelManager.JournalManager.draftEntries.append(updateEntry)
                })
                dispatch_async(GlobalUserInitiatedQueue, {
                    ZSPModelManager.DefaultModelManager.JournalManager.deleteEntry(updateEntry)
                })
            } else {
                updateEntry.isDraft = true
                dispatch_async(GlobalUserInitiatedQueue, {
                    ZSPNetworkManager.Network.API.Journaling.DRAFT_API.addDraftEntry(updateEntry)
                    ZSPModelManager.DefaultModelManager.JournalManager.delegate?.refresh()
                })
            }
        } else {
            let bodyText = placeholderIsActive ? "" : textEntryView.text
            let item = ZSPJournalEntry(isFromActivity: isActivity, isDraft: true, postMood: 1, editDate: NSDate(), body: bodyText)
            item.title = theTitle
            item.mood = theMood
            if let image = imageView.image {
                item.photo?.image = image
            }
            dispatch_async(GlobalUserInitiatedQueue) {
                ZSPNetworkManager.Network.API.Journaling.DRAFT_API.addDraftEntry(item)
            }
            dispatch_async(GlobalUserInitiatedQueue, {
                ZSPModelManager.DefaultModelManager.JournalManager.draftEntries.append(item)
            })
        }
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "PickCurrentMoodSegue" {
            if let moodPickerView = segue.destinationViewController as? MoodSelectionPopoverController {
                moodPickerView.delegate = self
            }
            
            if let presentationSegue = segue as? MZFormSheetPresentationViewControllerSegue {
                if let prezController = presentationSegue.formSheetPresentationController.presentationController {
                    prezController.contentViewSize = CGSizeMake(200, 385)
                    prezController.shouldCenterVertically = true
                    prezController.shouldDismissOnBackgroundViewTap = true
                    if saveOnDismiss {
                        prezController.dismissalTransitionDidEndCompletionHandler = { (vc) in
                            self.save(self)
                        }
                    }
                }
            }
        }
        
    }
    
    func registerForKeyboardNotifications() {
        let center:  NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.addObserver(self, selector: #selector(JournalEntryViewController.keyboardWillShowOrHide), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.kbWillShow), name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.keyboardDidShowOrHide), name: UIKeyboardDidShowNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.keyboardDidShowOrHide), name: UIKeyboardDidHideNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.kbDidHide), name: UIKeyboardDidHideNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.displayWaitingAlertController), name: UIKeyboardDidHideNotification, object: nil)
        center.addObserver(self, selector: #selector(JournalEntryViewController.keyboardWillShowOrHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func unregisterFromKeyboardNotifications () {
        let center:  NSNotificationCenter = NSNotificationCenter.defaultCenter()
        center.removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        center.removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)

    }
    
    var alerts: [()->Void] = []
    var isKeyBoardVisible = false
    
    func kbWillShow() {
        isKeyBoardVisible = true
    }
    
    func kbDidHide() {
        isKeyBoardVisible = false
    }
    
    func displayWaitingAlertController() {
        for anon in alerts {
            anon()
        }
        alerts.removeAll()
    }
    
    func presentOnDismiss(vc: UIViewController, completion: (()->Void)?) {
        let show = { self.presentViewController(vc, animated: true, completion: completion) }
        alerts.append(show)
        view.endEditing(true)
        if !isKeyBoardVisible {
            displayWaitingAlertController()
        }
    }
    
    func segueOnDismiss(id: String, sender: AnyObject?) {
        let show = { self.performSegueWithIdentifier(id, sender: sender) }
        alerts.append(show)
        view.endEditing(true)
        if !isKeyBoardVisible {
            displayWaitingAlertController()
        }
    }
    
    func keyboardWillShowOrHide(notification:NSNotification){
        let userInfo = notification.userInfo
        var animationDuration:NSTimeInterval?
        var animationCurve:UIViewAnimationCurve?
        var keyboardEndFrame:CGRect?
        
        animationCurve = UIViewAnimationCurve(rawValue: (userInfo![UIKeyboardAnimationCurveUserInfoKey]?.integerValue)!)
        animationDuration = userInfo![UIKeyboardAnimationDurationUserInfoKey] as? NSTimeInterval
        keyboardEndFrame = userInfo![UIKeyboardFrameEndUserInfoKey]?.CGRectValue
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationCurve(animationCurve!)
        UIView.setAnimationDuration(animationDuration!)
        
        toolBarBottomContsraint.constant = self.view.frame.height - (keyboardEndFrame?.origin.y)!
    }
    
    func keyboardDidShowOrHide() {
        UIView.commitAnimations()
    }
    
    @IBAction func selectMood(sender: AnyObject) {
        self.segueOnDismiss("PickCurrentMoodSegue", sender: sender)
    }
    
    func moodSelected(mood: ZSPMoodType) {
        switch mood {
        case .VeryHappy:
            entryMood = .VeryHappy
            currentMoodButton.image = UIImage(named: "VeryHappyFilterIcon")
            currentMoodButton.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        case .Happy:
            entryMood = .Happy
            currentMoodButton.image = UIImage(named: "HappyFilterIcon")
            currentMoodButton.tintColor = UIColor(red:0.46, green:0.76, blue:0.85, alpha:1.0)
        case .Neutral:
            entryMood = .Neutral
            currentMoodButton.image = UIImage(named: "NeutralFilterIcon")
            currentMoodButton.tintColor = UIColor(red:0.65, green:0.79, blue:0.84, alpha:1.0)
        case .Sad:
            entryMood = .Sad
            currentMoodButton.image = UIImage(named: "SadFilterIcon")
            currentMoodButton.tintColor = UIColor(red:0.67, green:0.71, blue:0.73, alpha:1.0)
        case .VerySad:
            entryMood = .VerySad
            currentMoodButton.image = UIImage(named: "VerySadFilterIcon")
            currentMoodButton.tintColor = UIColor(red:0.52, green:0.52, blue:0.52, alpha:1.0)
        case .Other:
            break
        }
    }
    
}