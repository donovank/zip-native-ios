//
//  ZSPManager.swift
//  ZSP
//
//  Created by Donovan King on 8/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol ZSPManager {
    
    static var Default: Self { get }
    
    func clearForSignOff() 
    
}