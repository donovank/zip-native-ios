//
//  ReminderHeaderView.swift
//  ZSP
//
//  Created by Donovan King on 7/19/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit

class ReminderHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var backgroundContainer: UIView!
    
    var isSetup = false
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    func headerSetup(dateString: String) {
        
        dateLabel.text = dateString
        if !isSetup {
            backgroundContainer.layer.cornerRadius = 4
            self.backgroundView?.backgroundColor = UIColor.clearColor()
            circleView.layer.cornerRadius = 4
        }
    }

}
