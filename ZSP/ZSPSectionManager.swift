//
//  ZSPSectionManager.swift
//  ZSP
//
//  Created by Donovan King on 9/15/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ZSPSectionManager {
    
    func objectUpdated(object: AnyObject, inSectionIndex sectionIndex: Int)
    
}