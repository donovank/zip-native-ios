//
//  ReminderActivityTableViewPopupController.swift
//  ZSP
//
//  Created by Donovan King on 8/1/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class ReminderActivityTableViewPopupController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    let MODEL = ZSPModelManager.DefaultModelManager.SafetyPlan.getAllActivities()
    var selected: ZSPActivityItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MODEL.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = MODEL[indexPath.row].title
//        cell.imageView?.frame = CGRectMake(0, 0, 10, 10)
//
//        cell.imageView?.image = cellImage(MODEL[indexPath.row].mood)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected = MODEL[indexPath.row]
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "You can create activities in your Safety Plan Step One."
        let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
                     NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
//    func cellImage(mood: Int) -> UIImage? {
//        
//        switch mood {
//        case 0:
//            return UIImage(named: "NCSad")
//        case 1:
//            return UIImage(named: "NCLonely")
//        case 2:
//            return UIImage(named: "NCWorried")
//        case 3:
//            return UIImage(named: "NCAngry")
//        case 4:
//            return UIImage(named: "NCOther")
//        default:
//            return UIImage(named: "NCOther")
//        }
//    }
    
}