//
//  UserDetailViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import SWRevealViewController

class UserDetailViewController: UIViewController, UITextFieldDelegate, UIScrollViewDelegate, SWRevealViewControllerDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var nameBGView: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var phoneBGView: UIView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailBGView: UIView!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var detailsBGView: UIView!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var signoutButton: UIButton!
 
    var activeField: UITextField?
    
    let formatter = NSDateFormatter()
    
    var metric = false {
        didSet {
            if metric {
                heightLabel.text = "Height (m)"
                weightLabel.text = "Weight (kg)"
            } else {
                heightLabel.text = "Height (ft|in)"
                weightLabel.text = "Weight (lbs)"
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        setupView()
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
        }
        
        
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)
        
        genderTextField.delegate = self
        weightTextField.delegate = self
        heightTextField.delegate = self
        birthdayTextField.delegate = self
        emailField.delegate = self
        nameField.delegate = self
        scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive
        
        let datePicker = UIDatePicker()
        let today = NSDate()
        datePicker.date = today
        datePicker.maximumDate = today
        datePicker.addTarget(self, action: #selector(updateDateTextField), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.datePickerMode = .Date
        birthdayTextField.inputView = datePicker
        
        formatter.dateStyle = NSDateFormatterStyle.LongStyle
        
        if let isMetric = NSLocale.currentLocale().objectForKey(NSLocaleUsesMetricSystem) as? Bool {
            metric = isMetric
        }
        
        let heightPicker = LeanTaaSHeightPicker()
        heightPicker.updateLabel = heightTextField
        heightPicker.isMetric = metric
        heightTextField.inputView = heightPicker
        
        let weightPicker = LeanTaaSWeightPicker()
        weightPicker.updateLabel = weightTextField
        weightPicker.isMetric = metric
        weightTextField.inputView = weightPicker
        
        if let dob = ZSPModelManager.DefaultModelManager.User.dateOfBirth {
            let dobString = formatter.stringFromDate(dob)
            birthdayTextField.text = dobString
        }
        
        if let gender = ZSPModelManager.DefaultModelManager.User.gender {
            genderTextField.text = gender
        }
        
        if let number = ZSPModelManager.DefaultModelManager.User.mobileNumber {
            let numberString = number.toNational()
            phoneLabel.text = numberString
        }

    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.registerForKeyboardNotifications()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.deregisterFromKeyboardNotifications()
    }
    
    @IBAction func userSwipedDown(sender: AnyObject) {
        view.endEditing(true)
    }

    @IBAction func signOut(sender: AnyObject) {
        ZSPModelManager.DefaultModelManager.signOutUser()
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        userSwipedDown(self)
        if(position == .Left) {
            scrollView.userInteractionEnabled = true
        } else {
            scrollView.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            scrollView.userInteractionEnabled = true
        } else {
            scrollView.userInteractionEnabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func updateDateTextField() {
        guard let picker = birthdayTextField.inputView as? UIDatePicker else {
            return
        }
        birthdayTextField.text = formatter.stringFromDate(picker.date)
    }
    
    func setupView() {
        
        if let img = UIImage(named: "SigninBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        let bgViews = [nameBGView, phoneBGView, emailBGView, signoutButton]
        
        for bgView in bgViews {
            bgView.clipsToBounds = true
            bgView.layer.cornerRadius = bgView.bounds.height/2
        }
        
        detailsBGView.clipsToBounds = true
        detailsBGView.layer.cornerRadius = 10
    }
    
    func registerForKeyboardNotifications() {
        //Adding notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UserDetailViewController.keyboardWasShown), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UserDetailViewController.keyboardWillBeHidden), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func deregisterFromKeyboardNotifications() {
        //Removing notifies on keyboard appearing
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func keyboardWasShown(notification: NSNotification) {
        //Need to calculate keyboard exact size due to Apple suggestions
        self.scrollView.scrollEnabled = true
        let info : NSDictionary = notification.userInfo!
        guard let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size else { return }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardSize.height + 10), 0.0)
        
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        
        var aRect : CGRect = self.view.frame
        aRect.size.height -= keyboardSize.height
        if let activeFieldPresent = activeField{
            if (!CGRectContainsPoint(aRect, activeFieldPresent.frame.origin)){
                self.scrollView.scrollRectToVisible(activeFieldPresent.frame, animated: true)
            }
        }
    }
    
    
    func keyboardWillBeHidden(notification: NSNotification) {
        //Once keyboard disappears, restore original positions
        let info : NSDictionary = notification.userInfo!
        guard let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue().size else { return }
        let contentInsets : UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, (-keyboardSize.height - 10), 0.0)
        self.scrollView.contentInset = contentInsets
        self.scrollView.scrollIndicatorInsets = contentInsets
        self.view.endEditing(true)
        self.scrollView.scrollEnabled = false
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        activeField = textField
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        activeField = nil
    }
    
}