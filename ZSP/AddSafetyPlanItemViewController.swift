//
//  AddSafetyPlanItemViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class AddSafetyPlanItemViewController: UIViewController, UITextFieldDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var navBar: UINavigationBar!
    
    var saveThis: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonEnabled(false)
        textField.delegate = self
        textField.autocapitalizationType = .Sentences
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupPopup()
    }
    
    @IBAction func save(sender: AnyObject) {
        textField.resignFirstResponder()
        saveThis = textField.text
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        textField.resignFirstResponder()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func buttonEnabled(val: Bool) {
        saveButton.enabled = val
        if val {
            saveButton.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        } else {
            saveButton.tintColor = UIColor.lightGrayColor()
        }
    }
    
    func setupPopup() {
//        saveButton.layer.cornerRadius = 10
        curveView(backgroundView, byRoundingCorners: [.BottomLeft, .BottomRight], withRadius: 6.0)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if var fullString = textField.text {
            if range.length > 0 {
                fullString.deleteCharactersInRange(range)
            } else {
                fullString.insertContentsOf(string.characters, at: fullString.startIndex.advancedBy(range.location))
            }
            
            if fullString.characters.count >= 1 {
                buttonEnabled(true)
            } else {
                buttonEnabled(false)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
