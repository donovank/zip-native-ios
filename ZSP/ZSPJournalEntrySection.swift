//
//  ZSPJournalEntrySection.swift
//  ZSP
//
//  Created by Donovan King on 9/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


class ZSPJournalEntrySection: ZSPDateCompareable, ZSPSectionManager, LeanTaaSThreadingMixin {
    
    var delegate: ZSPJournalEntrySectionDelegate
    var updating = false
    let headerFormater = NSDateFormatter()
    
    var entries = [ZSPJournalEntry]() {
        didSet {
            if !updating {
                delegate.sectionDone(self)
            } else {
                self.updating = false
            }
        }
    }

    init(entries: [ZSPJournalEntry], delegate: ZSPJournalEntrySectionDelegate){
        self.entries = entries
        self.delegate = delegate
        for entry in self.entries {
            entry.delegate = self
        }
        sortList(entries)
    }
    
    func objectUpdated(object: AnyObject, inSectionIndex sectionIndex: Int) {
        self.updating = true
        dispatch_async(GlobalUserInitiatedQueue) { 
            guard let updatedEntry = object as? ZSPJournalEntry where !updatedEntry.isDraft else {
                self.updating = false
                return
            }
            self.updateItem(updatedEntry, sectionIndex: sectionIndex)
        }
    }
    
    func addItem(item: ZSPJournalEntry) {
        item.delegate = self
        var tmp = entries
        tmp.append(item)
        sortList(tmp)
    }
    
    func sortList(tmpList: [ZSPJournalEntry]) {
        entries = tmpList.sort({ (a, b) -> Bool in
            if .OrderedDescending == a.date.compare(b.date) {
                return true
            } else {
                return false
            }
        })
    }

    var date: NSDate {
        get {
            // TEMP TEST FIX
            return entries[0].date
        }
    }
    
    var day: Int {
        get {
            return NSCalendar.currentCalendar().component(.Day, fromDate: self.date)
        }
    }
    
    var month: Int {
        get {
            return NSCalendar.currentCalendar().component(.Month, fromDate: self.date)
        }
    }
    
    var year: Int {
        get {
            return NSCalendar.currentCalendar().component(.Year, fromDate: self.date)
        }
    }
  
    func getDateHeaderString() -> String {
        headerFormater.dateFormat = "MMMM, yyyy"
        return headerFormater.stringFromDate(date)
    }
    
    /*
     * This is a horrible function. Probably can remove this at some point.
     * I built it just in case it's absolutely needed somewhere.
     */
    func removeItem(item: ZSPJournalEntry, sectionIndex: Int) {
        var index = 0
        for entry in self.entries {
            if entry === item {
                self.removeItemAtIndex(index, sectionIndex: sectionIndex)
            } else {
                index = index + 1
            }
        }
    }
    
    func removeItemAtIndex(index: Int, sectionIndex: Int) {
        var tmp = self.entries
        let entry = tmp.removeAtIndex(index)
        ZSPNetworkManager.Network.API.Journaling.deleteEntry(entry)
        entry.delegate = nil
        if tmp.isEmpty {
            self.delegate.sectionIsEmpty(sectionIndex)
        } else {
            self.entries = tmp
        }
    }
    
    func updateItem(item: ZSPJournalEntry, sectionIndex: Int) {
        var index = 0
        for entry in self.entries {
            if entry === item {
                self.updateItemAtIndex(index, sectionIndex: sectionIndex)
            } else {
                index = index + 1
            }
        }
    }
    
    func updateItemAtIndex(index: Int, sectionIndex: Int) {
        var tmp = self.entries
        let entry = tmp.removeAtIndex(index)
        entry.delegate = nil
        if tmp.isEmpty {
            self.delegate.sectionIsEmpty(sectionIndex)
        } else {
            self.entries = tmp
        }
        dispatch_async(GlobalUserInitiatedQueue) { 
            ZSPModelManager.DefaultModelManager.JournalManager.addNewEntry(entry)
        }
    }
    
}