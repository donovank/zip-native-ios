//
//  MoodSelectionViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/31/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class MoodSelectionViewController: UIViewController, LeanTaaSUIMixin {
    
    @IBOutlet weak var sadButton: UIButton!
    @IBOutlet weak var lonelyButton: UIButton!
    @IBOutlet weak var worriedButton: UIButton!
    @IBOutlet weak var angryButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    @IBOutlet weak var sadImageView: UIImageView!
    @IBOutlet weak var sadLabel: UILabel!
    @IBOutlet weak var lonelyImageView: UIImageView!
    @IBOutlet weak var lonelyLabel: UILabel!
    @IBOutlet weak var worriedImageView: UIImageView!
    @IBOutlet weak var worriedLabel: UILabel!
    @IBOutlet weak var angryImageView: UIImageView!
    @IBOutlet weak var angryLabel: UILabel!
    @IBOutlet weak var otherImageView: UIImageView!
    @IBOutlet weak var otherLabel: UILabel!
    
    
    var activityString: String!
    
    weak var textEntryVC: AddActivityItemViewController?
    
    var selectedMood: Int? {
        didSet {
            if selectedMood != nil {
                saveButton.enabled = true
            } else {
                saveButton.enabled = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saveButton.enabled = false
        
    }
    
    override func viewDidAppear(animated: Bool) {
        textEntryVC?.closeExtender()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupButtons()
    }
    
    @IBAction func moodSelected(sender: UIButton) {
        
        let buttons = [(button: sadButton, image: sadImageView, label: sadLabel),
                       (button: lonelyButton, image: lonelyImageView, label: lonelyLabel),
                       (button: worriedButton, image: worriedImageView, label: worriedLabel),
                       (button: angryButton, image: angryImageView, label: angryLabel),
                       (button: otherButton, image: otherImageView, label: otherLabel),]
        for button in buttons {
            if button.button !== sender {
                button.button.selected = false
                button.image.tintColor = UIColor.lightGrayColor()
                button.label.textColor = UIColor.lightGrayColor()
            } else {
                button.button.selected = !button.button.selected
                button.image.tintColor = button.button.selected ? selectedBlue() : UIColor.lightGrayColor()
                button.label.textColor = button.button.selected ? selectedBlue() : UIColor.lightGrayColor()
            }
        }
        
        switch (sender, sender.selected) {
        case (sadButton, true):
            selectedMood = 1
        case (lonelyButton, true):
            selectedMood = 2
        case (worriedButton, true):
            selectedMood = 3
        case (angryButton, true):
            selectedMood = 4
        case (otherButton, true):
            selectedMood = 5
        default:
            selectedMood = nil
        }
        
    }
    
    func setupButtons() {
        let bgColorH = selectedBlue().colorWithAlphaComponent(0.1)
//        let bgColorS = UIColor(red:0.00, green:0.48, blue:1.00, alpha:0.25)
        let bgImageS = imageFromColor(bgColorH)
//        let bgImageH = imageFromColor(bgColorS)
        let buttons = [(button: sadButton, image: sadImageView, label: sadLabel),
                       (button: lonelyButton, image: lonelyImageView, label: lonelyLabel),
                       (button: worriedButton, image: worriedImageView, label: worriedLabel),
                       (button: angryButton, image: angryImageView, label: angryLabel),
                       (button: otherButton, image: otherImageView, label: otherLabel),]

        for button in buttons {
            button.button.clipsToBounds = true
            button.button.setBackgroundImage(bgImageS, forState: .Highlighted)
//            button.setBackgroundImage(bgImageH, forState: .Selected)
            button.button.layer.cornerRadius = 6
            
            button.image.tintColor = UIColor.lightGrayColor()
            button.label.textColor = UIColor.lightGrayColor()
        }
    }
    
    @IBAction func save(sender: UIBarButtonItem) {
        
        createItemWithMood(selectedMood!)
    }
    
    
    func createItemWithMood(val: Int) {
        let item = ZSPActivityItem(userID: "", title: activityString, mood: val, reminder: false)
        ZSPModelManager.DefaultModelManager.SafetyPlan.currentFilter = val
        ZSPModelManager.DefaultModelManager.SafetyPlan.addNewActvity(item)
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}