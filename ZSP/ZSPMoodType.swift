//
//  ZSPMoodType.swift
//  ZSP
//
//  Created by Donovan King on 7/27/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

// 1 = very sad
// 2 = sad
// 3 = neutral
// 4 = happy
// 5 = very happy

enum ZSPMoodType: Int {
    
    case VerySad = 1
    case Sad = 2
    case Neutral = 3
    case Happy = 4
    case VeryHappy = 5
    
    case Other = 0
    
    static func fromNumber(val: Int) -> ZSPMoodType {
        switch val {
        case 1:
            return .VerySad
        case 2:
            return .Sad
        case 3:
            return .Neutral
        case 4:
            return .Happy
        case 5:
            return .VeryHappy
        default:
            return .Other
        }
    }
    
}