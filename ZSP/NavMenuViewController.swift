//
//  NavMenuViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit

class NavMenuViewController: UIViewController {
    
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var metricsContainerView: UIView!
    @IBOutlet weak var toolsContainerView: UIView!
    @IBOutlet weak var metricsLabel: UILabel!
    @IBOutlet weak var toolsLabel: UILabel!
    
    var isSetup = false
    
  
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
//        view.layoutIfNeeded()
        if !isSetup {
            addBottomBorder(toolsLabel)
            addBottomBorder(metricsLabel)
            addBottomBorder(infoLabel)
            isSetup = true
        }
    }
    
    @IBAction func goToAppSettings(sender: AnyObject) {
//        let alertController = UIAlertController (title: nil, message: "Go to Settings?", preferredStyle: .Alert)
//        
//        let settingsAction = UIAlertAction(title: "Settings", style: .Default) { (_) -> Void in
//            if let url = NSURL(string: UIApplicationOpenSettingsURLString) {
//                UIApplication.sharedApplication().openURL(url)
//            }
//        }
//        
//        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
//        alertController.addAction(settingsAction)
//        alertController.addAction(cancelAction)
//        
//        presentViewController(alertController, animated: true, completion: nil)
        
        performSegueWithIdentifier("UserDetailSegue", sender: self)
    }
    
    func addBottomBorder(view: UIView) {
        let topBorderLayer = CALayer()
        let thickness = CGFloat(1)
        topBorderLayer.backgroundColor = UIColor(red: 63/255, green: 210/255, blue: 1, alpha: 1).CGColor
        topBorderLayer.frame = CGRectMake(0, CGRectGetHeight(view.frame) - thickness, CGRectGetWidth(view.frame), thickness)
        view.layer.addSublayer(topBorderLayer)
    }
    
}

