//
//  ZSPSafetyPlanItem.swift
//  ZSP
//
//  Created by Donovan King on 7/30/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPSafetyPlanItem: ZSPModelObject {
    
    var id: String?
    let userID: String
    let title: String
    var phoneNumber: String?
    
    init(userID: String, title: String) {
        self.userID = userID
        self.title = title
    }
}