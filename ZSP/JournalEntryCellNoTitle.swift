//
//  JournalEntryCellNoTitle.swift
//  ZSP
//
//  Created by Donovan King on 9/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class JournalEntryCellNoTitle: UITableViewCell {
    
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var moodIcon: UIImageView!
    @IBOutlet weak var previewLabel: UILabel!
    @IBOutlet weak var imageIndicator: UIImageView!
    
    let indicator = UIImage(named: "imageIndicator")
    
    var mood: ZSPMoodType? {
        didSet {
            if let newMood = mood {
                moodSelected(newMood)
            }
        }
    }
    
    var showPhotoIndicator = false {
        didSet {
            if showPhotoIndicator {
                imageIndicator.image = indicator
            } else {
                imageIndicator.image = nil
            }
        }
    }
    
    func moodSelected(mood: ZSPMoodType) {
        switch mood {
        case .VeryHappy:
            moodIcon.image = UIImage(named: "VeryHappyFilterIcon")
            moodIcon.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        case .Happy:
            moodIcon.image = UIImage(named: "HappyFilterIcon")
            moodIcon.tintColor = UIColor(red:0.46, green:0.76, blue:0.85, alpha:1.0)
        case .Neutral:
            moodIcon.image = UIImage(named: "NeutralFilterIcon")
            moodIcon.tintColor = UIColor(red:0.65, green:0.79, blue:0.84, alpha:1.0)
        case .Sad:
            moodIcon.image = UIImage(named: "SadFilterIcon")
            moodIcon.tintColor = UIColor(red:0.67, green:0.71, blue:0.73, alpha:1.0)
        case .VerySad:
            moodIcon.image = UIImage(named: "VerySadFilterIcon")
            moodIcon.tintColor = UIColor(red:0.52, green:0.52, blue:0.52, alpha:1.0)
        case .Other:
            break
        }
    }
}