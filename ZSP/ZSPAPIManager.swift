//
//  ZSPAPIManager.swift
//  ZSP
//
//  Created by Donovan King on 8/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ZSPAPIManager {
    
    static var API: Self { get }
    
    func loadModel()
    
}