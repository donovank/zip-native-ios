//
//  LTWeightPicker.swift
//  ZSP
//
//  Created by Donovan King on 8/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class LeanTaaSWeightPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var weights: [Int]
    var isMetric = false
    
    var selectedWeight = 1
    var selectedString: String {
        get {
            if isMetric {
                return String(selectedWeight) + " kg"
            } else {
                return String(selectedWeight) + " lbs"
            }
        }
    }
    
    weak var updateLabel: UITextField?
    
    init() {
        weights = [Int]()
        for num in 1...1400 {
            weights.append(num)
        }
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        self.delegate = self
        self.dataSource = self
    }
    
    override init(frame: CGRect) {
        weights = [Int]()
        for num in 1...1400 {
            weights.append(num)
        }
        super.init(frame: frame)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        weights = [Int]()
        for num in 1...1400 {
            weights.append(num)
        }
        super.init(coder: aDecoder)
        self.delegate = self
        self.dataSource = self
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return weights.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if isMetric {
            return String(weights[row]) + " kg"
        } else {
            return String(weights[row]) + " lbs"
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedWeight = weights[row]
        
        updateLabel?.text = selectedString
    }
}