//
//  ZSPJournalAPI.swift
//  ZSP
//
//  Created by Donovan King on 9/21/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPJournalAPI: ZSPAPIManager, LeanTaaSThreadingMixin {
    
    static var API = ZSPJournalAPI()
    var DRAFT_API = ZSPJournalDraftAPI.API
    
    private init() { }
    
    private var page_number = 0
    private var _onLastPage = false
    var onLastPage: Bool {
        get {
            return _onLastPage
        }
    }
    
    func getNextPage(handler: ((Void) -> Void)?) {
        if !_onLastPage {
            page_number += 1
            loadModelWithHandle(handler)
        } else {
            handler?()
        }
    }
    
    func loadModel() {
        self.loadModelWithHandle(nil)
    }
    
    func loadModelWithHandle(handler: ((Void) -> Void)? = nil) {
        let URL = generateURLWithParams(page_number, search: nil)
        print(URL)
        Alamofire.request(.GET,
                          URL,
                          parameters: nil,
                          encoding: .JSON,
                          headers: TOKEN_HEADERS)
        .validate()
        .responseJSON { (res) in
            switch res.result {
            case .Success:
                if let value = res.result.value {
                    dispatch_async(self.GlobalUserInitiatedQueue, {
                        self.convertFromJSON(value, handler: handler)
                    })
                } else {
                    print(res)
                    print("ERROR reading journal entry JSON")
                }
            case .Failure(let error):
                    print("Error getting journal entries")
                    print(res)
                    print(error)
            }
        }
    }
    
    func addJournalEntry(entry: ZSPJournalEntry, wasDraft: Bool = false) {
        let URL = ZSPNetworkManager.baseURL + "/zsp/journal/addUpdateJournal"
        
        let titleOrBlank = entry.title == nil ? "" : entry.title!
        
        var params: [String : AnyObject] =
                    ["user" : CURRENT_USER_NAME,
                     "title" : titleOrBlank,
                     "desc" : entry.text,
                     "feeling" : entry.mood.rawValue,
                     "fromActivity" : entry.isFromActivity]
        
        
        
        if let id = entry.id where !id.isEmpty {
            if wasDraft {
                params["draftId"] = id
            } else {
                params["journalId"] =  id
            }
        }
        
        Alamofire.request(.POST,
                          URL,
                          parameters: params,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
        .responseJSON { (res) in
            switch res.result{
            case .Success:
                print("created draft response")
                print(res)
                if let value = res.result.value {
                    if let dataFromString = value.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
                        let json = JSON(data: dataFromString)
                        if let id = json["id"].string {
                            entry.id = id
                            print(id)
                        } else {
                            print(res)
                            print("ERROR GETTING ID VALUE FROM journal SAVE")
                            
                        }
                    } else {
                        print(res)
                        print("ERROR DECODING RES JSON FROM journal SAVE")
                    }
                    
                } else {
                    print(res)
                    print("ERROR reading journal Reply JSON")
                }
            case .Failure(let error):
                print("Error from add journal entry reply.")
                print(res)
                print(error)
            }
        }
        
        
    }
    
    func uploadPhotoWithId(journalId: String) {
        
        
    }
    
    private func convertFromJSON(jsonString: AnyObject, handler: ((Void) -> Void)? = nil) {
        let json = JSON(jsonString)
        
        var entries = [ZSPJournalEntry]()
        for entry in json["journals"].arrayValue {
            let entryData = entry["journals"]
            
            let title: String? = entryData["title"].stringValue == "" ? nil : entryData["title"].stringValue
            let text = entryData["desc"].stringValue
            let mood = entryData["feeling"].intValue
            let isActivity = entryData["activityFl"].boolValue
            let seconds = Double(entryData["date"]["$date"].intValue)/1000
            let date = NSDate(timeIntervalSince1970: seconds)
            let id = entryData["_id"]["$oid"].stringValue
            let photoID = entryData["photoId"].string
            
            let loadedEntry = ZSPJournalEntry(isFromActivity: isActivity,
                                              isDraft: false,
                                              postMood: mood,
                                              editDate: date,
                                              body: text)
            loadedEntry.title = title
            loadedEntry.id = id
            loadedEntry.photoID = photoID
            
            entries.append(loadedEntry)
        }
        if entries.count > 0 {
            if entries.count < 20 {
                self._onLastPage = true
            }
            handler?()
            dispatch_async(GlobalUserInitiatedQueue) {
                ZSPModelManager.DefaultModelManager.JournalManager.addLoadedEntries(entries)
            }
        }
    }
    
    func deleteEntry(entry: ZSPJournalEntry) {
        let URL = ZSPNetworkManager.baseURL + "/zsp/journal/deleteJournal"
        
        guard let theID = entry.id else {
            print("Journal Entry has no ID")
            return
        }
        
        let params = ["user" : CURRENT_USER_NAME,
                      "journalId" : theID]
        
        Alamofire.request(.POST,
            URL,
            parameters: params,
            encoding: .JSON,
            headers: ZSPNetworkManager.Network.getTokenHeader())
            .responseJSON { (res) in
                print(res)
        }

        
    }
    
    private func generateURLWithParams(page: Int?, search: String?) -> String {
        var URLString = ZSPNetworkManager.baseURL + "/zsp/journal/getDetails/" + CURRENT_USER_NAME
        if page == nil && search == nil {
            return URLString
        }
        URLString += "?"
        if let pageNum = page {
            URLString += "page=\(pageNum)"
        }
        if let searchFor = search?.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
            if page != nil {
                URLString += "&"
            }
            URLString += "search=\(searchFor)"
        }
        return URLString
    }
    
}