//
//  LTDateFormatMixin.swift
//  ZSP
//
//  Created by Donovan King on 9/15/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol LeanTaaSDateFormatMixin {
    
}

extension LeanTaaSDateFormatMixin {
//    
//    func getLocalMDY(mLength: DateFormatType, dLength: DateFormatType, yLength: DateFormatType) -> String {
//        
//        var dateFormat: String
//        switch NSLocale.currentLocale().localeIdentifier {
//        case "en_US": dateFormat = "MMM d"
//            ...
//        
//        }
//    }
    
}

enum DateFormatType {
    case Short
    case Abbreviated
    case Long
}