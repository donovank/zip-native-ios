//
//  ZSPReminderManager.swift
//  ZSP
//
//  Created by Donovan King on 8/1/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPReminderManager: ZSPReminderSectionDelegate {
    
    var delegate: ZSPModelManagerDelegate?
    var medListDelegate: ZSPModelManagerDelegate?
    var otherListDelegate: ZSPModelManagerDelegate?
    private var durringBuild = true
    
    private var allReminders = [ZSPReminder]() {
        didSet {
            buildSections()
        }
    }
    
    var allMedicationItems = [ZSPMedicationItem]() {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                self.medListDelegate?.refresh()
            }
        }
    }
    
    func addNewMedication(name: String) -> ZSPMedicationItem {
        let med = ZSPMedicationItem(userID: "", title: name)
        var tmp = allMedicationItems
        tmp.append(med)
        allMedicationItems = tmp.sort({ $0.title < $1.title})
        return med
    }
    
    func removeMedication(med: ZSPMedicationItem) -> Bool {
        guard let index = allMedicationItems.indexOf({ (item) -> Bool in
            if item === med { return true } else { return false }
        }) else { return false }
        var tmp = allMedicationItems
        tmp.removeAtIndex(index)
        allMedicationItems = tmp.sort({ $0.title < $1.title})
        return true
    }
    
    var allOtherItems = [ZSPOtherReminderItem]() {
        didSet {
            dispatch_async(dispatch_get_main_queue()) {
                self.otherListDelegate?.refresh()
            }
        }
    }
    
    func addNewOtherItem(name: String) -> ZSPOtherReminderItem {
        let otherItem = ZSPOtherReminderItem(userID: "", title: name)
        var tmp = allOtherItems
        tmp.append(otherItem)
        allOtherItems = tmp.sort({ $0.title < $1.title})
        return otherItem
    }
    
    func removeOtherItem(item: ZSPOtherReminderItem) -> Bool {
        guard let index = allOtherItems.indexOf({ (tmp) -> Bool in
            if tmp === item { return true } else { return false }
        }) else { return false }
        allOtherItems.removeAtIndex(index)
        allOtherItems = allOtherItems.sort({ $0.title < $1.title})
        return true
    }
    
    private var sections = [ZSPReminderSection]() {
        didSet {
            durringBuild = false
            dispatch_async(dispatch_get_main_queue()) {
                self.delegate?.refresh()
            }
        }
    }
    
    func getSectionCount() -> Int {
        return sections.count
    }
    
    private func setUp(list: [ZSPReminder]) {
        allReminders = list.sort({ (a, b) -> Bool in
            if .OrderedAscending == a.date.compare(b.date) {
                return true
            } else {
                return false
            }
        })
    }
    
    func countForSection(index: Int) -> Int {
        return sections[index].reminders.count
    }
    
    private func buildSections() {
        durringBuild = true
        let now = NSDate()
        var last = (d: NSCalendar.currentCalendar().component(.Day, fromDate: now),
                    m: NSCalendar.currentCalendar().component(.Month, fromDate: now),
                    y: NSCalendar.currentCalendar().component(.Year, fromDate: now))
        var tmpSections = [ZSPReminderSection]()
        var tmpList = [ZSPReminder]()
        for reminder in allReminders {
            if reminder.hasDifferentDateThan(last) {
                if tmpList.isEmpty {
                    last.d = reminder.day
                    last.m = reminder.month
                    last.y = reminder.year
                    tmpList.append(reminder)
                } else {
                    tmpSections.append(ZSPReminderSection(newReminders: tmpList, delegate: self))
                    last.d = reminder.day
                    last.m = reminder.month
                    last.y = reminder.year
                    tmpList = [reminder]
                }
            } else {
               tmpList.append(reminder)
            }
        }
        sectionSort(tmpSections)
    }
    
    internal func sectionDone(sender: ZSPReminderSection) {
        guard !durringBuild else {
            return
        }
        dispatch_async(dispatch_get_main_queue()) {
            self.delegate?.refresh()
        }
    }
    
    internal func sectionIsEmpy(index: Int) {
        sections.removeAtIndex(index)
    }
    
    private func sectionSort(list: [ZSPReminderSection]) {
        sections = list.sort({ (a, b) -> Bool in
            if .OrderedAscending == a.date.compare(b.date) {
                return true
            } else {
                return false
            }
        })
    }
    
    func addNewReminder(item: ZSPReminder) {
        var tmpSections = sections
        for section in tmpSections {
            if !item.hasDifferentDateThan(section) {
                section.addItem(item)
                return
            }
        }
        durringBuild = true
        let newSection = ZSPReminderSection(newReminders: [item], delegate: self)
        tmpSections.append(newSection)
        sectionSort(tmpSections)
    }
    
    func deleteReminder(reminder: ZSPReminder) {
        for (index, section) in sections.enumerate() {
            if !reminder.hasDifferentDateThan(section) {
                section.removeItem(reminder, sectionIndex: index)
                return
            }
        }
    }
    
    func deleteReminderAtIndexPath(path: NSIndexPath) {
        sections[path.section].removeItemAtIndex(path.row, sectionIndex: path.section)
    }
    
    func deleteReminderInSection(section: Int, atRow row: Int) {
        sections[section].removeItemAtIndex(row, sectionIndex: section)
    }
    
    func headerStringForSection(section: Int) -> String {
        return sections[section].getDateHeaderString()
    }
    
    func getReminderForIndexPath(indexPath: NSIndexPath) -> ZSPReminder {
        return sections[indexPath.section].reminders[indexPath.row]
    }
    
    func getReminderInSection(section: Int, atRow row: Int) -> ZSPReminder {
        return sections[section].reminders[row]
    }
    
}
