//
//  NewReminderSelectedLabelViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class NewReminderSelectedLabelViewController: UIViewController {
    
    var text: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    
    override func viewDidLoad() {
        titleLabel.text = text
    }
    
}