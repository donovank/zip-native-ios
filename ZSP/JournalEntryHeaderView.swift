//
//  JournalEntryHeaderView.swift
//  ZSP
//
//  Created by Donovan King on 7/28/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class JournalEntryHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    
    func headerSetup(title: String) {
        background.layer.cornerRadius = 4
        self.backgroundView?.backgroundColor = UIColor.clearColor()
        background.backgroundColor = UIColor(red:0.00, green:0.57, blue:0.75, alpha:0.5)
        titleLabel.text = title
        titleLabel.textColor = UIColor.whiteColor()
    }
}