//
//  ReminderMedicationTableViewPopupController.swift
//  ZSP
//
//  Created by Donovan King on 8/3/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class ReminderMedicationTableViewPopupController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, ZSPModelManagerDelegate {
    
    let MODEL = ZSPModelManager.DefaultModelManager.ReminderManager
    var selected: ZSPMedicationItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        MODEL.medListDelegate = self
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MODEL.allMedicationItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = MODEL.allMedicationItems[indexPath.row].title
        //        cell.imageView?.frame = CGRectMake(0, 0, 10, 10)
        //
        //        cell.imageView?.image = cellImage(MODEL[indexPath.row].mood)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected = MODEL.allMedicationItems[indexPath.row]
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            self.MODEL.removeMedication(self.MODEL.allMedicationItems[indexPath.row])
        }
        
        let delete = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteHandler)
        
        return [delete]
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Your past Medication Reminders will be displayed here."
        let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
                     NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func refresh() {
        self.tableView.reloadData()
    }
    
}