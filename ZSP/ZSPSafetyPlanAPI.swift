//
//  ZSPSafetyPlanAPI.swift
//  ZSP
//
//  Created by Donovan King on 8/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPSafetyPlanAPI: ZSPAPIManager, LeanTaaSThreadingMixin {
    
    static let API = ZSPSafetyPlanAPI()
    
    private init() {
        
    }
    
    func loadModel() {

        let url = ZSPNetworkManager.baseURL + "/zsp/safety-plan/setup/" + CURRENT_USER_NAME
        Alamofire.request(.GET,
                          url,
                          parameters: nil,
                          encoding: .URL,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
            .validate()
            .responseJSON { (res) in
                switch res.result {
                case .Success:
                    if let value = res.result.value {
                        dispatch_async(self.GlobalUserInitiatedQueue, { 
                            self.convertFromJSON(value)
                        })
                    } else {
                        print("ERROR READING SAFETY PLAN JSON")
                    }
                case .Failure(let error):
                    print(error)
                }
            }
    }
    
    func addActivity(activity: ZSPActivityItem) {
        let url = ZSPNetworkManager.baseURL + "/zsp/safety-plan/setup/add"
        let params: [String : AnyObject] =
                    ["user" : CURRENT_USER_NAME,
                     "plan" : activity.title,
                     "mood" : activity.mood,
                     "stepId" : 1,
                     "reminderFl" : activity.reminderOn]
        
        Alamofire.request(.POST,
                          url,
                          parameters: params,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
            .responseJSON { (res) in
                switch res.result {
                case .Success:
                    if let value = res.result.value {
                        let resJson = JSON(value)
                        activity.id = resJson["id"].stringValue
                        print(activity)
                    } else {
                        print("ERROR POSTING SAFETY PLAN JSON")
                    }
                case .Failure(let error):
                    print("ERROR POSTING SAFETY PLAN JSON")
                    print(error)
                }
        }
    }
    
    func deleteItem(item: ZSPSafetyPlanItem, step: Int) {
        guard let deleteId = item.id else {
            print("ERROR DELETING SAFETY PLAN ITEM: ITEM HAS NO ID")
            return
        }
        let url = ZSPNetworkManager.baseURL + "/zsp/safety-plan/setup/delete"
        let params: [String : AnyObject] = [ "user" : CURRENT_USER_NAME,
                                             "stepId" : step,
                                             "planId" : deleteId]
        Alamofire.request(.POST,
                          url,
                          parameters: params,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
                .validate()
                .responseJSON { (res) in
                    print(res)
        }
    }
    
    func addItem(item: ZSPSafetyPlanItem, toStep step: Int) {
        let url = ZSPNetworkManager.baseURL + "/zsp/safety-plan/setup/add"
        var params: [String : AnyObject] =
            ["user" : CURRENT_USER_NAME,
             "plan" : item.title,
             "stepId" : step]
        if let number = item.phoneNumber {
            params["phone"] = number
        }
        Alamofire.request(.POST,
                          url,
                          parameters: params,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
            .responseJSON { (res) in
                switch res.result {
                case .Success:
                    if let value = res.result.value {
                        let resJson = JSON(value)
                        item.id = resJson["id"].stringValue
                    } else {
                        print("ERROR POSTING SAFETY PLAN JSON")
                    }
                case .Failure(let error):
                    print("ERROR POSTING SAFETY PLAN JSON")
                    print(error)
                }
        }
    }
    
    private func convertFromJSON(jsonString: AnyObject) {
        let json = JSON(jsonString)
        for step in json.arrayValue {
            switch step["_id"].intValue {
            case 1:
                activitiesFromJSON(step)
            case 2:
                stepTwoFromJSON(step)
            case 3:
                stepThreeFromJSON(step)
            default:
                print("ERROR INVALID STEP ID")
                break
            }
        }
    }
    
    private func activitiesFromJSON(json: JSON) {
        var activities = [ZSPActivityItem]()
        for activity in json["items"].arrayValue {
            let hasReminder = activity["reminderFl"].boolValue
            let text = activity["plan"].stringValue
            let mood = activity["mood"].intValue
            let new = ZSPActivityItem(userID: CURRENT_USER_NAME,
                                      title: text,
                                      mood: mood,
                                      reminder: hasReminder)
            new.id = activity["_id"].stringValue
            activities.append(new)
        }
        dispatch_async(dispatch_get_main_queue()) { 
            ZSPModelManager.DefaultModelManager.SafetyPlan.addLoadedActvities(activities)
        }
    }
    
    private func stepTwoFromJSON(json: JSON) {
        var stepTwoItems = [ZSPSafetyPlanItem]()
        for item in json["items"].arrayValue {
            let text = item["plan"].stringValue
            let new = ZSPSafetyPlanItem(userID: CURRENT_USER_NAME, title: text)
            new.id = item["_id"].stringValue
            stepTwoItems.append(new)
        }
        dispatch_async(dispatch_get_main_queue()) {
            ZSPModelManager.DefaultModelManager.SafetyPlan.addLoadedStepTwoItems(stepTwoItems)
        }
    }
    
    private func stepThreeFromJSON(json: JSON) {
        var stepThreeItems = [ZSPSafetyPlanItem]()
        for item in json["items"].arrayValue {
            let text = item["plan"].stringValue
            let new = ZSPSafetyPlanItem(userID: CURRENT_USER_NAME, title: text)
            new.id = item["_id"].stringValue
            new.phoneNumber = item["phone"].string
            stepThreeItems.append(new)
        }
        dispatch_async(dispatch_get_main_queue()) {
            ZSPModelManager.DefaultModelManager.SafetyPlan.addLoadedStepThreeItems(stepThreeItems)
        }
    }
}