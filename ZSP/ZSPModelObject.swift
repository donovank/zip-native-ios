//
//  ZSPModelObject.swift
//  ZSP
//
//  Created by Donovan King on 7/27/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ZSPModelObject {
    var id: String? { get set }
//    var userID: String { get }
    
//    init(fromJson data: String)
}