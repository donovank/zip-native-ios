//
//  ZSPDismissable.swift
//  ZSP
//
//  Created by Donovan King on 8/24/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

protocol DismissalDelegate : class {
    func dismiss()
}

//protocol Dismissable : class {
//    weak var dismissalDelegate : DismissalDelegate? { get set }
//}