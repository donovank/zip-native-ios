//
//  RepeatOnDaysView.swift
//  ZSP
//
//  Created by Donovan King on 8/3/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

//@IBDesignable
class RepeatOnDaysView: UIView {
    
    @IBOutlet var view: UIView!
    
    @IBOutlet weak var textRepeatLabel: UILabel!
    @IBOutlet weak var perDayContainerView: UIView!
    
    @IBOutlet weak var monCheck: UIImageView!
    @IBOutlet weak var monLabel: UILabel!
    @IBOutlet weak var tueCheck: UIImageView!
    @IBOutlet weak var tueLabel: UILabel!
    @IBOutlet weak var wedCheck: UIImageView!
    @IBOutlet weak var wedLabel: UILabel!
    @IBOutlet weak var thuCheck: UIImageView!
    @IBOutlet weak var thuLabel: UILabel!
    @IBOutlet weak var friCheck: UIImageView!
    @IBOutlet weak var friLabel: UILabel!
    @IBOutlet weak var satCheck: UIImageView!
    @IBOutlet weak var satLabel: UILabel!
    @IBOutlet weak var sunCheck: UIImageView!
    @IBOutlet weak var sunLabel: UILabel!
    
    @IBOutlet weak var repeatLabel: UILabel!
    
    var days: [ZSPDayType]? {
        didSet {
            if let list = days where days?.count > 0 {
                setCheckmarks(list)
            }
        }
    }
    
    var type: ZSPReminderFrequency? {
        didSet {
            guard let theType = type else {
                return
            }
            switch theType.mode {
            case .Daily, .Never, .Weekly:
                setRepeatLabelTo(theType.mode)
            case .OnDays:
                setRepeatOnDays(theType.days)
            }
        }
    }
    
    let check = UIImage(named: "Checkmark")
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        NSBundle(forClass: RepeatOnDaysView.self).loadNibNamed("RepeatOnDaysView", owner: self, options: nil)
        guard let content = view else { return }
        content.frame = self.bounds
        content.autoresizingMask = [.FlexibleHeight, .FlexibleWidth]
        self.addSubview(content)
    }
    
    func setRepeatOnDays(days: [ZSPDayType]) {
        perDayContainerView.hidden = false
        self.days = days
    }
    
    func setRepeatLabelTo(mode: ZSPReminderRepeatMode) {
        perDayContainerView.hidden = true
        textRepeatLabel.text = mode.rawValue
    }
    
    func setDaysFontSize(size: CGFloat) {
        let dayLabels = [monLabel, tueLabel, wedLabel, thuLabel, friLabel, satLabel, sunLabel]
        if let font = UIFont(name: "Lato-Regular", size: size) {
            for day in dayLabels {
                day.font = font
            }
        }
    }
    
    private func clearMarks() {
        let views = [monCheck, tueCheck, wedCheck, thuCheck, friCheck, satCheck, sunCheck]
        views.forEach { (iView) in
            iView.image = nil
        }
    }
    
    
    private func setCheckmarks(dayList: [ZSPDayType]) {
        clearMarks()
        for day in dayList {
            getImageView(day).image = check
        }
    }
    
    private func getImageView(day: ZSPDayType) -> UIImageView {
        switch day {
        case .Mon:
            return monCheck
        case .Tue:
            return tueCheck
        case .Wed:
            return wedCheck
        case .Thu:
            return thuCheck
        case .Fri:
            return friCheck
        case .Sat:
            return satCheck
        case .Sun:
            return sunCheck
        }
    }
}

