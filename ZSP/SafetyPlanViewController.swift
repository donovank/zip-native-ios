//
//  SafetyPlanViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/20/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import SWRevealViewController

class SafetyPlanViewController: UIViewController, LeanTaaSUIMixin, SWRevealViewControllerDelegate {
    
    @IBOutlet weak var stepOneView: UIView!
    @IBOutlet weak var stepTwoView: UIView!
    @IBOutlet weak var stepThreeView: UIView!
    @IBOutlet weak var quoteLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
        
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var stepOneButton: UIButton!
    @IBOutlet weak var stepTwoButton: UIButton!
    @IBOutlet weak var stepThreeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStepViews()
        
        if self.revealViewController() != nil {
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)

        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        }
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            contentView.userInteractionEnabled = true
        } else {
            contentView.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            contentView.userInteractionEnabled = true
        } else {
            contentView.userInteractionEnabled = false
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if let img = UIImage(named: "SafetyPlanBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        quoteLabel.text = randomQuote()
    }

    func setupStepViews() {
        let stepViews = [stepOneView, stepTwoView, stepThreeView]

        
        for stepView in stepViews {
            stepView.layer.cornerRadius = 10
            stepView.clipsToBounds = true
        }

    }
    
    var currentQuote: UInt32 {
        get {
            return UInt32(NSUserDefaults.standardUserDefaults().integerForKey("quote_key"))
        }
        set (val) {
            NSUserDefaults.standardUserDefaults().setInteger(Int(val), forKey: "quote_key")
        }
    }
    
    func randomQuote() -> String {
        var randNum = arc4random_uniform(10) + 1
        if randNum == currentQuote {
            randNum += 1
        }
        currentQuote = randNum
        switch randNum {
        case 1:
            return "Going for a walk can help calm and clear the mind."
        case 2:
            return "Meditation is a possible way to ease into a relaxed state."
        case 3:
            return "Caring for a pet can prove satisfying for the caretaker and the companion."
        case 4:
            return "Volunteering for a cause has shown to produce a sense of satisfaction."
        case 5:
            return "Reading a book can help shift your mood for the better."
        case 6:
            return "Listening to a favorite song can lift the spirits."
        case 7:
            return "Journaling thoughts and feelings has shown to be constructive towards positivity."
        case 8:
            return "Joining a club or a class has shown to promote positive interactions."
        case 9:
            return "Having food with a friend can be a simple way of unwinding."
        case 10:
            return "Exercising regularly has been shown to be an effective antidepressant."
        default:
            return "Going for a walk can help calm and clear the mind."
        }
    }
    
}