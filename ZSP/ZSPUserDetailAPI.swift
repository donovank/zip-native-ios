//
//  ZSPUserDetailAPI.swift
//  ZSP
//
//  Created by Donovan King on 9/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import PhoneNumberKit

final class ZSPUserDetailAPI: ZSPAPIManager, LeanTaaSThreadingMixin {

    static var API = ZSPUserDetailAPI()
    
    private init() { }
    
    func loadModel() {
        let URL = ZSPNetworkManager.baseURL + "/zsp/user/getDetails/" + CURRENT_USER_NAME
        
        Alamofire.request(.GET,
                          URL,
                          parameters: nil,
                          encoding: .JSON,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
                .validate()
                .responseJSON { (res) in
                    switch res.result {
                    case .Success:
                        if let value = res.result.value {
                            dispatch_async(self.GlobalUserInitiatedQueue, {
                                self.convertFromJSON(value)
                            })
                        } else {
                            print(res)
                            print("ERROR reading user details JSON")
                        }
                    case .Failure(let error):
                        print("ERROR GETTING USER DETAILS")
                        print(res)
                        print(error)
                    }
        }
    
    }
    
    private func convertFromJSON(jsonString: AnyObject) {
        let json = JSON(jsonString)
        
        let details = json["details"]
        
        let user = ZSPModelManager.DefaultModelManager.User
        
        user.dateOfBirth = NSDate(timeIntervalSince1970: Double(details["dob"]["$date"].intValue/1000))
        user.firstName = details["firstName"].stringValue
        user.lastName = details["lastName"].stringValue
        user.gender = details["gender"].stringValue
        
        do {
            try user.mobileNumber = PhoneNumber(rawNumber: details["mobileNumber"].stringValue)
        } catch {
            print("PROBLEM PARSING PHONE NUMBER")
            print(error)
        }
        
    }
    
    
    
    
    
    
}