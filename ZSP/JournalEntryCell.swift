//
//  JournalEntryCell.swift
//  ZSP
//
//  Created by Donovan King on 7/16/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class JournalEntryCell: UITableViewCell {
    
    @IBOutlet weak var cellContainerView: UIView!
    @IBOutlet weak var cellContainerStack: UIStackView!
    @IBOutlet weak var dateLabelContainerView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var activityBadgeContainerView: UIView!
    @IBOutlet weak var titleContainerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bodyTextLabel: UILabel!
    var viewAssembled: Bool = false
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func cellSetup(entry: ZSPJournalEntry) {
        
//        dateLabel.text = entry.getDateString()
//        bodyTextLabel.text = entry.text
//        
//        if let title = entry.title where !title.isEmpty {
//            titleContainerView.hidden = false
//            titleLabel.text = title
//        } else {
//            titleContainerView.hidden = true
//            titleLabel.text = nil
//        }
//        
//        activityBadgeContainerView.hidden = !entry.isFromActivity
//        
//        
//        if !(viewAssembled) {
//            cellContainerView.layoutIfNeeded()
//            
//            backgroundColor = UIColor.clearColor()
//            selectionStyle = .None
//            cellContainerView.layer.cornerRadius = 10
//            
//            let bottomBorderLayer = CALayer()
//            let thickness = CGFloat(1)
//            bottomBorderLayer.backgroundColor = UIColor.darkGrayColor().CGColor
//            bottomBorderLayer.frame = CGRectMake(0, CGRectGetHeight(dateLabelContainerView.frame) - thickness, CGRectGetWidth(dateLabelContainerView.frame), thickness)
//            dateLabelContainerView.layer.addSublayer(bottomBorderLayer)
//            viewAssembled = true
//        }
        
    }

    
}