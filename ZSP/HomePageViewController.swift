//
//  HomePageViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import SWRevealViewController

class HomePageViewController: UIViewController, SWRevealViewControllerDelegate {
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var circleButtonOne: UIView!
    @IBOutlet weak var circleButtonTwo: UIView!
    @IBOutlet weak var circleButtonThree: UIView!
    @IBOutlet weak var circleThreeParent: UIView!
    @IBOutlet weak var circleButtonFour: UIView!
    
    @IBOutlet weak var journalButton: UIButton!
    @IBOutlet weak var safetyPlanButton: UIButton!
    @IBOutlet weak var metricsButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        circleButtonOne.layoutIfNeeded()
        circleButtonOne.layer.cornerRadius = circleButtonOne.layer.frame.size.width/2
        circleButtonOne.clipsToBounds = true
        
        circleButtonTwo.layoutIfNeeded()
        circleButtonTwo.layer.cornerRadius = circleButtonTwo.layer.frame.size.width/2
        circleButtonTwo.clipsToBounds = true
        
        circleButtonThree.layoutIfNeeded()
        circleButtonThree.layer.cornerRadius = circleButtonThree.layer.frame.size.width/2
        circleButtonThree.clipsToBounds = true
        
        circleButtonFour.layoutIfNeeded()
        circleButtonFour.layer.cornerRadius = circleButtonFour.layer.frame.size.width/2
        circleButtonFour.clipsToBounds = true
    
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        
       
        
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)

        
        if let img = UIImage(named: "HomePageBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        if self.revealViewController() != nil {
            self.revealViewController().clipsViewsToBounds = true
            self.revealViewController().rearViewRevealOverdraw = 10
            self.revealViewController().delegate = self
            menuButton.target = self.revealViewController()
            menuButton.action = Selector("revealToggle:")
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            ZSPModelManager.DefaultModelManager.mainNavControll = self.revealViewController()
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        let circleViews = [circleButtonOne, circleButtonTwo, circleButtonThree, circleButtonFour]
        
        if(position == .Left) {
            for circleView in circleViews {
                circleView.userInteractionEnabled = true
            }
        } else {
            for circleView in circleViews {
                circleView.userInteractionEnabled = false
            }
        }
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        let circleViews = [circleButtonOne, circleButtonTwo, circleButtonThree, circleButtonFour]
        
        if(position == .Left) {
            for circleView in circleViews {
                circleView.userInteractionEnabled = true
            }
        } else {
            for circleView in circleViews {
                circleView.userInteractionEnabled = false
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    
    @IBAction func safetyPlanTransition(sender: AnyObject) {
        self.revealViewController()
            .rearViewController
            .performSegueWithIdentifier("SafetyPlanSegue", sender: nil)
    }
    
    @IBAction func metricTransition(sender: AnyObject) {
        self.revealViewController()
            .rearViewController
            .performSegueWithIdentifier("MetricsSegue", sender: nil)
    }
    
    @IBAction func journalingTransition(sender: AnyObject) {
        self.revealViewController()
            .rearViewController
            .performSegueWithIdentifier("JournalingSegue", sender: nil)
    }
    

    @IBAction func profileTransition(sender: AnyObject) {
        self.revealViewController()
            .rearViewController
            .performSegueWithIdentifier("UserDetailSegue", sender: nil)
    }
    
    
    func setupView() {
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.translucent = true
        navigationController?.view.backgroundColor = UIColor.clearColor()
 
    }
    
    func drawCircleInView(parentView: UIView, targetView: UIView, color: UIColor, diameter: CGFloat)
    {
        let square = CGSize(width: max(parentView.bounds.width, parentView.bounds.height), height: max(parentView.bounds.width, parentView.bounds.height))
        let center = CGPointMake(square.width / 2 - diameter, square.height / 2 - diameter)
        
        let circlePath = UIBezierPath(arcCenter: center, radius: CGFloat(diameter), startAngle: CGFloat(0), endAngle: CGFloat(M_PI * 2), clockwise: true)
        let shapeLayer = CAShapeLayer()
        print(targetView.center)
        shapeLayer.path = circlePath.CGPath
        
        shapeLayer.fillColor = color.CGColor
        shapeLayer.strokeColor = color.CGColor
        shapeLayer.lineWidth = 1.0
        
        targetView.backgroundColor = UIColor.clearColor()
        targetView.layer.addSublayer(shapeLayer)
    }
    
    func setRoundedView(roundedView: UIView){
        
//        let circlePath = UIBezierPath(ovalInRect: roundedView.bounds)
        
//        roundedView.layer.masksToBounds = false
//        roundedView.layer.cornerRadius = roundedView.frame.size.height/2
//        roundedView.clipsToBounds = true
    }
    
}