//
//  ZSPMoodPickerDelegate.swift
//  ZSP
//
//  Created by Donovan King on 9/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ZSPMoodPickerDelegate {
    
    var moodHasBeenSet: Bool { get set }
    func moodSelected(mood: ZSPMoodType)
    
}