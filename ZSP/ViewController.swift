//
//  ViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/12/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import CoreGraphics
//import Charts

class ViewController: UIViewController {
    
    
    @IBOutlet weak var MyChartContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let lineChart = LineChartView(frame: MyChartContainer.bounds)
//        
//        
//        lineChart.noDataText = "No Data Availible"
//        lineChart.delegate = self
//        lineChart.backgroundColor = UIColor.clearColor()
//        lineChart.userInteractionEnabled = false
//
//        
//        lineChart.xAxis.drawGridLinesEnabled = false
//        lineChart.xAxis.labelTextColor = UIColor.whiteColor()
//        lineChart.xAxis.axisLineColor = UIColor.whiteColor()
//        lineChart.xAxis.axisLineWidth = 2
//        
//        lineChart.rightAxis.axisLineWidth = 2
//        lineChart.rightAxis.axisLineColor = UIColor.whiteColor()
//        lineChart.rightAxis.gridColor = UIColor(white: 1.0, alpha: 0.2)
//        lineChart.rightAxis.labelTextColor = UIColor.whiteColor()
//        lineChart.rightAxis.gridLineWidth = 1
//        let yFormat = NSNumberFormatter()
//        yFormat.allowsFloats = false
//        lineChart.rightAxis.valueFormatter = yFormat
//        lineChart.descriptionText = ""
//        
//        let gradientView = UIView(frame: lineChart.layer.bounds)
//        
////        let chartView = UIView(frame: lineChart.layer.bounds)
//        
//        let blueWhite = CAGradientLayer.init()
//        blueWhite.frame = gradientView.bounds
//        let b1 = UIColor(red: 70.0/255, green: 150.0/255, blue: 201.0/255, alpha: 1)
//        let b2 = UIColor(red: 145.0/255, green: 229.0/255, blue: 255.0/255, alpha: 1)
//        
//        blueWhite.colors = [b2.CGColor, b1.CGColor]
//        
//        
//        gradientView.layer.addSublayer(blueWhite)
//        MyChartContainer.insertSubview(gradientView, atIndex: 0)
//        MyChartContainer.insertSubview(lineChart, atIndex: 1)
//        
//        
//        
//        lineChart.legend.enabled = false
//        lineChart.leftAxis.enabled = false
//        lineChart.xAxis.labelPosition = .Bottom
//        
//        
//        var vals = [ChartDataEntry]()
//        let labels = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
//        
//        for index in 0...6 {
//            vals.append(ChartDataEntry(value: Double(index+1), xIndex: index))
//        }
//        
//        let dataSet = LineChartDataSet(yVals: vals, label: nil)
//        dataSet.setColor(UIColor.whiteColor())
//        dataSet.circleRadius = 5
//        dataSet.drawValuesEnabled = false
//        
//        dataSet.circleHoleColor = UIColor.whiteColor()
//        dataSet.circleColors = [UIColor.whiteColor()]
//        
//        
//        let chartData = LineChartData(xVals: labels, dataSet: dataSet)
//        
//        lineChart.data = chartData
//        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

