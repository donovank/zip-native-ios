//
//  ZSPJournalEntryManager.swift
//  ZSP
//
//  Created by Donovan King on 7/27/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPJournalEntryManager: ZSPJournalEntrySectionDelegate, LeanTaaSThreadingMixin, ZSPManager {

    static let Default = ZSPJournalEntryManager()
    
    let MMMMddyyyyFormater = NSDateFormatter()
    let MMMMyyyyFormater = NSDateFormatter()
    
    var delegate: ZSPModelManagerDelegate?
    private var durringBuild = true
    
    private init() {
        MMMMddyyyyFormater.dateFormat = "MMMM dd yyyy"
        MMMMyyyyFormater.dateFormat = "MMMM yyyy"
        
    }
    
    func clearForSignOff() {
        self.allEntries = []
        self.sections = []
        self.draftEntries = []
    }
    
    internal func sectionDone(sender: ZSPJournalEntrySection) {
        if durringBuild  { return }
        dispatch_async(dispatch_get_main_queue()) {
            self.delegate?.refresh()
        }
    }
    
    internal func sectionIsEmpty(index: Int) {
        sections.removeAtIndex(index)
    }

    private var sections = [ZSPJournalEntrySection]() {
        didSet {
            durringBuild = false
            dispatch_async(dispatch_get_main_queue()) {
                self.delegate?.refresh()
            }
        }
    }
    
    private var allEntries = [ZSPJournalEntry]() {
        didSet {
            dispatch_async(GlobalUserInitiatedQueue) { 
                self.buildSections()
            }
        }
    }
    
    var draftEntries = [ZSPJournalEntry]() {
        didSet {
            if !durringBuild {
                dispatch_async(dispatch_get_main_queue()) {
                    self.delegate?.refresh()
                }
            }
        }
    }
    var sectionCount: Int {
        get {
            return sections.count
        }
    }

    func countForSection(index: Int) -> Int {
        return sections[index].entries.count
    }

    private func buildSections() {
        durringBuild = true
        let now = NSDate()
        var last = (d: NSCalendar.currentCalendar().component(.Day, fromDate: now),
                    m: NSCalendar.currentCalendar().component(.Month, fromDate: now),
                    y: NSCalendar.currentCalendar().component(.Year, fromDate: now))
        var tmpSections = [ZSPJournalEntrySection]()
        var tmpList = [ZSPJournalEntry]()
        let tempAll = self.allEntries
        for entry in tempAll {
            if entry.hasDifferentMonthThan(last) {
                if tmpList.isEmpty {
                    last.d = entry.day
                    last.m = entry.month
                    last.y = entry.year
                    tmpList.append(entry)
                } else {
                    tmpSections.append(ZSPJournalEntrySection(entries: tmpList, delegate: self))
                    last.d = entry.day
                    last.m = entry.month
                    last.y = entry.year
                    tmpList = [entry]
                }
            } else {
                tmpList.append(entry)
            }
        }
        if tmpSections.isEmpty {
            tmpSections.append(ZSPJournalEntrySection(entries: tmpList, delegate: self))
        }
        sectionSort(tmpSections)
    }

    private func sectionSort(list: [ZSPJournalEntrySection]) {
        self.sections = list.sort({ (a, b) -> Bool in
            if .OrderedDescending == a.date.compare(b.date) {
                return true
            } else {
                return false
            }
        })
    }

    func addNewEntry(item: ZSPJournalEntry) {
        if item.isDraft {
            self.draftEntries.append(item)
            return
        }
        var tmpSections = sections
        for section in tmpSections {
            if !item.hasDifferentMonthThan(section) {
                section.addItem(item)
                return
            }
        }
        self.durringBuild = true
        let newSection = ZSPJournalEntrySection(entries: [item], delegate: self)
        tmpSections.append(newSection)
        sectionSort(tmpSections)
    }
    
    func addLoadedEntries(entries: [ZSPJournalEntry]) {
        self.allEntries.appendContentsOf(entries)
    }
    
    func deleteEntry(entry: ZSPJournalEntry) {
        for (index, section) in sections.enumerate() {
            if !entry.hasDifferentMonthThan(section) {
                section.removeItem(entry, sectionIndex: index)
                return
            }
        }
    }
    
    func deleteEntryAtIndexPath(path: NSIndexPath) {
        sections[path.section].removeItemAtIndex(path.row, sectionIndex: path.section)
    }
    
    func deleteEntryInSection(section: Int, atRow row: Int) {
        sections[section].removeItemAtIndex(row, sectionIndex: section)
    }
    
    func headerStringForSection(section: Int) -> String {
        return sections[section].getDateHeaderString()
    }
    
    func getEntryForIndexPath(indexPath: NSIndexPath) -> ZSPJournalEntry {
        return sections[indexPath.section].entries[indexPath.row]
    }
    
    func getEntryInSection(section: Int, atRow row: Int) -> ZSPJournalEntry {
        return sections[section].entries[row]
    }
    
}

