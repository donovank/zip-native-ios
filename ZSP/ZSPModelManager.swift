//
//  ZSPModelManager.swift
//  ZSP
//
//  Created by Donovan King on 7/27/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SWRevealViewController

//add JWTTOKEN with X-AUTH-TOKEN if doesn't work

class ZSPModelManager {
    static let DefaultModelManager = ZSPModelManager()
    let JournalManager = ZSPJournalEntryManager.Default
    let SafetyPlan = ZSPSafetyPlanManager.Default
    let ReminderManager = ZSPReminderManager()
    let User = ZSPUserProfile.Default
    
    weak var mainNavControll: SWRevealViewController?
    
    func signOutUser() {
        self.SafetyPlan.clearForSignOff()
        self.JournalManager.clearForSignOff()
        
        mainNavControll?.dismissViewControllerAnimated(true) {
            print("Signing off")
        }
    }
    
    private init() {
//        ZSPNetworkManager.Network.API.SafetyPlan.loadModel()
//        ZSPNetworkManager.Network.API.Reminder.loadModel()
    }
    
    func loadModels() {
        ZSPNetworkManager.Network.API.SafetyPlan.loadModel()
        ZSPNetworkManager.Network.API.Journaling.loadModel()
        ZSPNetworkManager.Network.API.Journaling.DRAFT_API.loadModel()
        ZSPNetworkManager.Network.API.UserDetails.loadModel()
    }
}



