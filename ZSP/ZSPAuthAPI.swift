//
//  ZSPAuthAPI.swift
//  ZSP
//
//  Created by Donovan King on 8/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPAuthAPI: ZSPAPIManager {
    
    static let API = ZSPAuthAPI()
    private let AuthBaseURL = "https://m.zsp-aws-test.leantaas.com"

    private init () { }
    
    func loadModel() { }
    
    func tryAuthUser(userName: String, password: String, completion: (ZSPAuthResult)->()) {
        
        let url = AuthBaseURL + "/api/v1/account/authenticate"
        let params: [String : String] = ["username": userName,
                                         "password": password]
        Alamofire.request(.POST,
            url,
            parameters: params,
            encoding: .JSON,
            headers: nil)
            .response(completionHandler: { (req, res, data, error) in
                guard error == nil else {
                    print(error)
                    completion(.Failure)
                    return
                }
                print(res)
                if let token = res?.allHeaderFields["X-AUTH-TOKEN"] as? String {
                    ZSPNetworkManager.Network.setToken(token, callback: { 
                        ZSPNetworkManager.Network.API.Auth.getProfile(completion)
                    })
                } else {
                    print("error unwrapping token from responce")
                    completion(.Failure)
                    print(res)
                }
            })
    }
    
    func getProfile(completion: (ZSPAuthResult)->()) {
        
        let url = ZSPNetworkManager.baseURL + "/api/user/profile"
        
        Alamofire.request(.GET,
            url, parameters: nil,
            encoding: .JSON,
            headers: ZSPNetworkManager.Network.getTokenHeader())
            .response { (req, res, data, error) in
                guard error == nil else {
                    print("Error getting user profile")
                    print(error)
                    completion(.Failure)
                    return
                }
                guard let theData = data else {
                    print("Error with user profile data")
                    print(error)
                    completion(.Failure)
                    return
                }
                let json = JSON(data: theData)
                
                ZSPModelManager.DefaultModelManager.User.userName = json["data"]["username"].string
                ZSPModelManager.DefaultModelManager.User.id = json["data"]["id"].string
                
                print(CURRENT_USER_NAME)
                
                if json["data"]["forcePwdChange"].intValue != 0 {
                    completion(.ResetPassword)
                } else {
                    completion(.Success)
                    ZSPModelManager.DefaultModelManager.loadModels()
                }
        }
    }
    
    func resetPasswordFrom(current: String, toNewPassword newPass: String, withConfirmation confirmPass: String, completion: (ZSPAuthResult)->()) {
        let url = AuthBaseURL + "/v1/account/resetpassword"
        let params: [String : String] = ["oldPassword": current,
                                         "newPassword": newPass,
                                         "confirmPassword": confirmPass]
        Alamofire.request(.PUT,
            url,
            parameters: params,
            encoding: .JSON,
            headers: ZSPNetworkManager.Network.getTokenHeader())
            .validate()
            .response(completionHandler: { (req, res, data, error) in
                
                guard error == nil else {
                    print(error)
                    completion(.Failure)
                    return
                }
                
                print(res)
                completion(.Success)
            })

    }
}

enum ZSPAuthResult {
    case Success
    case Failure
    case ResetPassword
}