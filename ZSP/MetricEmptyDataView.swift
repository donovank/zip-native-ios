//
//  MetricEmptyDataView.swift
//  ZSP
//
//  Created by Donovan King on 9/19/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class MetricEmptyDataView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var howAreYouImageView: UIImageView!
    @IBOutlet weak var recordMoodLabel: UILabel!
    @IBOutlet weak var graphMoodLabel: UILabel!
    
    var contentSize = CGSizeZero {
        didSet {
            scrollView.contentSize = contentSize
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        didLoad()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        didLoad()
    }
    
    convenience init() {
        self.init(frame: CGRectZero)
    }
    
    func didLoad() {
        NSBundle.mainBundle().loadNibNamed("MetricEmptyDataView", owner: self, options: nil)
        self.addSubview(self.view);
//        scrollView.contentSize = CGSizeMake(UIScreen.mainScreen().bounds.width, 1000)
        howAreYouImageView.layer.shadowColor = UIColor.lightGrayColor().CGColor
        howAreYouImageView.layer.shadowRadius = 3
        howAreYouImageView.layer.shadowOpacity = 0.5
        howAreYouImageView.layer.shadowOffset = CGSizeMake(0, 0)
        
        let color = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        let moodString = NSMutableAttributedString(string: "The Felt Depressed and Sleep Quality graphs help you track data from the Record Mood questionnaire.")
        let graphString = NSMutableAttributedString(string: "The Overall Mood graph is populated using data from the five feeling faces questionnaire.")
        
        if let font = UIFont(name: "Lato-Bold", size: 15) {
            let attributes = [NSForegroundColorAttributeName: color,
                              NSFontAttributeName: font]
            graphString.setAttributes(attributes, range: NSMakeRange(4, 12))
            moodString.setAttributes(attributes, range: NSMakeRange(4, 14))
            moodString.addAttributes(attributes, range: NSMakeRange(22, 14))
            recordMoodLabel.attributedText = moodString
            graphMoodLabel.attributedText = graphString
        }
    }
    
}