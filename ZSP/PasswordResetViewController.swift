//
//  PasswordResetViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import PhoneNumberKit

class PasswordResetViewController: UIViewController, UITextFieldDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var phoneNumberBGView: UIView!
    @IBOutlet weak var birthdayBGView: UIView!
    @IBOutlet weak var phoneNumberField: PhoneNumberTextField!
    @IBOutlet weak var birthdayField: UITextField!
    
    var phoneNumberIsValid = false {
        didSet {
            if phoneNumberIsValid {
                tryEnableSignup()
            }
        }
    }
    
    var tempPassIsValid = false {
        didSet {
            if tempPassIsValid {
                tryEnableSignup()
            }
        }
    }
    
    let formatter = NSDateFormatter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        let datePicker = UIDatePicker()
        let today = NSDate()
        datePicker.date = today
        datePicker.maximumDate = today
        datePicker.addTarget(self, action: #selector(updateTextField), forControlEvents: UIControlEvents.ValueChanged)
        datePicker.datePickerMode = .Date
        birthdayField.inputView = datePicker
        
        formatter.dateStyle = NSDateFormatterStyle.MediumStyle
    }
    
    
    
    func updateTextField() {
        guard let picker = birthdayField.inputView as? UIDatePicker else {
            return
        }
        birthdayField.text = formatter.stringFromDate(picker.date)
    }
    
    @IBAction func userSwipedDown(sender: AnyObject) {
        view.endEditing(true)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func tryEnableSignup () {
        if phoneNumberIsValid && tempPassIsValid {
            resetButton.alpha = 1.0
            resetButton.enabled = true
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
    func setupView() {
        if let img = UIImage(named: "SigninBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        let cornerViews = [resetButton, cancelButton, phoneNumberBGView, birthdayBGView]
        
        for view in cornerViews {
            view.clipsToBounds = true
            view.layer.cornerRadius = view.bounds.height/2
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        
//        resetButton.alpha = 0.75
//        resetButton.enabled = false
        
        phoneNumberField.delegate = self
        birthdayField.delegate = self
        
        resetButton.setTitleColor(UIColor.lightGrayColor(), forState: .Highlighted)
    }
}