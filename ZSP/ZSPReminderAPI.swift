//
//  ZSPReminderAPI.swift
//  ZSP
//
//  Created by Donovan King on 8/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class ZSPReminderAPI: ZSPAPIManager {
    
    static let API = ZSPReminderAPI()
    private init() { }
    
    func loadModel() {
        let url = ZSPNetworkManager.baseURL + "/zsp/reminders/getReminders/" + CURRENT_USER_NAME
        Alamofire.request(.GET,
                          url,
                          parameters: nil,
                          encoding: .URL,
                          headers: ZSPNetworkManager.Network.getTokenHeader())
                 .validate()
                 .responseJSON { (res) in
                    switch res.result {
                    case .Success:
                        if let value = res.result.value {
                            self.convertFromJSON(value)
                        } else {
                            print("ERROR READING REMINDER JSON")
                        }
                    case .Failure(let error):
                        print(error)
                    }
        }
    }
    
    private func convertFromJSON(jsonString: AnyObject) {
        let json = JSON(jsonString).arrayValue
        for byFreq in json {
            if let oneTimeReminders = byFreq.dictionaryValue["oneTime"]?.array {
                for oneTimeReminder in oneTimeReminders {
                    let id = oneTimeReminder["_id"].stringValue
                    let title = oneTimeReminder["title"].stringValue
                    let freq = oneTimeReminder["type"]
                    let isActive = oneTimeReminder["activated"].boolValue
                    let type = oneTimeReminder["subType"].stringValue
                    
                    
                    
                }
            } else if let onDaysReminders = byFreq.dictionaryValue["onTheseDays"]?.array {
                for onDayReminder in onDaysReminders {
//                    print(onDayReminder)
                    
                }
            } else if let onDaysReminders = byFreq.dictionaryValue["weekly"]?.array {
                for onDayReminder in onDaysReminders {
//                    print(onDayReminder)
                    
                }
            }
        }
    }
    
    func addNewReminder(reminder: ZSPReminder){
        
        let url = ZSPNetworkManager.baseURL + "/zsp/reminders/addReminder"
        
        var params: [String : AnyObject] =
                    ["user" : CURRENT_USER_NAME,
                     "title": reminder.title,
                     "date" : Int(floor(reminder.date.timeIntervalSince1970 * 1000)),
                     "subType" : reminder.type.getStringValue()]
        switch reminder.frequency.mode {
        case .Daily:
            params["type"] = "on these days"
            params["daysOfWeek"] = [1,2,3,4,5,6,7]
        case .OnDays:
            params["type"] = "on these days"
            params["daysOfWeek"] = ZSPDayType.getJSONDays(reminder.frequency.days)
        case .Weekly:
            break
        case .Never:
            break
        }
        
    }

    
    
    
}


