//
//  NewReminderViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/20/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class NewReminderViewController: UIViewController, UITableViewDataSource, UITextFieldDelegate, ReminderFrequencyDelegate, LeanTaaSUIMixin {

    @IBOutlet weak var atLabel: UILabel!
    @IBOutlet weak var scrollViewBackground: UIView!
    @IBOutlet weak var reminderTypeButtonsBackground: UIView!
    @IBOutlet weak var itemSelectionButton: UIButton!
    @IBOutlet weak var newItemView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var newItemTextField: UITextField!
    @IBOutlet weak var activityButton: UIButton!
    @IBOutlet weak var medButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var repeatDisplayView: RepeatOnDaysView!
    @IBOutlet weak var titleBoxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var repeatContainerView: UIView!
    @IBOutlet weak var newItemHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleContainerView: UIView!
    
    let MODEL = ZSPModelManager.DefaultModelManager.ReminderManager
    
    var repeatDays = [ZSPDayType : Bool]()
    var repeatMode: ZSPReminderRepeatMode = .Never {
        didSet {
            switch repeatMode {
            case .OnDays:
                repeatDisplayView.setRepeatOnDays(ZSPDayType.getTrueDays(repeatDays))
            default:
                repeatDisplayView.setRepeatLabelTo(repeatMode)
            }
        }
    }
    
    var activity: ZSPActivityItem? {
        didSet {
            titleLabel.text = activity?.title
            setTitleView()
            self.navigationItem.rightBarButtonItem?.enabled = isReminderValid()
        }
    }
    
    var medication: ZSPMedicationItem? {
        didSet {
            titleLabel.text = medication?.title
            setTitleView()
            self.navigationItem.rightBarButtonItem?.enabled = isReminderValid()
        }
    }
    
    var other: ZSPOtherReminderItem? {
        didSet {
            titleLabel.text = other?.title
            setTitleView()
            self.navigationItem.rightBarButtonItem?.enabled = isReminderValid()
        }
    }
    
//    var labelStore: (activity: String?, medication: String?, other: String?)
    
    var doSetup = true
    

    
    var reminderTypeState: ZSPReminderType = .Activity {
        willSet (newVal) {
            
            guard newVal != reminderTypeState else {
                return
            }
            changeSelectedType(newVal)
        }
        didSet {
            self.navigationItem.rightBarButtonItem?.enabled = isReminderValid()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyBackground()
        
//        newItemHeightConstraint.constant = 0
//        newItemView.hidden = true
        firstActivitySetup()
        
        titleContainerView.hidden = true
        newItemTextField.delegate = self
        newItemTextField.autocapitalizationType = .Sentences
        changeSelectedType(.Activity)
        
        self.navigationItem.rightBarButtonItem?.enabled = false
        
        datePicker.minimumDate = NSDate()
        datePicker.setDate(NSDate(), animated: true)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillShow), name:UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(keyboardWillHide), name:UIKeyboardWillHideNotification, object: nil)
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                                                                       NSFontAttributeName : font]
        }
        
    }
    
    override func viewWillDisappear(animated: Bool) {
       view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if doSetup {
            setupViews()
            doSetup = false
        }
        
        repeatContainerView.clipsToBounds = true
        curveView(repeatContainerView, byRoundingCorners: [.BottomRight, .BottomLeft], withRadius: 10)
    }
    
    @IBAction func save(sender: AnyObject) {
        let item: ZSPReminder
        
        switch reminderTypeState {
        case .Activity:
            item = createActivityReminder()
        case .Medication:
            item = createMedReminder()
        case .Other:
            item = createOtherReminder()
        }
        
        MODEL.addNewReminder(item)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    private var freq: ZSPReminderFrequency {
        get {
            return ZSPReminderFrequency(days: ZSPDayType.getTrueDays(repeatDays), mode: repeatMode)
        }
    }
    
    private func createActivityReminder() -> ZSPReminder {
        guard let selectedActivity = activity else {
            return createOtherReminder()
        }
        return ZSPReminder(fromActivity: selectedActivity, withFrequency: freq, onDate: datePicker.date)
    }
    
    private func createMedReminder() -> ZSPReminder {
        guard let selectedMed = medication else {
            return createOtherReminder()
        }
        return ZSPReminder(fromMedication: selectedMed, withFrequency: freq, onDate: datePicker.date)
    }
    
    private func createOtherReminder() -> ZSPReminder {
        guard let selectedOther = other else {
            let label = titleLabel.text != nil ? titleLabel.text! : "Reminder"
            return ZSPReminder(userID: "", title: label, frequency: freq, date: datePicker.date)
        }
        return ZSPReminder(fromOtherType: selectedOther, withFrequency: freq, onDate: datePicker.date)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func stateButtonClicked(sender: UIButton) {
        
        guard let title = sender.titleLabel?.text else {
            print("Sender had no title")
            return
        }
        
        switch title {
        case "Activity":
            reminderTypeState = .Activity
        case "Medication":
            reminderTypeState = .Medication
        case "Other":
            reminderTypeState = .Other
        default:
            print("no case matched")
            break
        }
        
    }
    
    @IBAction func presentPicker(sender: AnyObject) {
        switch reminderTypeState {
        case .Activity:
            performSegueWithIdentifier("ActivityTableSegue", sender: self)
        case .Medication:
            performSegueWithIdentifier("MedicationTableSegue", sender: self)
        case .Other:
            performSegueWithIdentifier("OtherTableSegue", sender: self)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BasicTextCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = "Testing"
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        view.endEditing(true)
        guard let id = segue.identifier else {
            print("SEGUE HAD NO ID")
            return
        }
        
        switch id {
        case "ActivityTableSegue":
            let isMessage = ZSPModelManager.DefaultModelManager.SafetyPlan.stepOneItems.count > 0
            createPopover(segue, hasData: isMessage, handler: { (vc) in
                if let castVC = vc.childViewControllers.first as? ReminderActivityTableViewPopupController {
                    if castVC.selected != nil {
                        self.activity = castVC.selected
                    }
                }
            })
        case "MedicationTableSegue":
            let isMessage = MODEL.allMedicationItems.count > 0
            createPopover(segue, hasData: isMessage, handler: { (vc) in
                if let castVC = vc.childViewControllers.first as? ReminderMedicationTableViewPopupController {
                    if castVC.selected != nil {
                        self.medication = castVC.selected
                    }
                }
            })
        case "OtherTableSegue":
            let isMessage = MODEL.allOtherItems.count > 0
            createPopover(segue, hasData: isMessage, handler: { (vc) in
                if let castVC = vc.childViewControllers.first as? ReminderOtherTableViewPopupController {
                    if castVC.selected != nil {
                        self.other = castVC.selected
                    }
                }
            })
        case "RepeatSelectSegue":
            createRepeatPopover(segue, handler: { (vc) in
                
            })
        default:
            break
        }
        
    }
    
    typealias 💩 = MZFormSheetPresentationControllerTransitionBeginCompletionHandler
    typealias 😖 = MZFormSheetPresentationViewControllerSegue
    
    func createPopover(segue: UIStoryboardSegue, hasData: Bool, handler: 💩) {
        let MZSegue = segue as! 😖
        if let prezController = MZSegue.formSheetPresentationController.presentationController {
            if !hasData {
                prezController.contentViewSize = CGSizeMake(300, 150)
            }
            prezController.shouldCenterHorizontally = true
            prezController.shouldCenterVertically = true
            prezController.shouldApplyBackgroundBlurEffect = false
            prezController.shouldDismissOnBackgroundViewTap = true
            prezController.dismissalTransitionWillBeginCompletionHandler = handler
        }
    }
    
    func createRepeatPopover(segue: UIStoryboardSegue, handler: 💩) {
        let MZSegue = segue as! 😖
        if let prezController = MZSegue.formSheetPresentationController.presentationController {
            prezController.contentViewSize = CGSizeMake(300, 500)
            prezController.shouldCenterHorizontally = true
            prezController.shouldCenterVertically = false
            prezController.shouldApplyBackgroundBlurEffect = false
            prezController.shouldDismissOnBackgroundViewTap = false
            MZSegue.formSheetPresentationController.willPresentContentViewControllerHandler = { (vc) in
                if let nav = vc as? UINavigationController,
                    let controller = nav.visibleViewController as? ReminderFrequencyPopup {
                    controller.delegate = self
                }
            }
        }

    }
    
    func applyBackground() {
        if let img = UIImage(named: "RemindersBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    
    func setupViews() {
        scrollViewBackground.clipsToBounds = true
        scrollViewBackground.layer.cornerRadius = 10
        
        reminderTypeButtonsBackground.clipsToBounds = true
        reminderTypeButtonsBackground.layoutIfNeeded()
        borderRadiusTop(reminderTypeButtonsBackground, radius: 10)
        
        let buttons = [activityButton, medButton, otherButton]
        for button in buttons {
            setupButton(button)
            let imageName = button.currentTitle! + "IconActive"
            button.setImage(UIImage(named: imageName), forState: .Selected)
            button.setTitleColor(UIColor.whiteColor(), forState: .Selected)
            bottomBorder(button, size: 2)
        }
        
        itemSelectionButton.layer.cornerRadius = itemSelectionButton.layer.bounds.height/2
        newItemTextField.borderStyle = .RoundedRect
//        newItemTextField.layer.borderWidth = 1
//        newItemTextField.layer.cornerRadius = 5
        
//        bottomBorder(itemSelectionButton, size: 1)
        bottomBorder(datePicker, size: 1)
        topBorder(datePicker, size: 1)

    }
    
    func borderRadiusTop(view: UIView, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.TopRight, .TopLeft], cornerRadii: CGSizeMake(radius, radius))
        let mask = CAShapeLayer()
        mask.path = path.CGPath
        view.layer.mask = mask
    }
    
    func bottomBorder(view: UIView, size: CGFloat) {
        let border = CALayer()
        let width = size
        border.borderColor = UIColor.lightGrayColor().CGColor
        border.frame = CGRect(x: 0, y: view.frame.size.height - width, width:  view.frame.size.width, height: view.frame.size.height)
        
        border.borderWidth = width
        view.layer.addSublayer(border)
        view.layer.masksToBounds = true
    }
    
    func topBorder(view: UIView, size: CGFloat) {
        let border = CALayer()
        let width = size
        border.borderColor = UIColor.lightGrayColor().CGColor
        border.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: width)
        
        border.borderWidth = width
        view.layer.addSublayer(border)
        view.layer.masksToBounds = true
    }
    
    
    func setupButton(button: UIButton) {
        let spacing: CGFloat = 6.0
        
        //TODO: Make code safe again!
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: button.titleLabel!.text!)
        let titleSize = labelString.sizeWithAttributes([NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField === newItemTextField {
            createNewItem(textField)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func keyboardWillShow(notification:NSNotification){
        
        var userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).CGRectValue()
        keyboardFrame = self.view.convertRect(keyboardFrame, fromView: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentSize.height = view.bounds.height
        self.scrollView.contentInset = contentInset
        
    }
    
    func keyboardWillHide(notification:NSNotification){
        
        let contentInset:UIEdgeInsets = UIEdgeInsetsZero
        self.scrollView.contentInset = contentInset
    }
    
    
    @IBAction func createNewItem(sender: AnyObject) {
        guard let itemName = newItemTextField.text where !itemName.isEmpty else {
            return
        }
        
        switch reminderTypeState {
        case .Medication:
            newMedication(itemName)
        case .Other:
            newOtherItem(itemName)
        default:
            break
        }
        newItemTextField.text = nil
    }
    
    func newMedication(name: String) {
        medication = MODEL.addNewMedication(name)
    }
    
    func newOtherItem(name: String) {
        other = MODEL.addNewOtherItem(name)
    }
    
//    
//    func saveLabel(saveTo: ReminderType) {
//        switch saveTo {
//        case .Activiy:
//            labelStore.activity = titleLabel.text
//        case .Medication:
//            labelStore.medication = titleLabel.text
//        case .Other:
//            labelStore.other = titleLabel.text
//        }
//    }
    
    var first = true
    
    func changeSelectedType(selection: ZSPReminderType) {
        for type in ZSPReminderType.iteratable {
            if type != selection {
                type.buttonforInstance(self).backgroundColor = UIColor.whiteColor()
                type.buttonforInstance(self).selected = false
            } else {
                type.buttonforInstance(self).backgroundColor = type.activeColor()
                type.buttonforInstance(self).selected = true
            }
        }
        switch selection {
        case .Activity:
            if first {
                firstActivitySetup()
                first = false
            } else {
                setupActivityPanel()
            }
        case .Medication:
            setupMedPanel()
        case .Other:
            setupOtherPanel()
        }

    }
    
    func setupActivityPanel() {
        newItemTextField.resignFirstResponder()
        
        itemSelectionButton.setTitle("Select Activity", forState: .Normal)
        itemSelectionButton.backgroundColor = ZSPReminderType.Activity.activeColor()
        
//        self.newItemHeightConstraint.constant = 0
        
        UIView.transitionWithView(newItemView, duration: 0.2,
                                  options: .CurveEaseInOut,
                                  animations: {
                                    self.newItemView.hidden = true
                                  },
                                  completion: nil)
        
        titleLabel.text = activity?.title
        setTitleView()
        
        // animate?
        
        //change data source to activities
    }
    
    func firstActivitySetup() {
        newItemTextField.resignFirstResponder()
        self.newItemView.hidden = true
        
        itemSelectionButton.setTitle("Select Activity", forState: .Normal)
        itemSelectionButton.backgroundColor = ZSPReminderType.Activity.activeColor()
        
    }
    
    func setupMedPanel() {
        newItemTextField.placeholder = "New Medication"
        newItemTextField.attributedPlaceholder = NSAttributedString(string:"New Medication",
                                                                    attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
        itemSelectionButton.setTitle("Previous Medication", forState: .Normal)
        itemSelectionButton.backgroundColor = ZSPReminderType.Medication.activeColor()
        
        UIView.transitionWithView(newItemView, duration: 0.2,
                                  options: .CurveEaseInOut,
                                  animations: {
                                    self.newItemView.hidden = false
                                  },
                                  completion: nil)
        
        titleLabel.text = medication?.title
        setTitleView()

    }
    
    func setupOtherPanel() {
        newItemTextField.placeholder = "New Reminder Text"
        newItemTextField.attributedPlaceholder = NSAttributedString(string:"New Reminder Text",
                                                                    attributes:[NSForegroundColorAttributeName: UIColor.darkGrayColor()])
        itemSelectionButton.setTitle("Previous Reminder", forState: .Normal)
        itemSelectionButton.backgroundColor = ZSPReminderType.Other.activeColor()

        UIView.transitionWithView(newItemView, duration: 0.2,
                                  options: .CurveEaseInOut,
                                  animations: {
//                                    self.newItemView.hidden = false
//                                    self.newItemHeightConstraint.constant = 50
                                  },
                                  completion: nil)
        
        titleLabel.text = other?.title
        setTitleView()
        
//        titleLabel.text = labelStore.other
        
        // change data source
    }
    
    func setTitleView() {
        if let title = titleLabel.text where !title.isEmpty {
            UIView.transitionWithView(titleContainerView,
                                      duration: 0.2,
                                      options: .CurveEaseInOut,
                                      animations: {
                                        self.titleContainerView.hidden = false
                },
                                      completion: nil)
        } else {
            UIView.transitionWithView(titleContainerView,
                                      duration: 0.2,
                                      options: .CurveEaseInOut,
                                      animations: {
                                        self.titleContainerView.hidden = true
                },
                                      completion: nil)
        }
    }
    
    private func isReminderValid() -> Bool {
        switch reminderTypeState {
        case .Activity:
            if activity != nil { return true }
        case .Medication:
            if medication != nil { return true }
        case .Other:
            if other != nil { return true }
        }
        
        return false
    }
    
}