//
//  ZSPJournalEntrySectionDelegate.swift
//  ZSP
//
//  Created by Donovan King on 9/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ZSPJournalEntrySectionDelegate {
    func sectionDone(sender: ZSPJournalEntrySection)
    func sectionIsEmpty(index: Int)
}