//
//  ZSPReminderType.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

enum ZSPReminderType: IteratableEnum {
    case Activity
    case Medication
    case Other
    
    static let iteratable = [Activity, Medication, Other]
    
    func activeColor() -> UIColor {
        switch self {
        case .Activity:
            return UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        case .Medication:
            return UIColor(red:0.28, green:0.87, blue:0.87, alpha:1.0)
        case .Other:
            return UIColor(red:0.79, green:0.79, blue:0.79, alpha:1.0)
        }
    }
    
    func getStringValue() -> String {
        switch self {
        case .Activity:
            return  "activity"
        case .Medication:
            return "medication"
        case .Other:
            return "other"
        }
    }
    
    func buttonforInstance(controller: NewReminderViewController) -> UIButton {
        switch self {
        case .Activity:
            return controller.activityButton
        case .Medication:
            return controller.medButton
        case .Other:
            return controller.otherButton
        }
    }
    static func buttonForType(type: ZSPReminderType, forInstance controller: NewReminderViewController) -> UIButton {
        switch type {
        case .Activity:
            return controller.activityButton
        case .Medication:
            return controller.medButton
        case .Other:
            return controller.otherButton
        }
    }
}