//
//  StepTwoViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import MZFormSheetPresentationController

class StepTwoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SafetyPlanPopupParentController, ZSPModelManagerDelegate {
    
    @IBOutlet weak var stepTwoContainerView: UIView!
    
    @IBOutlet weak var stepTwoCornerContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let MODEL = ZSPModelManager.DefaultModelManager.SafetyPlan
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        MODEL.stepTwoDelegate = self
        
        if let img = UIImage(named: "SafetyPlanBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        navigationItem.rightBarButtonItem?.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setupView()
    }
    
    func refresh() {
        tableView.reloadData()
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row % 2 == 1 {
            return tableView.dequeueReusableCellWithIdentifier("SpacerCell", forIndexPath: indexPath)
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("SimpleActivityCell", forIndexPath: indexPath)
            
            cell.layer.cornerRadius = 10
            cell.textLabel?.text = MODEL.stepTwoItems[indexPath.row/2].title
            
            return cell
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MODEL.stepTwoItems.count * 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row % 2 == 1 {
            return 10
        }
        
        return 50
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        
        view.backgroundColor = UIColor.clearColor()
        
        return view
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        if indexPath.row % 2 == 1 {
            return false
        }
        return true
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        if indexPath.row % 2 == 1 {
            return nil
        }
        
        let deleteHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            let item = self.MODEL.stepTwoItems[indexPath.row/2]
            dispatch_async(dispatch_get_main_queue(), { 
                ZSPNetworkManager.Network.API.SafetyPlan.deleteItem(item, step: 2)
            })
            ZSPModelManager.DefaultModelManager.SafetyPlan.stepTwoItems.removeAtIndex(indexPath.row/2)
        }
        
        let delete = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteHandler)
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.layer.cornerRadius = 0
        
        return [delete]
    }
    
    func tableView(tableView: UITableView, didEndEditingRowAtIndexPath indexPath: NSIndexPath) {
        
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        cell?.layer.cornerRadius = 10
        
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let id = segue.identifier else {
            return
        }
        
        if id == "AddSafetyPlanS2ItemSegue" {
            presentPopupFromSegue(segue, withLabelString: "Support Person", andPlaceHolderText: "Go for a coffee with John") { vc in
                
                guard let popupController = vc as? AddSafetyPlanItemViewController else {
                    return
                }
                popupController.textField.resignFirstResponder()
                
                if let titleString = popupController.saveThis where !titleString.isEmpty {
                    let newItem = ZSPSafetyPlanItem(userID: "", title: titleString)
                    self.MODEL.addStepTwoItem(newItem)
                }
            }
        }
    }
    
    func setupView() {
        
        stepTwoCornerContainer.clipsToBounds = true
        stepTwoCornerContainer.layer.cornerRadius = 10
        
        stepTwoContainerView.layer.shadowColor = UIColor.blackColor().CGColor
        stepTwoContainerView.layer.shadowOpacity = 0.2
        stepTwoContainerView.layer.shadowOffset = CGSizeMake(0, 3 )
        stepTwoContainerView.layer.shadowRadius = 1
        
    }
}


