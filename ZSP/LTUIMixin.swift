//
//  LTUIMixin.swift
//  ZSP
//
//  Created by Donovan King on 8/2/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

protocol LeanTaaSUIMixin {}

extension LeanTaaSUIMixin {
    
    func clear() -> UIColor {
        return UIColor.clearColor()
    }
    
    func white() -> UIColor {
        return UIColor.whiteColor()
    }
    
    func selectedBlue() -> UIColor {
        return UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
    }
    
    func activityColor() -> UIColor {
        return UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
    }
    
    func medicationColor() -> UIColor {
        return UIColor(red:0.28, green:0.87, blue:0.87, alpha:1.0)
    }
    
    func otherReminderColor() -> UIColor {
        return UIColor(red:0.79, green:0.79, blue:0.79, alpha:1.0)
    }
    
    func curveView(view: UIView, byRoundingCorners corners: UIRectCorner, withRadius radius: CGFloat) {
        
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSizeMake(radius, radius))
        let mask = CAShapeLayer()
        mask.frame = view.bounds
        mask.path = path.CGPath
        view.layer.mask = mask
    }
    
    func imageFromColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 100, height: 100)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
    func displayErrorAlert(view: UIViewController, message: String, completion: (() -> Void)?) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: .Destructive, handler: nil))
        view.presentViewController(alert, animated: true, completion: completion)
    }
    
    func presentViewControllerOverRoot(root: UIViewController, vc: UIViewController, completion: (() -> Void)?) {
        let alertView = UIWindow(frame: UIScreen.mainScreen().bounds)
        alertView.rootViewController = root
        alertView.windowLevel = 10000001
        alertView.hidden = false

        alertView.rootViewController?.presentViewController(vc, animated: true, completion: nil)
    }
    
}