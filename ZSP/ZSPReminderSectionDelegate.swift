//
//  ZSPReminderSectionDelegate.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol ZSPReminderSectionDelegate {
    func sectionDone(sender: ZSPReminderSection)
    func sectionIsEmpy(index: Int)
}