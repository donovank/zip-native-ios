//
//  ReminderFrequencyPopup.swift
//  ZSP
//
//  Created by Donovan King on 8/4/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ReminderFrequencyPopup: UIViewController, LeanTaaSUIMixin {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var oneTimeImageView: UIImageView!
    @IBOutlet weak var oneTimeButton: UIButton!
    @IBOutlet weak var onDaysImageView: UIImageView!
    @IBOutlet weak var onDaysButton: UIButton!
    @IBOutlet weak var dailyImageView: UIImageView!
    @IBOutlet weak var dailyButton: UIButton!
    @IBOutlet weak var weeklyImageView: UIImageView!
    @IBOutlet weak var weeklyButton: UIButton!
    @IBOutlet weak var nextOrSave: UIBarButtonItem!
    
    var delegate: ReminderFrequencyDelegate?
    
    let checkedImage = UIImage(named: "Checkedbox")
    let unCheckedImage = UIImage(named: "UncheckedBox")
    
    var mode: ZSPReminderRepeatMode = .Never {
        didSet {
            setMode()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
        }
        
        if let presetMode = delegate?.repeatMode {
            mode = presetMode
        } else {
            setMode()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        curveView(backgroundView, byRoundingCorners: [.BottomLeft, .BottomRight], withRadius: 10)
    }
    
    @IBAction func saveOrNext(sender: AnyObject) {
        switch mode {
        case .Never:
            delegate?.repeatMode = mode
            dismissViewControllerAnimated(true, completion: nil)
        case .OnDays:
            performSegueWithIdentifier("SelectDaysSegue", sender: self)
        case .Daily:
            delegate?.repeatMode = mode
            dismissViewControllerAnimated(true, completion: nil)
        case .Weekly:
            delegate?.repeatMode = mode
            dismissViewControllerAnimated(true, completion: nil)
        }

    }

    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func selectOneTime(sender: AnyObject) {
        mode = .Never
        
    }

    @IBAction func selectOnDays(sender: AnyObject) {
        mode = .OnDays
    }
    
    @IBAction func selectDaily(sender: AnyObject) {
        mode = .Daily
    }

    @IBAction func selectWeekly(sender: AnyObject) {
        mode = .Weekly
    }
    
    private func setMode() {
        let imageViews = [oneTimeImageView, onDaysImageView, dailyImageView, weeklyImageView]
        for imageView in imageViews {
//            imageView.image =
            UIView.transitionWithView(imageView,
                                      duration:0.2,
                                      options: UIViewAnimationOptions.TransitionCrossDissolve,
                                      animations: { imageView.image = self.unCheckedImage },
                                      completion: nil)
        }
        switch mode {
        case .Never:
            oneTimeImageView.image = checkedImage
            setBarButtonToNext(false)
        case .OnDays:
            onDaysImageView.image = checkedImage
            setBarButtonToNext(true)
        case .Daily:
            dailyImageView.image = checkedImage
            setBarButtonToNext(false)
        case .Weekly:
            weeklyImageView.image = checkedImage
            setBarButtonToNext(false)
        }
    }
    
    private func setupImages() {
        let imageViews = [oneTimeImageView, onDaysImageView, dailyImageView, weeklyImageView]
        for imageView in imageViews {
            imageView.tintColor = selectedBlue()
        }
    }
    
    private func setBarButtonToNext(isNext: Bool) {
        if isNext {
            UIView.setAnimationsEnabled(false)
            nextOrSave.title = "Next"
            UIView.setAnimationsEnabled(true)
        } else {
            UIView.setAnimationsEnabled(false)
            nextOrSave.title = "Save"
            UIView.setAnimationsEnabled(true)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SelectDaysSegue" {
            if let destVC = segue.destinationViewController as? ReminderDaysPopup {
                destVC.delegate = self.delegate
            }
        }
    }
    
}