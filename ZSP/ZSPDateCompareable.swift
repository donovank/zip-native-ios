//
//  ZSPDateCompareable.swift
//  ZSP
//
//  Created by Donovan King on 8/1/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol ZSPDateCompareable {
    var day: Int { get }
    var month: Int { get }
    var year: Int { get }
    var date: NSDate { get }
}

extension ZSPDateCompareable {
    
    func isDifferentMonth(itemA: ZSPDateCompareable, itemB: ZSPDateCompareable) -> Bool {
        if itemA.month != itemB.month || itemA.year != itemB.year {
            return true
        }
        return false
    }
    
    func hasDifferentMonthThan(item: ZSPDateCompareable) -> Bool {
        if self.month != item.month || self.year != item.year {
            return true
        }
        return false
    }
    
    func hasDifferentMonthThan(tup: (d: Int, m: Int, y: Int)) -> Bool {
        if tup.m != self.month || tup.y != self.year {
            return true
        }
        return false
    }
    
    func isDifferentDate(itemA: ZSPDateCompareable, itemB: ZSPDateCompareable) -> Bool {
        if itemA.day != itemB.day || itemA.month != itemB.month || itemA.year != itemB.year {
            return true
        }
        return false
    }
    
    func hasDifferentDateThan(item: ZSPDateCompareable) -> Bool {
        if self.day != item.day || self.month != item.month || self.year != item.year {
            return true
        }
        return false
    }
    
    func isDifferentDate(tup: (d: Int, m: Int, y: Int), thanItem item: ZSPDateCompareable) -> Bool {
        if tup.d != item.day || tup.m != item.month || tup.y != item.year {
            return true
        }
        return false
    }
    
    func hasDifferentDateThan(tup: (d: Int, m: Int, y: Int)) -> Bool {
        if tup.d != self.day || tup.m != self.month || tup.y != self.year {
            return true
        }
        return false
    }
    
    func isDifferentDate(date: NSDate, thanItem item: ZSPDateCompareable) -> Bool {
        let tup = (d: NSCalendar.currentCalendar().component(.Day, fromDate: date),
                   m: NSCalendar.currentCalendar().component(.Month, fromDate: date),
                   y: NSCalendar.currentCalendar().component(.Year, fromDate: date))
        
        if tup.d != item.day || tup.m != item.month || tup.y != item.year {
            return true
        }
        return false
    }
    
    func hasDifferentDateThan(date: NSDate) -> Bool {
        let tup = (d: NSCalendar.currentCalendar().component(.Day, fromDate: date),
                   m: NSCalendar.currentCalendar().component(.Month, fromDate: date),
                   y: NSCalendar.currentCalendar().component(.Year, fromDate: date))
        
        if tup.d != self.day || tup.m != self.month || tup.y != self.year {
            return true
        }
        return false
    }
    
    func getDayPostfix() -> String {
        switch self.day {
        case 11, 12, 13:
            return "th"
        default:
            break
        }
        
        let modded = self.day%10
        switch modded {
        case 1:
            return "st"
        case 2:
            return "nd"
        case 3:
            return "rd"
        default:
            return "th"
        }
    }
    
    func getPostfixForDay(theDay: Int) -> String {
        var modded = theDay % 100
        switch modded {
        case 11, 12, 13: return "th"
        default: break
        }
        modded %= 10
        switch modded {
        case 1: return "st"
        case 2: return "nd"
        case 3: return "rd"
        default: return "th"
        }
    }
}