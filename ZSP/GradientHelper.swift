//
//  GradientHelper.swift
//  ZSP
//
//  Created by Donovan King on 7/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit


protocol GradientHelper {
    var presets: SpecGradientPresets { get }
//    func addBlueGradientLayer(view: UIView)
//    func clear() -> UIColor
//    func white() -> UIColor
}

extension GradientHelper {
    
    func addBlueGradientLayer(view: UIView) {
        
        view.backgroundColor = clear()
        let blueGradient = CAGradientLayer.init(layer: view.layer)
        blueGradient.frame = view.layer.bounds
        blueGradient.colors = presets.getBlueCellGradientColors()
        blueGradient.cornerRadius = presets.cellBorderRadius
        view.layer.insertSublayer(blueGradient, atIndex: 0)
    }
    
    func clear() -> UIColor {
        return UIColor.clearColor()
    }
    
    func white() -> UIColor {
        return UIColor.whiteColor()
    }
    
    func fromColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        CGContextSetFillColorWithColor(context, color.CGColor)
        CGContextFillRect(context, rect)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return img
    }
    
}



