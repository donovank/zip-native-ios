//
//  ZSPReminderFrequency.swift
//  ZSP
//
//  Created by Donovan King on 7/30/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

struct ZSPReminderFrequency {
    
    var mode: ZSPReminderRepeatMode
    var days: [ZSPDayType]
    
    init(days: [ZSPDayType], mode: ZSPReminderRepeatMode){
        self.days = days
        self.mode = mode
    }
    
}