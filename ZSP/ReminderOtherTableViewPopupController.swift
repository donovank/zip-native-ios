//
//  ReminderOtherTableViewPopupController.swift
//  ZSP
//s
//  Created by Donovan King on 8/3/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import DZNEmptyDataSet

class ReminderOtherTableViewPopupController: UITableViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, ZSPModelManagerDelegate {
    
    let MODEL = ZSPModelManager.DefaultModelManager.ReminderManager
    var selected: ZSPOtherReminderItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        MODEL.otherListDelegate = self
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MODEL.allOtherItems.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BasicCell", forIndexPath: indexPath)
        
        cell.textLabel?.text = MODEL.allOtherItems[indexPath.row].title
        //        cell.imageView?.frame = CGRectMake(0, 0, 10, 10)
        //
        //        cell.imageView?.image = cellImage(MODEL[indexPath.row].mood)
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selected = MODEL.allOtherItems[indexPath.row]
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteHandler = { (action: UITableViewRowAction, indexPath: NSIndexPath) -> Void in
            self.MODEL.removeOtherItem(self.MODEL.allOtherItems[indexPath.row])
        }
        
        let delete = UITableViewRowAction(style: .Default, title: "Delete", handler: deleteHandler)
        
        return [delete]
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let str = "Past Reminders will show here."
        let attrs = [NSFontAttributeName: UIFont.preferredFontForTextStyle(UIFontTextStyleBody),
                     NSForegroundColorAttributeName: UIColor.darkGrayColor()]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func refresh() {
        self.tableView.reloadData()
    }
}
