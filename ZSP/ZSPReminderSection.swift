//
//  ZSPReminderSection.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

class ZSPReminderSection: ZSPDateCompareable, ZSPSectionManager, LeanTaaSThreadingMixin {
    
    var updating = false
    var reminders = [ZSPReminder]() {
        didSet {
            if !updating {
                delegate.sectionDone(self)
            } else {
                self.updating = false
            }
        }
    }
    
    func objectUpdated(object: AnyObject, inSectionIndex sectionIndex: Int) {
        self.updating = true
        dispatch_async(GlobalUserInitiatedQueue) {
            guard let updatedEntry = object as? ZSPReminder else {
                self.updating = false
                return
            }
            self.updateItem(updatedEntry, sectionIndex: sectionIndex)
        }
    }
    
    var date: NSDate {
        get {
            return reminders[0].date
        }
    }
    
    var day: Int {
        get {
            return NSCalendar.currentCalendar().component(.Day, fromDate: self.date)
        }
    }
    
    var month: Int {
        get {
            return NSCalendar.currentCalendar().component(.Month, fromDate: self.date)
        }
    }
    
    var year: Int {
        get {
            return NSCalendar.currentCalendar().component(.Year, fromDate: self.date)
        }
    }
    
    var delegate: ZSPReminderSectionDelegate
    
    init(newReminders: [ZSPReminder], delegate: ZSPReminderSectionDelegate){
        self.delegate = delegate
        for reminder in newReminders {
            reminder.delegate = self
        }
        sortList(newReminders)
    }
    
    func getDateHeaderString() -> String {
        let formater = NSDateFormatter()
        
        formater.dateFormat = "EEEE, MMMM d'\(getDayPostfix())', yyyy"
        
        return formater.stringFromDate(date)
    }
    
    func addItem(item: ZSPReminder) {
        item.delegate = self
        var tmp = reminders
        tmp.append(item)
        sortList(tmp)
    }
    
    /* 
     * This is a horrible function. Probably can remove this at some point.
     * I built it just in case it's absolutely needed somewhere.
     */
    func removeItem(item: ZSPReminder, sectionIndex: Int) -> Bool {
        var index = 0
        for reminder in reminders {
            if reminder === item {
                removeItemAtIndex(index, sectionIndex: sectionIndex)
                return true
            } else {
                index = index + 1
            }
        }
        return false
    }
    
    func removeItemAtIndex(index: Int, sectionIndex: Int) {
        var tmp = reminders
        tmp.removeAtIndex(index)
        if tmp.isEmpty {
            delegate.sectionIsEmpy(sectionIndex)
        } else {
            reminders = tmp
        }
    }
    
    func sortList(tmpList: [ZSPReminder]) {
        reminders = tmpList.sort({ (a, b) -> Bool in
            if .OrderedAscending == a.date.compare(b.date) {
                return true
            } else {
                return false
            }
        })
    }
    
    func updateItem(item: ZSPReminder, sectionIndex: Int) {
        var index = 0
        for reminder in self.reminders {
            if reminder === item {
                self.updateItemAtIndex(index, sectionIndex: sectionIndex)
            } else {
                index = index + 1
            }
        }
    }
    
    func updateItemAtIndex(index: Int, sectionIndex: Int) {
        var tmp = self.reminders
        let reminder = tmp.removeAtIndex(index)
        reminder.delegate = nil
        if tmp.isEmpty {
            self.delegate.sectionIsEmpy(sectionIndex)
        } else {
            self.reminders = tmp
        }
        dispatch_async(GlobalUserInitiatedQueue) {
            ZSPModelManager.DefaultModelManager.ReminderManager.addNewReminder(reminder)
        }
    }
}
