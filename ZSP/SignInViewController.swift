//
//  SignInViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import SWRevealViewController
import PhoneNumberKit

class SignInViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var signinButton: UIButton!
    @IBOutlet weak var phoneNumberBGView: UIView!
    @IBOutlet weak var passwordBGView: UIView!
    @IBOutlet weak var phoneNumberField: PhoneNumberTextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var forgotPassButton: UIButton!
    
    
    
    var phoneNumberIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    var passIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        passwordField.secureTextEntry = true
        setupView()
        
    }
    
    @IBAction func userSwipedDown(sender: AnyObject) {
        view.endEditing(true)
    }
    
    
    @IBAction func signInClicked(sender: AnyObject) {
        // **DISABLES SIGN IN**
//        self.performSegueWithIdentifier("GoToApp", sender: self)
//        return
        // **DISABLES SIGN IN**
        if let number = phoneNumberField.text,
            let pass = passwordField.text {
            do {
                let phoneNumber = try PhoneNumber(rawNumber: number)
                let plain = phoneNumber.toE164(false)
                trySignInUser(plain, pass: pass)
            } catch {
                print("ERROR WITH PHONE NUMBER")
            }
        } else {
            print("Error unwrapping sign in optionals")
        }
    }
    
    func trySignInUser(phoneNum: String, pass: String) {
        activityIndicator.startAnimating()
        print(phoneNum)
        ZSPNetworkManager.Network.API.Auth.tryAuthUser(phoneNum,
                                                       password: pass)
        { (result) in
            switch result {
            case .Success:
                self.performSegueWithIdentifier("GoToApp", sender: self)
            case .ResetPassword:
                // Load resetpassword
                break
            case .Failure:
                self.activityIndicator.stopAnimating()
                self.view.endEditing(true)
                self.wrongPassWord()
            }
            self.activityIndicator.stopAnimating()
        }
    }
    
    func wrongPassWord() {
        
        let wrongAlert = UIAlertController(title: "Incorrect password or phonenumber.", message: nil, preferredStyle: .Alert)
        wrongAlert.addAction(UIAlertAction(title: "Okay", style: .Cancel){ (alert) in
            self.passwordField.text = nil
            })
        self.presentViewController(wrongAlert, animated: true, completion: nil)
        
    }
    
    func tryEnableSignup () {
        if phoneNumberIsValid && passIsValid {
            signinButton.alpha = 1.0
            signinButton.enabled = true
        } else {
            signinButton.alpha = 0.5
            signinButton.enabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.signInClicked(textField)
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if var fullString = textField.text {
            if range.length > 0 {
                fullString.deleteCharactersInRange(range)
            } else {
                fullString.insertContentsOf(string.characters, at: fullString.startIndex.advancedBy(range.location))
            }
            
            if fullString.characters.count >= 1 {
                if textField === passwordField {
                    passIsValid = true
                    return true
                }
                if textField === phoneNumberField {
                    var valid = false
                    do {
                        valid = try PhoneNumber(rawNumber: fullString).isValidNumber
                    } catch {
//                        print("ERROR WITH PHONE NUMBER")
                    }
                    
                    if valid {
                        phoneNumberIsValid = true
                        return true
                    } else {
                        phoneNumberIsValid = false
                        return true
                    }
                }
            } else {
                if textField === passwordField {
                    passIsValid = false
                    return true
                } else if textField === phoneNumberField {
                    phoneNumberIsValid = false
                    return true
                }
            }
        }
        return true
    }
    
    func setupView() {
        if let img = UIImage(named: "SigninBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        let cornerViews = [signupButton,signinButton, phoneNumberBGView, passwordBGView]
        
        for view in cornerViews {
            view.clipsToBounds = true
            view.layer.cornerRadius = view.bounds.height/2
        }
        // **DISABLES SIGN IN**
        signinButton.alpha = 0.5
        signinButton.enabled = false
        // **DISABLES SIGN IN**
        phoneNumberField.delegate = self
        passwordField.delegate = self
        
        forgotPassButton.setTitleColor(UIColor.lightGrayColor(), forState: .Highlighted)
    }
    
}