//
//  SafetyPlanPopupParentController.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import MZFormSheetPresentationController

protocol SafetyPlanPopupParentController {
    
}

extension SafetyPlanPopupParentController {
    
    func presentPopupFromSegue(segue: UIStoryboardSegue, withLabelString showText: String, andPlaceHolderText phText: String, completionHandler: MZFormSheetPresentationViewControllerCompletionHandler?) {
        let presentationSegue = segue as! MZFormSheetPresentationViewControllerSegue
        //        let container = presentationSegue.formSheetPresentationController.view
        //        let screenBounds = UIScreen.mainScreen().bounds
        
        if let prezController = presentationSegue.formSheetPresentationController.presentationController {
            //                prezController.contentViewSize = CGSizeMake(300, 200)  // Set size first!
            //            prezController.contentViewSize = CGSizeMake(CGFloat(0.9*screenBounds.width), CGFloat(0.3*screenBounds.height))
            
            //            let y = screenBounds.height/3 - container.frame.height
            //
            //            prezController.portraitTopInset = y
            
            prezController.shouldCenterHorizontally = true
            prezController.shouldApplyBackgroundBlurEffect = false
            
        }
        presentationSegue.formSheetPresentationController.contentViewControllerTransitionStyle = .SlideFromTop
        //        presentationSegue.formSheetPresentationController.contentViewCornerRadius = 10
        
        presentationSegue.formSheetPresentationController.willPresentContentViewControllerHandler = { vc in
            
            guard let popupController = vc as? AddSafetyPlanItemViewController else {
                return
            }
            
            popupController.navBar.topItem?.title = showText
            popupController.navBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor()]
            popupController.navBar.tintColor = UIColor.darkGrayColor()
            if let font = UIFont(name: "Lato-Regular", size: 18) {
                popupController.navBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                     NSFontAttributeName : font]
                popupController.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
                popupController.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
                popupController.navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Disabled)
            } else {
                let font = UIFont()
                popupController.navBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                     NSFontAttributeName : font]
            }
            popupController.textField.placeholder = phText
            popupController.textField.becomeFirstResponder()
        }
        if let handler = completionHandler {
            presentationSegue.formSheetPresentationController.didDismissContentViewControllerHandler = handler
        }
    }
    
}