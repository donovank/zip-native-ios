//
//  ZSPJournalEntryCellType.swift
//  ZSP
//
//  Created by Donovan King on 9/13/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

enum ZSPJournalEntryCellType {
    case Basic
    case NoTitle
    case ActivityEntry
}