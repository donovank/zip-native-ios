//
//  ScaledSliderDelegate.swift
//  ZSP
//
//  Created by Donovan King on 9/19/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

protocol ScaledSliderDelegate {
    
    func sliderChangedValue(newValue: Int)
    
}