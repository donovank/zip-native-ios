//
//  SignUpTwoViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class SignUpTwoViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var tempPassBGView: UIView!
    @IBOutlet weak var tempPassField: UITextField!
    @IBOutlet weak var newPassBGView: UIView!
    @IBOutlet weak var newPassField: UITextField!
    @IBOutlet weak var confirmPassBGView: UIView!
    @IBOutlet weak var confirmPassField: UITextField!
    
    var otp: String?
    
    
    var tempPassIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    var newPassIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        tempPassField.text = otp
        if let tmp = tempPassField.text where !tmp.isEmpty {
            tempPassIsValid = true
        }
    }
    
    @IBAction func userSwipedDown(sender: AnyObject) {
        view.endEditing(true)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func signUpClicked(sender: AnyObject) {
        if let oldPass = tempPassField.text,
            let newPass = newPassField.text,
            let confirm = confirmPassField.text {
            trySignUpUser(oldPass, new: newPass, newConfirmed: confirm)
        } else {
            print("Error unwrapping one of the text fields")
        }
    }
    
    func trySignUpUser(old: String, new: String, newConfirmed: String) {
        ZSPNetworkManager.Network.API.Auth.resetPasswordFrom(old,
                                                             toNewPassword: new,
                                                             withConfirmation: newConfirmed)
        { (result) in
            switch result {
            case .Success:
                ZSPModelManager.DefaultModelManager.loadModels()
                self.performSegueWithIdentifier("SignUpCompleteSegue", sender: self)
            case .ResetPassword:
                print("got reset password code")
                break
            case .Failure:
                print("Got fail code")
                break
            }
        }
    }
    
    func tryEnableSignup () {
        if newPassIsValid && tempPassIsValid {
            signupButton.alpha = 1.0
            signupButton.enabled = true
        } else {
            signupButton.alpha = 0.5
            signupButton.enabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if var fullString = textField.text {
            if range.length > 0 {
                fullString.deleteCharactersInRange(range)
            } else {
                fullString.insertContentsOf(string.characters, at: fullString.startIndex.advancedBy(range.location))
            }
            
            if fullString.characters.count >= 1 {
                if textField === tempPassField {
                    tempPassIsValid = true
                    return true
                }
                if textField === newPassField {
                    if confirmPassField.text == fullString {
                        newPassIsValid = true
                        return true
                    } else {
                        newPassIsValid = false
                        return true
                    }
                }
                if textField === confirmPassField {
                    if newPassField.text == fullString {
                        newPassIsValid = true
                        return true
                    } else {
                        newPassIsValid = false
                        return true
                    }
                }
            } else {
                if textField === tempPassField {
                    tempPassIsValid = false
                    return true
                } else if textField === newPassField || textField === newPassField {
                    newPassIsValid = false
                    return true
                }
            }
        }
        return true
    }
    
    func setupView() {
        if let img = UIImage(named: "SigninBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        let cornerViews = [signupButton,
                           backButton,
                           newPassBGView,
                           tempPassBGView,
                           confirmPassBGView]
        
        for view in cornerViews {
            view.clipsToBounds = true
            view.layer.cornerRadius = view.bounds.height/2
        }
        
        signupButton.alpha = 0.5
        signupButton.enabled = false
        
        newPassField.delegate = self
        tempPassField.delegate = self
        confirmPassField.delegate = self
    }
    
}

