//
//  NewReminderContainerViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/25/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class NewReminderContainerViewController: UIViewController, UITableViewDelegate {
    
    var labelText: String?
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
//        if let parent = parentViewController as? NewReminderViewController {
//          tableView.dataSource = parent
//        }
        tableView.tableFooterView = UIView()
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        labelText = cell?.textLabel?.text
        performSegueWithIdentifier("ToReminderLabel", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "ToReminderLabel" {
            let destination = segue.destinationViewController as! NewReminderSelectedLabelViewController
            destination.text = labelText
        }
    }
    
    
}