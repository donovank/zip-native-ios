//
//  PrivacyViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/25/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import SWRevealViewController

class PrivacyViewController: UIViewController, LeanTaaSUIMixin ,SWRevealViewControllerDelegate {
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var scrollBackgroundView: UIView!
    @IBOutlet weak var faqButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyBackground()
        setupViews()
        
        menuButton.target = self.revealViewController()
        menuButton.action = Selector("revealToggle:")
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.revealViewController()?.delegate = self
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsetsMake(0, -10, 0, 15)
    }
    
    func revealController(revealController: SWRevealViewController!, didMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            faqButton.userInteractionEnabled = true
        } else {
            faqButton.userInteractionEnabled = false
        }
    }
    
    func revealController(revealController: SWRevealViewController!, willMoveToPosition position: FrontViewPosition) {
        if(position == .Left) {
            faqButton.userInteractionEnabled = true
        } else {
            faqButton.userInteractionEnabled = false
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
    }
    
    func applyBackground() {
        if let img = UIImage(named: "SettingsBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MoreQuestionsSegue" {
            if let vc = segue.destinationViewController as? FAQViewController {
                vc.navigationItem.leftBarButtonItem = vc.navigationItem.backBarButtonItem
            }
        }
    }
    

    
    func setupViews() {
//        scrollBackgroundView.layoutIfNeeded()
        scrollBackgroundView.clipsToBounds = true
        scrollBackgroundView.layer.cornerRadius = 10

        let height = faqButton.layer.frame.height/2
        faqButton.layer.cornerRadius = height

    }
    
}