//
//  LTHeightPicker.swift
//  ZSP
//
//  Created by Donovan King on 8/23/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class LeanTaaSHeightPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    let feet = [1,2,3,4,5,6,7,8]
    let inch = [0,1,2,3,4,5,6,7,8,9,10,11]
    
    var isMetric = false
    
    var cm: [Int]
    
    var selectedFeet = 1
    var selectedInch = 0
    var selectedCm = 1
    var selectedString: String {
        get {
            if isMetric {
                return String(selectedCm) + " cm"
            } else {
                return String(selectedFeet) + " ft " + String(selectedInch) + " in"
            }
        }
    }
    
    weak var updateLabel: UITextField?
    
    init() {
        cm = [Int]()
        for num in 1...300 {
            cm.append(num)
        }
        super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        self.delegate = self
        self.dataSource = self
    }
    
    override init(frame: CGRect) {
        cm = [Int]()
        for num in 1...300 {
            cm.append(num)
        }
        super.init(frame: frame)
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        cm = [Int]()
        for num in 1...300 {
            cm.append(num)
        }
        super.init(coder: aDecoder)
        self.delegate = self
        self.dataSource = self
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return isMetric ? 1 : 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return isMetric ? cm.count : feet.count
        } else {
            return inch.count
        }
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return isMetric ? (String(cm[row]) + " cm") : (String(feet[row]) + " ft")
        } else {
            return String(inch[row]) + " in"
        }
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if component == 0 {
            if isMetric {
                selectedCm = cm[row]
            } else {
                selectedFeet = feet[row]
            }
        } else {
            selectedInch = inch[row]
        }
        
        updateLabel?.text = selectedString
    }
}