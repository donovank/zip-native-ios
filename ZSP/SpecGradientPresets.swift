//
//  SpecGradientPresets.swift
//  Pods
//
//  Created by Donovan King on 7/13/16.
//
//

import Foundation
import UIKit

struct SpecGradientPresets {
    private let gB1 = UIColor(red: 70.0/255, green: 150.0/255, blue: 201.0/255, alpha: 1)
    private let gB2 = UIColor(red: 145.0/255, green: 229.0/255, blue: 255.0/255, alpha: 1)
        
    let cellBorderRadius = CGFloat(6)
    
    func getBlueCellGradientColors() -> [CGColor] {
        return [gB2.CGColor, gB1.CGColor]
    }
    
}