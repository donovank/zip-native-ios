//
//  ZSPSafetyPlanManager.swift
//  ZSP
//
//  Created by Donovan King on 7/31/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation

final class ZSPSafetyPlanManager: ZSPManager, LeanTaaSThreadingMixin {
    
    static let Default = ZSPSafetyPlanManager()
    
    private init () { }
    
    func clearForSignOff() {
        
        self.allStepOneItems = []
        self.stepOneItems = []
        self.stepTwoItems = []
        self.stepThreeItems = []
        
    }
    
    private var allStepOneItems = [ZSPActivityItem]()
    var stepOneItems = [ZSPActivityItem]() {
        didSet {
            dispatch_async(GlobalMainQueue) { self.stepOneDelegate?.refresh() }
        }
    }
    
    var currentFilter = 5
    
    var stepTwoItems = [ZSPSafetyPlanItem]() {
        didSet {
            dispatch_async(GlobalMainQueue) { self.stepTwoDelegate?.refresh() }
        }
    }
    
    var stepThreeItems = [ZSPSafetyPlanItem]() {
        didSet {
            dispatch_async(GlobalMainQueue) { self.stepThreeDelegate?.refresh() }
        }
    }
    
    var stepOneDelegate: ZSPModelManagerDelegate?
    var stepTwoDelegate: ZSPModelManagerDelegate?
    var stepThreeDelegate: ZSPModelManagerDelegate?
    
    func setActivityMood(mood: Int) {
        guard currentFilter != mood else {
            return
        }
        currentFilter = mood
        self.filterOne()
    }
    
    private func filterOne() {
        let tmpAll = self.allStepOneItems
        dispatch_async(GlobalUserInitiatedQueue) { 
            self.stepOneItems = tmpAll.filter { (item) -> Bool in
                if item.mood == self.currentFilter {
                    return true
                }
                return false
            }
        }
    }
    
    func getAllActivities() -> [ZSPActivityItem] {
        return allStepOneItems
    }

    func addNewActvity(activity: ZSPActivityItem) {
        ZSPNetworkManager.Network.API.SafetyPlan.addActivity(activity)
        self.allStepOneItems.append(activity)
        self.filterOne()
    }
    
    func addLoadedActvities(activities: [ZSPActivityItem]) {
        self.allStepOneItems.appendContentsOf(activities)
        dispatch_async(GlobalUserInitiatedQueue, { self.filterOne() })
    }
    
    
    func removeActivity(activity: ZSPActivityItem) {        
        var index = 0
        for n in allStepOneItems {
            if n === activity {
                allStepOneItems.removeAtIndex(index)
                ZSPNetworkManager.Network.API.SafetyPlan.deleteItem(activity, step: 1)
                self.filterOne()
            }
            index = index + 1
        }
    }
    
    func addStepTwoItem(item: ZSPSafetyPlanItem) {
        ZSPNetworkManager.Network.API.SafetyPlan.addItem(item, toStep: 2)
        stepTwoItems.append(item)
    }
    
    func addLoadedStepTwoItems(items: [ZSPSafetyPlanItem]) {
        stepTwoItems.appendContentsOf(items)
    }
    
    func addStepThreeItem(item: ZSPSafetyPlanItem) {
        ZSPNetworkManager.Network.API.SafetyPlan.addItem(item, toStep: 3)
        stepThreeItems.append(item)
    }
    
    func addLoadedStepThreeItems(items: [ZSPSafetyPlanItem]) {
        stepThreeItems.appendContentsOf(items)
    }
    
}