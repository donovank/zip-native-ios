//
//  ReminderFrequencyDelegate.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol ReminderFrequencyDelegate {
    var repeatMode: ZSPReminderRepeatMode { get set }
    var repeatDays: [ZSPDayType : Bool] { get set }
    
}