//
//  ReminderCell.swift
//  ZSP
//
//  Created by Donovan King on 7/19/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit

class ReminderCell: UITableViewCell, LeanTaaSUIMixin {
    
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var alarmButton: UIButton!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var repeatOnDaysView: RepeatOnDaysView!
    
    var needsSetup = true
    var cellReminder: ZSPReminder?
    
    var active: Bool = false {
        didSet {
            if active {
                alarmButton.setImage(UIImage(named: "ActiveAlarmIcon"), forState: .Normal)
            } else {
                alarmButton.setImage(UIImage(named: "InactiveAlarmIcon"), forState: .Normal)
            }
        }
    }
        
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func toggleActivation(sender: AnyObject) {
        guard let reminder = cellReminder else {
            fatalError("CELL DOES NOT HAVE REMINDER")
        }
        reminder.active = !reminder.active
        active = reminder.active
    }
    
    func cellSetup(withReminder reminder: ZSPReminder) {
        cellReminder = reminder
        
        titleLabel.text = reminder.title
        active = reminder.active
        repeatOnDaysView.type = reminder.frequency
        alarmButton.imageView?.tintColor = white()
        
        switch reminder.type {
        case .Activity:
            alarmButton.backgroundColor = activityColor()
        case .Medication:
            alarmButton.backgroundColor = medicationColor()
        case .Other:
            alarmButton.backgroundColor = otherReminderColor()
        }
        
        
        if needsSetup {
            contentContainer.layoutIfNeeded()
            
            
            backgroundColor = UIColor.clearColor()
            contentContainer.layer.cornerRadius = 4
            contentContainer.clipsToBounds = true
            selectionStyle = .None
        }
    }
}
