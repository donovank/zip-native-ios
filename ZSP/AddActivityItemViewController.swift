//
//  AddActivityItemViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/31/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import MZFormSheetPresentationController

class AddActivityItemViewController: UIViewController, UITextFieldDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var textField: UITextField!

    @IBOutlet weak var nextButton: UIBarButtonItem!

    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var bgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var extenderView: UIView!
    
    var extended = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        buttonEnabled(false)
        textField.enablesReturnKeyAutomatically = true
        textField.autocapitalizationType = .Sentences
        
        curveView(backgroundView, byRoundingCorners: [.BottomLeft, .BottomRight], withRadius: 6.0)
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        
        if let font = UIFont(name: "Lato-Regular", size: 18) {
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
        } else {
            let font = UIFont()
            navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor(),
                 NSFontAttributeName : font]
            navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
            navigationItem.rightBarButtonItem?.setTitleTextAttributes([NSFontAttributeName : font], forState: .Normal)
        }
        
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "PickMoodSegue" {
            
            if let vc = segue.destinationViewController as? MoodSelectionViewController {
                vc.activityString = textField.text
                vc.textEntryVC = self
            }
        }
    }
    
    @IBAction func next(sender: AnyObject) {
        textField.resignFirstResponder()
        UIView.transitionWithView(extenderView,
                                  duration: 0.2,
                                  options: UIViewAnimationOptions.TransitionCrossDissolve,
                                  animations: { 
                                    self.extenderView.hidden = false
            }) { (completed) in }
        self.performSegueWithIdentifier("PickMoodSegue", sender: sender)
    }
    
    func closeExtender() {
        self.extenderView.hidden = true
    }

    func buttonEnabled(val: Bool) {
        nextButton.enabled = val
        if val {
            nextButton.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        } else {
            nextButton.tintColor = UIColor.lightGrayColor()
        }
    }
    
    
//    func setupPopup() {
//        saveButton.layer.cornerRadius = 10
//        cancelButton.layer.cornerRadius = 10
//    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if var fullString = textField.text {
            if range.length > 0 {
                fullString.deleteCharactersInRange(range)
            } else {
                fullString.insertContentsOf(string.characters, at: fullString.startIndex.advancedBy(range.location))
            }
            
            if fullString.characters.count >= 1 {
                buttonEnabled(true)
            } else {
                buttonEnabled(false)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        next(textField)
        return false
    }
    
}