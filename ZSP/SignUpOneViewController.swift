//
//  SignUpOneViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/22/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import PhoneNumberKit

class SignUpOneViewController: UIViewController, UITextFieldDelegate, LeanTaaSUIMixin {
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var phoneNumberBGView: UIView!
    @IBOutlet weak var tempPassBGView: UIView!
    @IBOutlet weak var phoneNumberField: PhoneNumberTextField!
    @IBOutlet weak var tempPassField: UITextField!
    
    var phoneNumberIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    var tempPassIsValid = false {
        didSet {
            tryEnableSignup()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    @IBAction func userSwipedDown(sender: AnyObject) {
        view.endEditing(true)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func moveToNextStep(sender: AnyObject) {
//        self.performSegueWithIdentifier("SignupPartTwoSegue", sender: self)
//        return
        guard phoneNumberField.isValidNumber else {
            let alert = UIAlertController(title: "Invalid Phone Number", message: "The phone number you entered is not valid. Please double check the phone number and try again.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Done", style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        print("valid phone number")
        if let number = phoneNumberField.text,
            let pass = tempPassField.text {
            do {
                let phoneNumber = try PhoneNumber(rawNumber: number)
                let plain = phoneNumber.toE164(false)
                tryMoveToNextStep(plain, tempPass: pass)
            } catch {
                print("ERROR WITH PHONE NUMBER")
            }
        } else {
            print("Error unwrapping sign in optionals")
        }

    }
    
    func tryMoveToNextStep(phone: String, tempPass: String) {
        ZSPNetworkManager.Network.API.Auth.tryAuthUser(phone,
                                                       password: tempPass)
        { (result) in
            switch result {
            case .Success:
                self.performSegueWithIdentifier("SignupPartTwoSegue", sender: self)
            case .ResetPassword:
                self.performSegueWithIdentifier("SignupPartTwoSegue", sender: self)
            case .Failure:
                break
            }
        }
    }
    
    func tryEnableSignup () {
        if phoneNumberIsValid && tempPassIsValid {
            signupButton.alpha = 1.0
            signupButton.enabled = true
        }else {
            signupButton.alpha = 0.5
            signupButton.enabled = false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if var fullString = textField.text {
            if range.length > 0 {
                fullString.deleteCharactersInRange(range)
            } else {
                fullString.insertContentsOf(string.characters, at: fullString.startIndex.advancedBy(range.location))
            }
            
            if fullString.characters.count >= 1 {
                if textField === tempPassField {
                    tempPassIsValid = true
                    return true
                }
                if textField === phoneNumberField {
                    var valid = false
                    do {
                        valid = try PhoneNumber(rawNumber: fullString).isValidNumber
                    } catch {
                        print("ERROR WITH PHONE NUMBER")
                    }

                    if valid {
                        phoneNumberIsValid = true
                        return true
                    } else {
                        phoneNumberIsValid = false
                        return true
                    }
                }
            } else {
                if textField === tempPassField {
                    tempPassIsValid = false
                    return true
                } else if textField === phoneNumberField {
                    phoneNumberIsValid = false
                    return true
                }
            }
        }
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "SignupPartTwoSegue" {
            if let vc = segue.destinationViewController as? SignUpTwoViewController {
                vc.otp = tempPassField.text
            }
        }
    }
    
    
    func setupView() {
        if let img = UIImage(named: "SigninBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
        
        let cornerViews = [signupButton, cancelButton, phoneNumberBGView, tempPassBGView]
        
        for view in cornerViews {
            view.clipsToBounds = true
            view.layer.cornerRadius = view.bounds.height/2
        }
        
        let navbarImage = imageFromColor(UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.3))
        navigationController?.navigationBar.setBackgroundImage(navbarImage, forBarMetrics: .Default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.darkGrayColor()]
        navigationController?.navigationBar.tintColor = UIColor.darkGrayColor()
        
        signupButton.alpha = 0.5
        signupButton.enabled = false
        
        phoneNumberField.delegate = self
        tempPassField.delegate = self
    }
    
}