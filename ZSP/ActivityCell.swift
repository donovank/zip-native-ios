//
//  SimpleActivityCell.swift
//  ZSP
//
//  Created by Donovan King on 7/31/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ActivityCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var alarmImage: UIButton!
    
    var activity: ZSPActivityItem?
    
    var hasActiveReminder = false {
        didSet {
            if hasActiveReminder {
                alarmImage.setImage(UIImage(named: "ActiveAlarmIcon"), forState: .Normal)
                activity?.reminderOn = hasActiveReminder
            } else {
                alarmImage.setImage(UIImage(named: "InactiveAlarmIcon"), forState: .Normal)
                activity?.reminderOn = hasActiveReminder
            }
        }
    }
    
    @IBAction func toggleReminder(sender: AnyObject) {
        
        hasActiveReminder = !hasActiveReminder
    }
    
    func fromActivity(activity: ZSPActivityItem) {
        title.text = activity.title
        hasActiveReminder = activity.reminderOn
        self.activity = activity
    }
    
}