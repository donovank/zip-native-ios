//
//  MoodMetricChartCell.swift
//  ZSP
//
//  Created by Donovan King on 7/12/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import UIKit
//import Charts

class MoodMetricChartCell: UITableViewCell, GradientHelper {
    
    let presets = SpecGradientPresets()
    
    @IBOutlet weak var moodCellBackgroundView: UIView!
    @IBOutlet weak var chartView: UIView!
    @IBOutlet weak var chartTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setUpCell() {
        contentView.layoutIfNeeded()
        
//        moodCellBackgroundView.backgroundColor = clear()
//        chartView.backgroundColor = clear()
//        
//        addBlueGradientLayer(moodCellBackgroundView)
//        
//        setUpChart()
        
    }
    
//    func setUpChart() {
//        let lineChart = LineChartView(frame: chartView.layer.bounds)
//        
//        lineChart.noDataText = "No Data Availible"
//        lineChart.delegate = self
//        lineChart.backgroundColor = UIColor.clearColor()
//        lineChart.userInteractionEnabled = false
//        lineChart.legend.enabled = false
//        lineChart.leftAxis.enabled = false
//        
//        lineChart.xAxis.labelPosition = .Bottom
//        lineChart.xAxis.drawGridLinesEnabled = false
//        lineChart.xAxis.labelTextColor = white()
//        lineChart.xAxis.axisLineColor = white()
//        lineChart.xAxis.axisLineWidth = 2
//        
//        lineChart.rightAxis.axisLineWidth = 2
//        lineChart.rightAxis.axisLineColor = white()
//        lineChart.rightAxis.gridColor = UIColor(white: 1.0, alpha: 0.2)
//        lineChart.rightAxis.labelTextColor = white()
//        lineChart.rightAxis.gridLineWidth = 1
//        
//        let yFormat = NSNumberFormatter()
//        yFormat.allowsFloats = false
//        lineChart.rightAxis.valueFormatter = yFormat
//        lineChart.descriptionText = ""
//
//        
//        chartView.insertSubview(lineChart, atIndex: 0)
//    }
    
    
}
