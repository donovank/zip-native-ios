//
//  SettingsViewController.swift
//  ZSP
//
//  Created by Donovan King on 7/25/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var fbStackCell: UIView!
    @IBOutlet weak var locationStackCell: UIView!
    @IBOutlet weak var pushStackCell: UIView!
    @IBOutlet weak var touchIDStackCell: UIView!
    
    @IBOutlet weak var privacyButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        applyBackground()
        setupStackCells()
        setupButtons()
        
        
        
    }
    
    func setupButtons() {
        let buttons = [privacyButton, logoutButton]
        
        for button in buttons {
            let height = button.layer.frame.height/2
        
            button.layer.cornerRadius = height
        }
    }
    
    func setupStackCells() {
        let cells = [fbStackCell, locationStackCell, pushStackCell, touchIDStackCell]
        
        for cell in cells {
            cell.clipsToBounds = true
            cell.layer.cornerRadius = 10
        }
    }
    
    func applyBackground() {
        if let img = UIImage(named: "SettingsBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    
}