//
//  PhoneCell.swift
//  ZSP
//
//  Created by Donovan King on 9/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class PhoneCell: UITableViewCell {
    
    @IBOutlet weak var phoneIconView: UIImageView!
    
    weak var delegate: StepThreeViewController?
    var phoneNumber: String?
    
    @IBAction func phoneButtonDidBeginClick(sender: AnyObject) {
        phoneIconView.tintColor = UIColor.lightGrayColor()
    }
    
    @IBAction func phoneButtonDidEndClick(sender: AnyObject) {
        phoneIconView.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
        if let number = phoneNumber {
            delegate?.callPhoneNumber(number)
        }
    }
    
    @IBAction func switchBackColor(sender: AnyObject) {
        phoneIconView.tintColor = UIColor(red:0.02, green:0.69, blue:0.90, alpha:1.0)
    }
    
    
}