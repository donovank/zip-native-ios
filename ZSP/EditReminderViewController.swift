//
//  EditReminderViewController.swift
//  ZSP
//
//  Created by Donovan King on 8/10/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit
import MZFormSheetPresentationController

class EditReminderViewController: UIViewController, UITextFieldDelegate, LeanTaaSUIMixin, LeanTaaSThreadingMixin, ReminderFrequencyDelegate {
    
    @IBOutlet weak var typeIcon: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var repeatView: RepeatOnDaysView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var reminderTypeBackground: UIView!
    @IBOutlet weak var repeatContainerView: UIView!
    
    
    lazy var activityIcon = UIImage(named: "ActivityIconInactive")
    lazy var medIcon = UIImage(named: "MedicationIconInactive")
    lazy var otherIcon = UIImage(named: "OtherIconInactive")
    
    weak var reminder: ZSPReminder? {
        didSet {
            if nameTextField != nil {
                setupViewData()
            }
        }
    }
    
    var editingEntryIndexPath: NSIndexPath?
    
    var repeatDays = [ZSPDayType : Bool]()
    var repeatMode: ZSPReminderRepeatMode = .Never {
        didSet {
            switch repeatMode {
            case .OnDays:
                repeatView.setRepeatOnDays(ZSPDayType.getTrueDays(repeatDays))
            default:
                repeatView.setRepeatLabelTo(repeatMode)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        scrollView.backgroundColor = clear()
        
        setupViewData()
        applyBackground()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        repeatContainerView.clipsToBounds = true
        curveView(repeatContainerView, byRoundingCorners: [.BottomRight, .BottomLeft], withRadius: 10)
        
        reminderTypeBackground.clipsToBounds = true
        curveView(reminderTypeBackground, byRoundingCorners: [.TopLeft, .TopRight], withRadius: 10)
    }
    
    func setupViewData() {
        guard let theReminder = reminder else {
            return
        }
        
        switch theReminder.type {
        case .Activity:
            typeLabel.text = "Activity"
            typeIcon.image = activityIcon
            reminderTypeBackground.backgroundColor = activityColor()
        case .Medication:
            typeLabel.text = "Medication"
            typeIcon.image = medIcon
            reminderTypeBackground.backgroundColor = medicationColor()
        case .Other:
            typeLabel.text = "Other"
            typeIcon.image = otherIcon
            reminderTypeBackground.backgroundColor = otherReminderColor()
        }
        
        nameTextField.text = theReminder.title
        datePicker.date = theReminder.date
        
        for day in theReminder.frequency.days {
            repeatDays[day] = true
        }
        repeatMode = theReminder.frequency.mode
        
    }
    
    @IBAction func cancel(sender: AnyObject) {
        view.endEditing(true)
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func save(sender: AnyObject) {
        view.endEditing(true)
        guard let theReminder = reminder else {
            print("ERROR WITH EDITING A REMINDER")
            return
        }
        
        if let titleEdit = nameTextField.text where !titleEdit.isEmpty {
            theReminder.title = titleEdit
        }
        
        theReminder.date = datePicker.date
        
        theReminder.frequency.days = ZSPDayType.getTrueDays(repeatDays)
        theReminder.frequency.mode = self.repeatMode
        
        if let section = editingEntryIndexPath?.section {
            dispatch_async(GlobalUserInitiatedQueue, {
                theReminder.updateObjectAtSectionIndex(section)
            })
        } else {
            print("ERROR WITH INDEXPATH")
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let titleEdit = textField.text where !titleEdit.isEmpty {
            reminder?.title = titleEdit
        }
        return false
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            navigationItem.rightBarButtonItem?.enabled = false
            return true
        }
        
        if text.isEmpty {
            navigationItem.rightBarButtonItem?.enabled = false
        } else {
            navigationItem.rightBarButtonItem?.enabled = true
        }
        
        return true
    }
    
    func applyBackground() {
        if let img = UIImage(named: "RemindersBackground") {
            view.backgroundColor = UIColor(patternImage: img)
        } else {
            view.backgroundColor = UIColor.lightGrayColor()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "RepeatSelectSegue" {
            createRepeatPopover(segue, handler: { (vc) in })
        }
    }
    
    typealias 💩 = MZFormSheetPresentationControllerTransitionBeginCompletionHandler
    typealias 😖 = MZFormSheetPresentationViewControllerSegue
    
    func createRepeatPopover(segue: UIStoryboardSegue, handler: 💩) {
        let MZSegue = segue as! 😖
        if let prezController = MZSegue.formSheetPresentationController.presentationController {
            prezController.contentViewSize = CGSizeMake(300, 500)
            prezController.shouldCenterHorizontally = true
            prezController.shouldCenterVertically = false
            prezController.shouldApplyBackgroundBlurEffect = false
            prezController.shouldDismissOnBackgroundViewTap = false
            MZSegue.formSheetPresentationController.willPresentContentViewControllerHandler = { (vc) in
                if let nav = vc as? UINavigationController,
                    let controller = nav.visibleViewController as? ReminderFrequencyPopup {
                    controller.delegate = self
                }
            }
        }
        
    }
    
}