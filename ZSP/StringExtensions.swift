//
//  StringExtensions.swift
//  ZSP
//
//  Created by Donovan King on 9/26/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


extension String {
    mutating func deleteCharactersInRange(range: NSRange) {
        let mutableSelf = NSMutableString(string: self)
        mutableSelf.deleteCharactersInRange(range)
        self = mutableSelf as String
    }
}