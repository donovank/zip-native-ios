//
//  ZSPCollector.h
//  ZSP
//
//  Created by Donovan King on 8/2/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <net/if_dl.h>

@interface ZSPCollector : NSObject

-(NSDictionary *) getData;

@end
