//
//  MoodSelectionPopoverController.swift
//  ZSP
//
//  Created by Donovan King on 9/18/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class MoodSelectionPopoverController: UIViewController {
    
    @IBOutlet weak var veryHappyButton: UIButton!
    @IBOutlet weak var happyButton: UIButton!
    @IBOutlet weak var neutralButton: UIButton!
    @IBOutlet weak var sadButton: UIButton!
    @IBOutlet weak var verySadButton: UIButton!
    
    var delegate: ZSPMoodPickerDelegate?
    
    @IBAction func moodSelected(sender: AnyObject) {
        
        if sender === veryHappyButton {
            delegate?.moodSelected(.VeryHappy)
        }
        if sender === happyButton {
            delegate?.moodSelected(.Happy)
        }
        if sender === neutralButton {
            delegate?.moodSelected(.Neutral)
        }
        if sender === sadButton {
            delegate?.moodSelected(.Sad)
        }
        if sender === verySadButton {
            delegate?.moodSelected(.VerySad)
        }
        delegate?.moodHasBeenSet = true
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}