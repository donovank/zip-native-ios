//
//  ReminderDaysPopup.swift
//  ZSP
//
//  Created by Donovan King on 8/4/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation
import UIKit

class ReminderDaysPopup: UIViewController, LeanTaaSUIMixin {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var mondayImageView: UIImageView!
    @IBOutlet weak var mondayButton: UIButton!
    @IBOutlet weak var tuesdayImageView: UIImageView!
    @IBOutlet weak var tuesdayButton: UIButton!
    @IBOutlet weak var wednesdayImageView: UIImageView!
    @IBOutlet weak var wednesdayButton: UIButton!
    @IBOutlet weak var thursdayImageView: UIImageView!
    @IBOutlet weak var thursdayButton: UIButton!
    @IBOutlet weak var fridayImageView: UIImageView!
    @IBOutlet weak var fridayButton: UIButton!
    @IBOutlet weak var saturdayImageView: UIImageView!
    @IBOutlet weak var saturdayButton: UIButton!
    @IBOutlet weak var sundayImageView: UIImageView!
    @IBOutlet weak var sundayButton: UIButton!
    
    var delegate: ReminderFrequencyDelegate?
    
    let checkedImage = UIImage(named: "Checkedbox")
    let unCheckedImage = UIImage(named: "UncheckedBox")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for day in ZSPDayType.iteratable {
            if let isOn = delegate?.repeatDays[day] where isOn {
                checkmarkOnDay(day, isOn: true)
            } else {
                delegate?.repeatDays[day] = false
                checkmarkOnDay(day, isOn: false)
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        curveView(backgroundView, byRoundingCorners: [.BottomLeft, .BottomRight], withRadius: 10)
    }
    
    @IBAction func save(sender: AnyObject) {
        delegate?.repeatMode = .OnDays
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func toggleMonday(sender: AnyObject) {
        toggleDay(.Mon)
    }
    
    @IBAction func toggleTuesday(sender: AnyObject) {
        toggleDay(.Tue)
    }
    
    @IBAction func toggleWednesday(sender: AnyObject) {
        toggleDay(.Wed)
    }

    @IBAction func toggleThursday(sender: AnyObject) {
        toggleDay(.Thu)
    }

    @IBAction func toggleFriday(sender: AnyObject) {
        toggleDay(.Fri)
    }
    
    @IBAction func toggleSaturday(sender: AnyObject) {
        toggleDay(.Sat)
    }
    
    @IBAction func toggleSunday(sender: AnyObject) {
        toggleDay(.Sun)
    }
    
    private func toggleDay(day: ZSPDayType) {
        if let on = delegate?.repeatDays[day] where on {
            delegate?.repeatDays[day] = false
            checkmarkOnDay(day, isOn: false)
        } else {
            delegate?.repeatDays[day] = true
            checkmarkOnDay(day, isOn: true)
        }
    }
    
    private func checkmarkOnDay(day: ZSPDayType, isOn: Bool) {
        var toggleImage: UIImage?

        if isOn {
            toggleImage = checkedImage
        } else {
            toggleImage = unCheckedImage
        }
        
        switch day {
        case .Mon:
            mondayImageView.image = toggleImage
        case .Tue:
            tuesdayImageView.image = toggleImage
        case .Wed:
            wednesdayImageView.image = toggleImage
        case .Thu:
            thursdayImageView.image = toggleImage
        case .Fri:
            fridayImageView.image = toggleImage
        case .Sat:
            saturdayImageView.image = toggleImage
        case .Sun:
            sundayImageView.image = toggleImage
        }
    }
    
    
}