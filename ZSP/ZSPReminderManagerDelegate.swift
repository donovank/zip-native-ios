//
//  ZSPReminderManagerDelegate.swift
//  ZSP
//
//  Created by Donovan King on 8/8/16.
//  Copyright © 2016 Donovan King. All rights reserved.
//

import Foundation


protocol ZSPModelManagerDelegate {
    func refresh()
}